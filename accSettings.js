import React from 'react'

import { ImageBackground, Image, Button, TouchableOpacity, KeyboardAvoidingView, Dimensions, Platform, PixelRatio } from 'react-native';
import { StyleSheet, View, Text, TextInput} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import Dialog from "react-native-dialog";
import { ScrollView } from 'react-native-gesture-handler';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import profile from './images/profile.png'
import as_icon from './images/as_icon.png'
import back_image from './src/backgrounds/app_background.png'
import defaultImage from './android/app/src/main/assets/defaultImage.png'
import uploadImage from './android/app/src/main/assets/upload.png'
const options = {
    title: 'Select Avatar',
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };

  const {
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
  } = Dimensions.get('window');
  
  // based on phone's scale
  const scale = SCREEN_WIDTH / 320;

export default class accSettings extends React.PureComponent {

  _normalize(size) {
    const newSize = size * scale 
    return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2
    
  }
    _uploadImage(){
        console.log("function called")
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);
           
            if (response.didCancel) {
              console.log('User cancelled image picker');
            } else if (response.error) {
              console.log('ImagePicker Error: ', response.error);
            } else {
              const source = { uri: response.uri };
            if (source){
                console.log("inside source log")
                db.transaction(tx => {
                      tx.executeSql('update userdetails set profileimage=?', [response.uri], (tx, results) => {
                        console.log("profile image url resultsaffected: ", results.rowsAffected)
                      });
                  });
                }
              // You can also display the image using data:
              // const source = { uri: 'data:image/jpeg;base64,' + response.data };
                console.log("profile image url: ",source)
              this.setState({
                avatarSource: source,
              });
            }
          });
    }
    _showDialog(){
      this.setState({dialogStatus: true})
    }
 
    _handleCancel = () => {
      this.setState({ dialogStatus: false });
    };
    _handleOK (x){
      console.log("changed nickname: ", x)
      this.setState({ dialogStatus: false });
      this._updateNickname(this.state.nickname)
    };

    _updateNickname(x){
      console.log("_updateNickname function called")
      console.log("new nickname value: ", x)
      this.setState({nickname: x})
      db.transaction(tx => {
        tx.executeSql('update userdetails set nickname=?', [x], (tx, results) => {
          console.log("Nickname updated in local database: ", results.rowsAffected)
        })
      })
      fetch(hostip+'updateUserDetails', {
        method: 'PUT',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            userId: uid,
            nickname: this.state.nickname
        }),
        
        })
        .then((response) => response.json())
        .then((responseData) => {
            console.log("Response",responseData )
            console.log("userid: ", uid)
            if (responseData.response == 'Success')
            {
                console.log("Nickname updated in server")
            }
        })
        .catch(error => {
          console.log('There has been a problem with your fetch operation: ' + error.message);
          })  
    }

componentDidMount(){
    fetch(hostip+'userDetails?uid='+uid,{
        method: 'GET'
      })
      .then(res => res.json())
      .then((data) => {
        db.transaction(tx => {
            tx.executeSql('update userdetails set useruniqueid=?,nickname=?, gender=?, birthday=?, location=? from userdetails', [results.rows.item(0)['useruniqueid'], results.rows.item(1)['nickname'], results.rows.item(2)['gender'], results.rows.item(3)['birthday'], results.rows.item(4)['location']], (tx, results) => {
              console.log("userdetails results: ", results.rows.item(0))
            });
          });
        this.setState({ 
            userUniqueId: data.userUniqueId,
            nickname: data.nickname,
            gender: data.gender,
            birthday: data.birthday,
            location: data.location,
            dialogStatus: false
        })

       console.log("userDetails api called")
      })
      .catch(error => {
        console.log('There has been a problem with your fetch operation: ' + error.message);
        })  
}

constructor(props) {
    super(props);
    this.state = {
        userUniqueId: 0,
        nickname: 'n/a',
        gender: '',
        birthday: '',
        location: '',
        avatarSource: defaultImage,
        tvalue: ''
    };
    
    console.log("inside acc settings constructor: ", this.state.avatarSource)

    db.transaction(tx => {
        tx.executeSql('select * from userdetails', [], (tx, results) => {
          console.log("results: ", results.rows.item(0))
          this.setState({
            userUniqueId: results.rows.item(0)['useruniqueid'], nickname: results.rows.item(0)['nickname'],
            gender: results.rows.item(0)['gender'], birthday: results.rows.item(0)['birthday'], location: results.rows.item(0)['location'],
            avatarSource: {"uri":results.rows.item(0)['profileimage']}
          });
        });
      });
      console.log("avatarSource: ", this.state.avatarSource)
    }

    render() {
      


/***************************************************************************************************************** */
        return (
          
            <ImageBackground
            source={back_image}
            style={{width: '100%', height: '100%'}}>
              <ScrollView>
                <View style={{flex: 1}}>
                  
                    <View style={{height:  150, backgroundColor: 'rgb(84, 106, 133)'}}></View>
                    <View style={{flexGrow: 1, alignItems: 'center', backgroundColor: 'rgb(176, 186, 198)'}}>
                            <TouchableOpacity activeOpacity = { 1 } onPress={() => {this._uploadImage()}} style={{top:-60,}}>
                                <View style={{width: '100%', height: 105,alignItems: 'center', justifyContent: 'center'}}>
                                    <Image source={this.state.avatarSource} style = {{ alignItems: "center",justifyContent: "center" ,width: 120, height: 120, borderRadius: 60, borderWidth: 3, borderColor: 'white'}}/>
                                    <Image source={uploadImage} style={{width: 25, height: 25, top: -23, marginLeft: 100}}/>
                                </View>
                            </TouchableOpacity>
                        <KeyboardAvoidingView style={{width: '100%', height: 400}}>
                        <View style={{flex: 1,alignItems: "center",justifyContent: "center", height:10}}>
                            <Image source={as_icon} style={{ position: 'absolute',resizeMode: "contain",alignItems: "center",justifyContent: "center", width: '90%', height: '90%' }}/>
                            <Text allowFontScaling={false} style={{fontSize: this._normalize(13),  fontWeight: 'bold', position: "absolute",color: "white", paddingRight: wp('63%'), paddingBottom: wp('7%')}}>USER ID</Text>
                            <Text allowFontScaling={false} style={{fontSize: this._normalize(15), position: "absolute",color: "white", textTransform: 'uppercase'}}>{this.state.userUniqueId}</Text>
                        </View>
                        
                        <TouchableOpacity style={{flex: 1,alignItems: "center",justifyContent: "center"}} onPress={() => {this._showDialog()}}>
                            <Image source={as_icon} style={{ position: 'absolute',resizeMode: "contain",alignItems: "center",justifyContent: "center", width: '90%', height: '90%' }}/>
                            <Text allowFontScaling={false} style={{fontSize: this._normalize(13),  fontWeight: 'bold', position: "absolute",color: "white", paddingRight: wp('56%'), paddingBottom: wp('7%')}}>NICKNAME</Text>
                            {/* <TextInput style={{fontSize: 15, position: "absolute", color: "white"}} defaultValue= {this.state.nickname} onEndEditing={event => {this._updateNickname(event.nativeEvent.text)}} /> */}
                            <Text allowFontScaling={false} style={{fontSize: this._normalize(15), position: "absolute", color: "white"}}>{this.state.nickname}</Text>
                            <View>
                              <Dialog.Container visible={this.state.dialogStatus}>
                                <Dialog.Title>Change nickname</Dialog.Title>
                                <Dialog.Input placeholder='Enter nickname' onChangeText={text => this.setState({ nickname: text })} /> 
                                <Dialog.Button label="Cancel" onPress={() => this._handleCancel()}/>
                                <Dialog.Button label="OK" onPress={() => this._handleOK(this.state.nickname)}/>
                              </Dialog.Container>
                            </View>
                        </TouchableOpacity>
                        
                        <View style={{flex: 1,alignItems: "center",justifyContent: "center"}}>
                            <Image source={as_icon} style={{ position: 'absolute',resizeMode: "contain",alignItems: "center",justifyContent: "center", width: '90%', height: '90%' }}/>
                            <Text allowFontScaling={false} style={{fontSize: this._normalize(13),  fontWeight: 'bold', position: "absolute",color: "white", paddingRight: wp('60%'), paddingBottom: wp('7%')}}>GENDER</Text>
                            <Text allowFontScaling={false} style={{fontSize: this._normalize(15), position: "absolute",color: "white"}}>{this.state.gender}</Text>
                        </View>
                        <View style={{flex: 1,alignItems: "center",justifyContent: "center"}}>
                            <Image source={as_icon} style={{ position: 'absolute',resizeMode: "contain",alignItems: "center",justifyContent: "center", width: '90%', height: '90%' }}/>
                            <Text allowFontScaling={false} style={{fontSize: this._normalize(13),  fontWeight: 'bold', position: "absolute",color: "white", paddingRight: wp('56%'), paddingBottom: wp('7%')}}>BIRTHDAY</Text>
                            <Text allowFontScaling={false} style={{fontSize: this._normalize(15), position: "absolute",color: "white"}}>{this.state.birthday}</Text>
                        </View>
                        <View style={{flex: 1,alignItems: "center",justifyContent: "center"}}>
                            <Image source={as_icon} style={{ position: 'absolute',resizeMode: "contain",alignItems: "center",justifyContent: "center", width: '90%', height: '90%' }}/>
                            <Text allowFontScaling={false} style={{fontSize: this._normalize(13),  fontWeight: 'bold', position: "absolute",color: "white", paddingRight: wp('56%'), paddingBottom: wp('7%')}}>LOCATION</Text>
                            <Text allowFontScaling={false} style={{fontSize: this._normalize(15), position: "absolute",color: "white"}}>{this.state.location}</Text>
                        </View>
                        <View style={{alignItems:'center', justifyContent:'center'}}><Text style={{ fontSize: this._normalize(10), color: 'white'}}>v1.7
                        </Text>
                        </View>
                        </KeyboardAvoidingView>
                    </View>
                </View>
                </ScrollView>
        </ImageBackground>
        )


    }
 
}
const styles = StyleSheet.create ({
    item: {
       flexDirection: 'column',
       width: '100%',
       height: 200,
       marginLeft: '5%',
       marginRight: '10%',
       marginTop: '10%',
       marginBottom: '3%',
       alignItems: 'center',
       position: 'relative',
    }
 })


