import React, { Component } from 'react' ;
import { View, StyleSheet } from 'react-native' ;

class roundProgressBar extends Component {
    render () {
        return (
            <View style={{flex: 1, justifyContent: 'space-evenly', margin: 100}}>
            <View style={styles.container}>
                <View style={styles.progressLayer}>
                    <View style={styles.hideProgressLayer}></View>
                </View>
            </View>
            </View>
        )
    }
} ;

const styles = StyleSheet.create ({
    container: {
        width: 200,
        height: 200,
        //backgroundColor: '#9b59b6',
        borderWidth: 20,
        borderRadius: 100,
        borderColor:'grey',
        justifyContent: 'center',
        alignItems: 'center',
    },
    progressLayer: {
        width: 200,
        height: 200,
        position: 'absolute',
        //backgroundColor: 'blue',
        borderWidth: 20,
        borderRadius: 100,
        //borderColor: '#3498db',
        borderRightColor: 'transparent',
        borderBottomColor: 'transparent',
        borderLeftColor: 'transparent',
        borderTopColor: '#3498db',
        transform:[{rotateZ: '45deg'}],
        justifyContent: 'center',
        alignItems: 'center',
    },
    hideProgressLayer: {
        width: 200,
        height: 200,
        position: 'absolute',
        borderWidth: 20,
        //borderColor: 'grey',
        borderRadius: 100,
        borderTopColor: 'transparent',
        borderRightColor: 'grey',
        borderBottomColor: 'grey',
        borderLeftColor: 'grey',
        transform:[{rotateZ: '-10deg'}],
    },
});

export default roundProgressBar ;