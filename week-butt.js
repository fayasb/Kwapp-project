import React, { useEffect, useState, PureComponent } from 'react'
import { BarChart, Grid, XAxis, YAxis  } from 'react-native-svg-charts'
import { NavigationEvents } from 'react-navigation';
import { ImageBackground, Image, Dimensions, PixelRatio  } from 'react-native';
import { StyleSheet, View, Button, SafeAreaView, Text, TextInput, Alert,UIManager,LayoutAnimation,TouchableOpacity} from 'react-native';
import city from "./cityscape.png";
import * as scale from 'd3-scale';
import para from './para_snipped.png'
import back_image from './src/backgrounds/app_background.png'
import { ScrollView } from 'react-native-gesture-handler';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const window = Dimensions.get("window");
const screen = Dimensions.get("screen");
const wscale = window.width / 320;
UIManager.setLayoutAnimationEnabledExperimental &&
  UIManager.setLayoutAnimationEnabledExperimental(true);
  const screenWidth = Math.round(Dimensions.get('window').width);
  const contentInset = {top: 10}
 
export default class week extends React.PureComponent {
  errorCB(err) {
    console.log("SQL Error: " + err);
  }
   
  successCB() {
    console.log("SQL executed fine");
  }
   
  openCB() {
    console.log("Database OPENED");
  }
  



      componentDidMount() {
        this._daily(this.state.statValue)
      }

      _normalize(size) {
        const newSize = size * wscale 
        return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2
        
      }
      cityLocation() {
          console.log("city function called")
        if (this.state.stepsWalked > 5000 && this.state.stepsWalked <= 10000) {
            this.state.position = 'center'
        } else if (this.state.stepsWalked > 10000)
        {
            this.state.position = 'flex-start'
        }
        else {
            this.state.position = 'flex-end'
        }
    }      

      _daily(x=1) {
        //daily points fetching from local db
        var tempArray1 = this.state.dailySteps
        var tempArray2 = this.state.dailyPoints
        var tempArray3 = ['0','0','0','0','0','0','0']
        var tempArray4 = this.state.dailyStepsY
        var tempArray5 = this.state.dailyPointsY
        this.setState({stepsWalkedText: "steps walked today",
                        pointsEarnedText: "points earned today",
                        paddingSteps: this.boxWidth - 265,
                        paddingPoints: this.boxWidth - 265})
        
        console.log("inside daily function=============================================")
        var year = new Date().getFullYear();
        console.log("year: ",year)
        try{
        db.transaction(tx => {
          tx.executeSql('select dailypoints, dailysteps, timeid, statid, day, month from userstatdetails order by statid desc limit 7', [], (tx, results) => {
            console.log("length of array: ", results.rows.length)
            for (let i = 0; i < results.rows.length; ++i){
              console.log("inside for loop: ", results.rows.item(i)['day'])
              tempArray1[6-i] = results.rows.item(i)['dailysteps']
              tempArray2[6-i] = results.rows.item(i)['dailypoints']
              console.log("month ", results.rows.item(i)['month'])
              if (results.rows.item(i)['month']>=10){
                  tempArray3[6-i] = results.rows.item(i)['day'].toString() + '.' + results.rows.item(i)['month'] + '.'
              }
              else {
                tempArray3[6-i] = results.rows.item(i)['day'].toString() + '.0' + results.rows.item(i)['month'] + '.'
              }

              tempArray4[7-i] = results.rows.item(i)['dailysteps']
              tempArray5[7-i] = results.rows.item(i)['dailypoints']
            }
            
            console.log("tempArray1 after for loop: ", tempArray3)
            this.setState({
              dailySteps: tempArray1, 
              dailyPoints: tempArray2, 
              stepsWalked: results.rows.item(0)['dailysteps'],
              pointsEarned: results.rows.item(0)['dailypoints'],
              xdata: tempArray3
            });
          });
        });
      }
      catch(err) {
        console.log(`----> (ERROR INSIDE COMPOUNT DID MOUNT) <----`, err);
      } 
      console.log("value of x******************************: ", x)

      if (x!=1){
          this.setState({ydata: tempArray5, barData: tempArray2})
        }
        else{
          this.setState({ydata: tempArray4, barData: tempArray1})
        }
      }


      _1week()
      {
        
        var tempArray1 = [0,0,0,0,0,0,0]
        var tempArray2 = [0,0,0,0,0,0,0]
        var tempArray3 = ['0','0','0','0','0','0','0']
        var tempArray4 = [0,0,0,0,0,0,0,0]
        var tempArray5 = [0,0,0,0,0,0,0,0]
        var stepSum = 0
        var pointSum = 0
        this.setState({stepsWalkedText: "steps walked these weeks",
                        pointsEarnedText: "points earned these weeks",
                        paddingSteps: '3%',
                        paddingPoints: '2%'})
        console.log("temparray1: ", tempArray1)
          db.transaction(tx => {
            tx.executeSql('select sum(dailysteps) as dailysteps, sum(dailypoints) as dailypoints, week, day, month from userstatdetails group by week order by statid desc', [], (tx, results) => {
              console.log("results: ", results.rows.item(0))
              for (let i=0;i<results.rows.length;++i){
                tempArray1[6-i] = results.rows.item(i)['dailysteps']
                tempArray2[6-i] = results.rows.item(i)['dailypoints']
                if (results.rows.item(i)['month']>=10){
                  tempArray3[6-i] = results.rows.item(i)['day'].toString() + '.' +results.rows.item(i)['month'] + '.'
              }
              else {
                tempArray3[6-i] = results.rows.item(i)['day'].toString() + '.0' +results.rows.item(i)['month'] + '.'
              }
                tempArray4[i] = results.rows.item(i)['dailysteps']
                tempArray5[i] = results.rows.item(i)['dailypoints']
                stepSum = stepSum + results.rows.item(i)['dailysteps']
                pointSum = pointSum + results.rows.item(i)['dailypoints']
                console.log("temp array in week: ", tempArray3)
              }
              console.log("temparray1: ", tempArray1)
              this.setState({dailySteps: tempArray1, 
                dailyPoints: tempArray2, 
                xdata: tempArray3,
              stepsWalked: stepSum,
            pointsEarned: pointSum})
            });
          });
          if (this.state.statValue!=1){
            this.setState({ydata: tempArray5, barData: tempArray2})
          }
          else{
            this.setState({ydata: tempArray4, barData: tempArray1})
          }

      }

      _1month()
      {
        var tempArray1 = [0,0,0,0,0,0,0]
        var tempArray2 = [0,0,0,0,0,0,0]
        var tempArray3 = ['0','0','0','0','0','0','0']
        var tempArray4 = [0,0,0,0,0,0,0,0]
        var tempArray5 = [0,0,0,0,0,0,0,0]
        var stepSum = 0
        var pointSum = 0
        var months = {
          '0' : 'xx',
          '1' : 'Jan',
          '2' : 'Feb',
          '3' : 'Mar',
          '4' : 'Apr',
          '5' : 'May',
          '6' : 'Jun',
          '7' : 'Jul',
          '8' : 'Aug',
          '9' : 'Sep',
          '10' : 'Oct',
          '11' : 'Nov',
          '12' : 'Dec'
      }
        this.setState({stepsWalkedText: "steps walked these months",
                      pointsEarnedText: "points earned these months",
                      paddingSteps: '0%',
                      paddingPoints: '0%'})
        console.log("temparray1: ", tempArray1)
          db.transaction(tx => {
            tx.executeSql('select sum(dailysteps) as dailysteps, sum(dailypoints) as dailypoints, month, year, timeid from userstatdetails group by month order by statid desc', [], (tx, results) => {
              console.log("results: ", results.rows.item(0))
              for (let i=0;i<results.rows.length;++i){
                tempArray1[6-i] = results.rows.item(i)['dailysteps']
                tempArray2[6-i] = results.rows.item(i)['dailypoints']
                tempArray3[6-i] = months[results.rows.item(i)['month']]
                tempArray4[7-i] = results.rows.item(i)['dailysteps']
                tempArray5[7-i] = results.rows.item(i)['dailypoints']
                stepSum = stepSum + results.rows.item(i)['dailysteps']
                pointSum = pointSum + results.rows.item(i)['dailypoints']
                console.log("sum(dailysteps): ",results.rows.item(i) )
              }
              console.log("temparray3 after for loop: ", tempArray3)
              this.setState({dailySteps: tempArray1, 
                dailyPoints: tempArray2, 
                xdata: tempArray3,
              stepsWalked: stepSum,
            pointsEarned: pointSum})
            });
          });
          if (this.state.statValue!=1){
            this.setState({ydata: tempArray5, barData: tempArray2})
          }
          else{
            this.setState({ydata: tempArray4, barData: tempArray1})
          }        
      }




      changeButtonColor1= () => {
        this.setState({ opacity1: 1,opacity2: 0.5, opacity3: 0.5}); 
      }
      changeButtonColor2= () => {
        this.setState({ opacity2: 1,opacity1: 0.5, opacity3: 0.5});  
      }
      changeButtonColor3= () => {
        this.setState({ opacity3: 1,opacity2: 0.5, opacity1: 0.5});  
      }
      changeButtonColor4= () => {
        this.setState({ buttonColor4: 'rgb(254, 170, 58)', buttonColor2: '#7b899b', buttonColor1: '#7b899b', buttonColor3: '#7b899b' }); 
      }

      statValueStepButton= () => {
        this.setState({statValue: 1, stepsButton: 1, pointButton: .5, opacity1: 1,opacity2: 0.5, opacity3: 0.5})
        this._daily(1)
      }

      statValuePointButton = () => {
        console.log("button clicked")
        this.setState({statValue: 0, pointButton: 1, stepsButton: .5, opacity1: 1,opacity2: 0.5, opacity3: 0.5})
        this._daily(0)
      }
      buttonlog= () => {
        console.log("button clicked")
      }

    constructor(props) {
        super(props);
        
        this.state = {
            xdata:[],
            ydata:[],
            stepsWalked: 10,
            pointsEarned: 10,
            position: "flex-end",
            opacity1: 1,
            opacity2: .5,
            opacity3: .5,
            stepsButton: 1,
            statValue: 1,
            pointButton: .5,
            marginPosition: '0%',
            dailySteps: [0,0,0,0,0,0,0],
            dailyPoints: [0,0,0,0,0,0,0],
            dailyStepsY: [0,0,0,0,0,0,0,10],
            dailyPointsY: [0,0,0,0,0,0,0,10],
            barData: [0,0,0,0,0,0,0],
            stepsWalkedText: "steps walked",
            pointsEarnedText: "points earned",
            paddingSteps:'18%',
            paddingPoints: '18%'
        };
        console.log("Global user id: ", uid)
        this.dimensions = {window, screen};
        this.offsetby = 10;
        this.hsize = this.dimensions.window.width - this.offsetby;
        this.boxWidth = this.hsize - 60;
        this.boxHeight = 50;
        this.buttonWidth = this.boxWidth - wp('50%')
        var p_total = 0;
        var moveMargin = 0
        try {
          db.transaction(tx => {
            tx.executeSql('select sum(dailysteps) as dailysteps, sum(dailypoints) as dailypoints, week, year from userstatdetails group by week order by week desc', [], (tx, results) => {
            moveMargin = (results.rows.item(0)['dailysteps']/70000)*20
            if (moveMargin < 20){
              console.log("movemargin: ", moveMargin.toString()+"%")
              this.setState({marginPosition: moveMargin.toString()+"%"})
              console.log("marginposition: ", this.state.marginPosition)
            }
            else{
              this.setState({marginPosition: "20%"})
            }
            });
    
        });
      }
      catch(err) {
        console.log(`----> (ERROR) <----`, err);
      }  
      
    }


    render() {
        const fill = 'rgb(254, 170, 58)'
        const data = this.state.xdata;
        const xvalue=[10,20,30,40,50,60,70]
            const axesSvg = { fontSize: 10, fill: 'white' };
            const verticalContentInset = { top: 10, bottom: 10 }
            const xAxisHeight = 30
            const tempValues = ['Mon', 'b', 'c', 'd', 'e', 'f', 'g']

            /* const ydata = [0, 200, 400, 600, 800, 1000, 1200, 1400, 1600] */
            console.log("xdata: ", this.state.xdata)
            console.log("rendered y axis: ", this.state.ydata)
            console.log("rendered barddata: ", this.state.barData)


/***************************************************************************************************************** */
        return (
            <ImageBackground
            source={back_image}
            style={{width: '100%', height: '100%'}}>
            <NavigationEvents
            onDidFocus={() => {this.componentDidMount()}}
            />
            <ScrollView>
                    <View style={{ height: 10, width : '90%',  flexDirection: 'row', marginTop: '10%', marginLeft: wp('3%')}}>
                        <TouchableOpacity activeOpacity = { 1 }  onPress={() => {this._daily(),this.changeButtonColor1()}}>
                                <Image source={para} style={{ resizeMode: "contain",alignItems: "center", justifyContent: "center" ,width: this.buttonWidth, height: wp('6%'), opacity: this.state.opacity1}}/>
                                <Text allowFontScaling={false} style={{fontSize: this._normalize(13), position: "absolute",color: "white", marginLeft: '35%'}}>Daily</Text>
                        </TouchableOpacity>
                        <TouchableOpacity activeOpacity = { 1 } onPress={() => {this._1week(),this.changeButtonColor2()}} style={{left: -wp('3%')}} > 
                                <Image source={para} style={{ resizeMode: "contain",alignItems: "center",justifyContent: "center" ,width: this.buttonWidth, height: wp('6%'), opacity: this.state.opacity2}}/>
                                <Text allowFontScaling={false} style={{fontSize: this._normalize(13),  position: "absolute",color: "white", marginLeft: '35%'}}>Weekly</Text>
                        </TouchableOpacity>
                        <TouchableOpacity activeOpacity = { 1 } onPress={() => {this._1month(),this.changeButtonColor3()}} style={{left: -wp('6%')}}>  
                                <Image source={para} style={{ resizeMode: "contain",alignItems: "center",justifyContent: "center" ,width: this.buttonWidth, height: wp('6%'), opacity: this.state.opacity3}}/>
                                <Text allowFontScaling={false} style={{fontSize: this._normalize(13),  position: "absolute",color: "white", marginLeft: '30%'}}>Monthly</Text>   
                        </TouchableOpacity>
                    </View>
                    <View style={{flexDirection: 'row', paddingTop: 15}}>
                          <TouchableOpacity activeOpacity = { 1 } onPress={() => {this.statValueStepButton()}} style={{ marginLeft: '10%',}}>
                                  <Image source={para} style={{ resizeMode: "contain",alignItems: "center",justifyContent: "center" ,width: this.buttonWidth, height: wp('6%'), opacity: this.state.stepsButton}}/>
                                  <Text allowFontScaling={false} style={{fontSize: this._normalize(13),  position: "absolute",color: "white", marginLeft: '35%'}}>Steps</Text>
                          </TouchableOpacity>
                          <TouchableOpacity activeOpacity = { 1 } onPress={() => {this.statValuePointButton()}} style={{ left: -wp('3%')}}>
                                  <Image source={para} style={{resizeMode: "contain",alignItems: "center",justifyContent: "center" ,width: this.buttonWidth, height: wp('6%'), opacity: this.state.pointButton}}/>
                                  <Text allowFontScaling={false} style={{fontSize: this._normalize(13),  position: "absolute",color: "white", marginLeft: '30%'}}>Points</Text>
                          </TouchableOpacity>

                    </View>
                    <View style={{ flexDirection: "row" }} >
                            <YAxis
                              data={this.state.ydata}
                              style={{
                                backgroundColor: "transparent",
                                flexGrow: 0,
                              }}
                              svg={axesSvg}
                              contentInset={{ top: 40, bottom: 35 }}
                              formatLabel={(value, index) => value}
                              numberOfTicks={7}
                              decimalPlaces= {0}
                            />
                            <View style={{ flex: 1, marginLeft: 5 }}>
                              <BarChart
                                  style={{ height: 300 }}
                                  data={ this.state.barData }
                                  svg={{ fill }}
                                  gridMin={0}
                                  contentInset={{ top: 40, bottom: 10, left:13, right: 10}}
                                  spacingInner={0.8}
                                  spacingOuter={0.3}
                                  numberOfTicks={7}
                                  

                              >
                                  <Grid/>
                              </BarChart>
                              <View style={{width: '100%', height: 25, opacity: .6,backgroundColor: '#8ab0bb', textAlignVertical: "center"}}>
                                <XAxis
                                    data={this.state.xdata}
                                    style={{paddingTop: 6}}
                                    formatLabel={(value,index) => `${this.state.xdata[index]}`}
                                    contentInset={{left: 30, right: 30}}
                                    svg={{ fontSize: 15, fontWeight: "bold", fill: 'rgb(255,255,255)' }}
                                />
                              </View>
                          </View>
                </View>
                <View style= {{flexDirection:'column',flex:1, justifyContent: 'center', alignItems: 'center'}}>
                        <View style={ {flexDirection: 'row', width: this.boxWidth, height: this.boxHeight, backgroundColor: '#d6d2d295', marginBottom: '3%', marginTop: '4%',borderRadius:10, borderWidth: 1, borderColor: '#d6d2d295'}} >
                                <Text allowFontScaling={false} style={{textAlign: 'left', marginLeft: '4%', fontWeight: "bold", fontFamily: 'sans-serif', fontSize: this._normalize(25), width: 110}}>{this.state.stepsWalked}</Text>  
                                <Text allowFontScaling={false} style={{textAlign: 'right', marginTop: '3%', fontFamily: 'sans-serif', fontSize: this._normalize(13)}}>{this.state.stepsWalkedText}</Text>           
                        </View>
                        <View style={{flexDirection: 'row', width: this.boxWidth, height: this.boxHeight, backgroundColor: '#d6d2d295', borderRadius:10, borderWidth: 1, borderColor: '#d6d2d295'}} >
                            <Text allowFontScaling={false} style={{textAlign: 'left', marginLeft: '4%', fontWeight: "bold", fontFamily: 'sans-serif', fontSize: this._normalize(25), width: 110}}>{this.state.pointsEarned}</Text>  
                            <Text allowFontScaling={false} style={{textAlign: 'left', paddingTop: '3%', fontFamily: 'sans-serif', fontSize: this._normalize(13)}}>{this.state.pointsEarnedText}</Text> 
                            {this.cityLocation(),console.log('position',  this.state.position)}
                        </View>
                            {/* <View style={[styles.container, { justifyContent: 'this.state.position', marginLeft: 30,flexDirection: 'row'},{width: screenWidth, height: 38, marginBottom: '3%'}]}> */}
                            <View style={[styles.container, { justifyContent: 'flex-end', marginLeft: this.state.marginPosition,flexDirection: 'row'},{width: screenWidth, height: 200, marginBottom: '3%'}]}>
                                <Image source={city} style = {{ width: 500, height: 300}} />     
                            </View>
                </View>
                </ScrollView>
        </ImageBackground>
        )


    }
 
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        paddingTop: 64,
        paddingBottom: 32
      },
    button: {
      backgroundColor: "#DDDDDD"
    },
    line: {
      borderBottomWidth: 1,
      borderColor: '#f48a92'
    },
    parallelogram: {
      width: 150,
      height: 100
    },
    responsiveBox: {
      width: wp('77%'),
      height: hp('6%'),
      flexDirection: 'column',
      justifyContent: 'space-around',
      backgroundColor: '#d6d2d295', 
      borderRadius:10, 
      borderWidth: 1, 
      borderColor: '#d6d2d295' 
    }   
  });

  