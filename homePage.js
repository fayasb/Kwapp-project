/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * 
 */

import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, ImageBackground, Dimensions } from 'react-native';

// Import custom components
import CircularProgress from './src/components/circularProgressBar/CircularProgress';
import HorizontalProgress from './src/components/horizontalProgressBar/HorizontalProgress';
import CascadingCircularProgress from './src/components/circularProgressBar/CascadingCircularProgress';
import TextWithImgae from './src/components/textWithImage/textWithImage';

// Gather app window and screen dimensions
const window = Dimensions.get("window");
const screen = Dimensions.get("screen");

// Image handler for the app background
const bgimage = require('./src/backgrounds/app_background.png');
const classTxtBgImage = require("./src/backgrounds/class_background.png");
const mascotImg = require("./src/backgrounds/mascot.png");
export default function App({navigation}) {
   // Refresh dimensions
   const [dimensions, setDimensions] = useState({ window, screen });
 /*  const [data, setData] = useState({userClass: 'beginner',profileLevelPcnt: '50', stepsWalked: '5000', stepsGoal: '10000', pointProg: '50' })
    */const onChange = ({ window, screen }) => {
     setDimensions({ window, screen });
   }
   const [sw, setSw] = useState('1')
   const [plp, setPlp] = useState('1');
   const [uc, setUc] = useState('Beginner');
   const [pe, setPe] = useState(1)
   const [data, setData] = useState({userClass: 'Master',profileLevelPcnt: '50', stepsWalked: sw, stepsGoal: '10000', pointProg: pe  })
   console.log("sw: ", sw)
   db.transaction(tx => {
    tx.executeSql('select day, d_pointsearned from userstat where statid = 7', [], (tx, results) => {
      console.log("homepage steps and points: ", results.rows.item(0))
      setPe(results.rows.item(0)['d_pointsearned'])
      setSw(results.rows.item(0)['day'])
      data.stepsWalked = sw
      data.pointProg = pe
    });
  });
  console.log("sw1: ", sw)
  db.transaction(tx => {
    tx.executeSql('select userlevel from userdetails', [], (tx, results) => {
      console.log("userlevel: ", results.rows.item(0)['userlevel'])
      uc = results.rows.item(0)['userlevel']
      data.stepsWalked = sw
      data.pointProg = pe
    });
  });
  console.log({userClass: 'Master',profileLevelPcnt: '50', stepsWalked: sw, stepsGoal: '10000', pointProg: pe  })
  let  rdata =  {userClass: 'Master',profileLevelPcnt: '50', stepsWalked: sw, stepsGoal: '10000', pointProg: pe  } 
  console.log ("steps walked ", sw)
  console.log ("points earned ", pe)
  console.log("data: ",data)
    let t_sw;
    let  t_pe;
   useEffect(() => {
     console.log("inside useeffect")
    setData({userClass: 'Master',profileLevelPcnt: '50', stepsWalked: sw, stepsGoal: '10000', pointProg: pe  })
    fetch('http://192.168.0.103:5000/homeRequest')
      .then((response) => response.json())
      .then((json) => {
        console.log("json: ", json)
        t_pe = json.pointProg
         setData(json)
         console.log("points from fetch ", json)
        db.transaction(tx => {
          tx.executeSql('update userstat set d_pointsearned=?, day=? where statid=7 ', [t_pe, json.stepsWalked], (tx, results) => {
            console.log("home afterfetch1 resultsaffected: ", results.rowsAffected)
            console.log("updated points: ", t_pe)
          });
      });
    })
      .catch(error => {
        console.log('There has been a problem with your fetch operation: ' + error.message);
        
        }) 
  }, []);
  
  
   // Points and leveling progress
 /*   const userClass = this.props.navigation.getParams('userClass','nothing sent'); */
    /* var userClass = navigation.getParam('userClass'); */
/*     console.log("userClass1", navigation.getParam('userClass')) */
/*   const { userClass } = route.params; */
   /* const profileLevelPcnt = "99";
   const stepsWalked = "4050";
   const stepsGoal = "10000";
   const pointProg = "75"; */

  // Handler variables
  const offsetby = 96;
  const hsize = dimensions.window.width - offsetby;
  const vsize = (dimensions.window.height / 2) - offsetby;
  const strokeWidth = 4;
  const radius = ((hsize - strokeWidth) / 2) - (offsetby / 2);
  const boxWidth = hsize - 20;
  const boxHeight = 28;
  const progressCx = hsize / 2;
  const progressCy = vsize / 2;

  // Variables for profile pic view
  const profileViewPcnt = 20;
  const profileWidth = profileViewPcnt * hsize / 100;
  const profileCx = profileWidth / 2;
  const profileCy = profileWidth / 2;
  const profileStrokeWidth = 3;
  const profileRadius = (profileWidth - profileStrokeWidth) / 2;
/***************************************************************************************************** */
 

  return (
    <>
      <View style={styles.containerStyle}>
        <ImageBackground source={bgimage} style={styles.imageStyle}>
          <View style={styles.headerContainerStyle}>
            <View style={styles.classContainerStyle}>
              <TextWithImgae source={classTxtBgImage}>
                {data.userClass} challenger
              </TextWithImgae>
            </View>
            <View style={styles.profileContainerStyle}>
              <CircularProgress
                boxWidth={profileWidth}
                boxHeight={profileWidth}
                bgColor="white"
                width={profileStrokeWidth}
                r={profileRadius}
                cx={profileCx}
                cy={profileCy}
                progressColor="white"
                dashArray="1, 0"
                progressPcnt={data.profileLevelPcnt}
              />
            </View>
          </View>
          <View style={styles.statsContainerStyle}>
            <View style={styles.pointProgressContainer}>
              <CascadingCircularProgress
                boxWidth={hsize}
                boxHeight={vsize}
                bgColor="white"
                width={strokeWidth}
                r={radius}
                cx={progressCx}
                cy={progressCy}
                progressColor="rgb(255,152,75)"
                dashArray="5, 1"
                steps={data.stepsWalked}
                stepsGoal={data.stepsGoal}
              />
              <HorizontalProgress
                boxWidth={boxWidth}
                boxHeight={boxHeight}
                edgeCurve="3"
                bgColor="#ffffff"
                progressColor="rgb(255,152,75)"
                progressPcnt={data.pointProg}
              />
              <Text style={styles.textStyle}>{data.pointProg}/100 points earned today</Text>
            </View>
          </View>
          <View style={styles.footerContainerStyle}>
            <View style={styles.notificationContainerStyle}>
              <Text style={styles.notificationTextStyle}>
                You have met your goal four days in a row! Just one more until the next achievement!
              </Text>
            </View>
            <View style={styles.mascottContainerStyles}>
              <Text style={styles.placeholderStyle}>The Mascott</Text>
            </View>
          </View>
        </ImageBackground>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between",
  },
  imageStyle: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
  },
  textStyle: {
    color: "white",
    fontSize: 15,
    fontWeight: "bold",
  },
  headerTextStyle: {
    color: "white",
    fontSize: 15,
    fontWeight: "bold",
    fontFamily: "sans-serif-light",
  },
  headerContainerStyle: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "grey",
    flexDirection: "row",
    padding: 20,
    width: "100%",
    height: "20%",
    justifyContent: "flex-start",
    alignItems: "center",
  },
  statsContainerStyle: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "grey",
    flexDirection: "column",
    padding: 20,
    width: "100%",
    height: "50%",
    justifyContent: "center",
    alignItems: "center",
  },
  footerContainerStyle: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "grey",
    flexDirection: "row",
    padding: 20,
    width: "100%",
    height: "30%",
    justifyContent: "center",
    alignItems: "center",
  },
  classContainerStyle: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "blue",
    padding: 20,
    width: "70%",
    justifyContent: "center",
    alignItems: "center",
  },
  profileContainerStyle: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "grey",
    // padding: 10,
    width: "30%",
    justifyContent: "center",
    alignItems: "center",
  },
  roundProgressContainer: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "grey",
    height: "70%",
    justifyContent: "center",
    alignItems: "center",
  },
  pointProgressContainer: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "grey",
    height: "30%",
    justifyContent: "center",
    alignItems: "center",
  },
  notificationContainerStyle: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "grey",
    width: "75%",
    justifyContent: "center",
    alignItems: "center",
  },
  notificationTextStyle: {
    color: "white",
    fontSize: 12,
    fontWeight: "bold",
  },
  mascottContainerStyles: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "grey",
    width: "25%",
    justifyContent: "center",
    alignItems: "center",
  },
  placeholderStyle: {
    color: "#282c34",
    fontSize: 12,
    fontWeight: "bold",
  },
});

{/* <Text style={styles.textStyle}>This is the header view</Text> 
<Text style={styles.textStyle}>Profile picture</Text>
<Text style={styles.textStyle}>This is the current stats</Text>


  placeholderStyle: {
    color: "#282c34",
    fontSize: 12,
    fontWeight: "bold",
  },
*/ }