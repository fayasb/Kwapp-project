import React, { useEffect, useState, PureComponent } from 'react'
import { NavigationEvents } from 'react-navigation';
import { ImageBackground, Image, Dimensions, PixelRatio  } from 'react-native';
import { StyleSheet, View, Button, SafeAreaView, Text, TextInput, Alert,UIManager,LayoutAnimation,TouchableOpacity} from 'react-native';
import {SlideBarChart} from 'react-native-slide-charts'
import city from "./cityscape.png";
import * as scale from 'd3-scale';
import para from './para_snipped.png'
import back_image from './src/backgrounds/app_background.png'
import { ScrollView } from 'react-native-gesture-handler';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const window = Dimensions.get("window");
const screen = Dimensions.get("screen");
const wscale = window.width / 320;
UIManager.setLayoutAnimationEnabledExperimental &&
  UIManager.setLayoutAnimationEnabledExperimental(true);
  const screenWidth = Math.round(Dimensions.get('window').width);
  const contentInset = {top: 10}
 
export default class statpage extends React.PureComponent {
  errorCB(err) {
    console.log("SQL Error: " + err);
  }
   
  successCB() {
    console.log("SQL executed fine");
  }
   
  openCB() {
    console.log("Database OPENED");
  }
  



      

      _normalize(size) {
        const newSize = size * wscale 
        return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2
        
      }
      cityLocation() {
          console.log("city function called")
        if (this.state.stepsWalked > 5000 && this.state.stepsWalked <= 10000) {
            this.state.position = 'center'
        } else if (this.state.stepsWalked > 10000)
        {
            this.state.position = 'flex-start'
        }
        else {
            this.state.position = 'flex-end'
        }
    }      

    _getGraphWidth(arrayLength) {
      var newGraphSize= this.state.defaultGraphSize + arrayLength*40
      this.setState({graphSize: newGraphSize})
    }

      _daily(x=1) {
        //daily points fetching from local db
        var tempArray1 = this.state.dailySteps
        var tempArray2 = this.state.dailyPoints
        var tempArray3 = ['0','0','0','0','0','0','0']
        var tempArray4 = this.state.dailyStepsY
        var tempArray5 = this.state.dailyPointsY
        var statBar = []
        var statAxes = []
        var statPoints = []
        var statYaxis = []
        var newGraphSize = 0
        var ymax = []
        var pymax = []
        var pointsYMax = 0
        var stepsYMax = 0
        this.setState({stepsWalkedText: "steps walked today",
                        pointsEarnedText: "points earned today",
                        paddingSteps: this.boxWidth - 265,
                        paddingPoints: this.boxWidth - 265})
        
        console.log("inside daily function=============================================")
        var year = new Date().getFullYear();
        console.log("year: ",year)
        try{
        db.transaction(tx => {
          tx.executeSql('select DISTINCT dailypoints, dailysteps, timeid, statid, day, month, year from userstatdetails group by timeid order by year desc, month desc, week desc, day desc', [], (tx, results) => {
            console.log("length of array: ", results.rows.length)
            for (let i = 0; i < results.rows.length; ++i){
              tempArray1[6-i] = results.rows.item(i)['dailysteps']
              tempArray2[6-i] = results.rows.item(i)['dailypoints']
              ymax.push(results.rows.item(i)['dailysteps'])
              pymax.push(results.rows.item(i)['dailypoints'])
              if (results.rows.item(i)['month']>=10){
                  tempArray3[6-i] = results.rows.item(i)['day'].toString() + '.' + results.rows.item(i)['month'] + '.'
                  statAxes.push({'x': results.rows.item(i)['day'].toString() + '.' + results.rows.item(i)['month'] + '.' , 'y': results.rows.item(i)['dailysteps']})
              }
              else {
                tempArray3[6-i] = results.rows.item(i)['day'].toString() + '.0' + results.rows.item(i)['month'] + '.'
                statAxes.push({'x': results.rows.item(i)['day'].toString() + '.0' + results.rows.item(i)['month'] + '.' , 'y': results.rows.item(i)['dailysteps']})
              }
              statBar.push({'x': results.rows.item(i)['day'], 'y': results.rows.item(i)['dailysteps']})
              statPoints.push({'x': results.rows.item(i)['day'], 'y': results.rows.item(i)['dailypoints']})
              tempArray4[7-i] = results.rows.item(i)['dailysteps']
              tempArray5[7-i] = results.rows.item(i)['dailypoints']
            }
            
            /* statBar=[{ x: 1, y: 1 },{ x: 2, y: 2 },{ x: 3, y: 3 },{ x: 4, y: 4 },{ x: 5, y: 5 },{ x: 6, y: 6 },{ x: 7, y: 7 },{ x: 8, y: 8 },{ x: 9, y: 9 },{ x: 10, y: 10 }] */
            console.log("statBar: ", statBar)
            console.log("xzData: ", statAxes)
            console.log("Max step: ", Math.round((Math.max.apply(null, ymax)/1000)*1000))
            console.log("Max points: ", Math.round((Math.max.apply(null, pymax)/100)*100))
            console.log("STeps: ", tempArray4)
            pointsYMax = Math.max.apply(null, pymax)
            var temp = pointsYMax.toString().length;
            if (temp>0){
              pointsYMax = (Math.floor(pointsYMax/Math.pow(10, temp-1))+1)*Math.pow(10,temp-1)
            }
            else{
              pointsYMax = 100
            }
            stepsYMax = Math.max.apply(null, ymax)
            temp = stepsYMax.toString().length;
            if (temp>1){
              stepsYMax = (Math.floor(stepsYMax/Math.pow(10, temp-1))+1)*Math.pow(10,temp-1)
            }
            else{
              stepsYMax = 100
            }
            
            if (this.state.statValue!=1){
              this.setState({yAxisRange: [0, pointsYMax], yLabel: 'Points'})
            }
            else {
              this.setState({yAxisRange: [0, stepsYMax], yLabel: 'Steps'})
            }
            if (statBar.length > 7){
              console.log("inside statbar length check!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!", this.state.graphSize)
              newGraphSize= this.state.defaultGraphSize + statBar.length*40
            }
            else{
              newGraphSize = this.state.defaultGraphSize
            }
            console.log("new graph size::::::::::::::::::::::::::::::::::::::::: ", newGraphSize)
            this.setState({
              dailySteps: tempArray1, 
              dailyPoints: tempArray2, 
              stepsWalked: results.rows.item(0)['dailysteps'],
              pointsEarned: results.rows.item(0)['dailypoints'],
              xyData: statAxes,
              graphSize: newGraphSize,
              present: 'Today'
            });
          });
        });
        console.log("value of x******************************: ", x)
      if (x==0){
        console.log("points selected****************************************************")
          this.setState({yAxisRange: [0, pointsYMax], xdata: statPoints})
        }
        else{
          console.log("steps selected****************************************************")
          this.setState({xdata: statBar,yAxisRange: [0, stepsYMax]})
        }
      }
      catch(err) {
        console.log(`----> (ERROR INSIDE COMPOUNT DID MOUNT) <----`, err);
      } 
      

      }


      _1week()
      {
        
        var tempArray1 = [0,0,0,0,0,0,0]
        var tempArray2 = [0,0,0,0,0,0,0]
        var tempArray3 = ['0','0','0','0','0','0','0']
        var tempArray4 = [0,0,0,0,0,0,0,0]
        var tempArray5 = [0,0,0,0,0,0,0,0]
        var stepSum = 0
        var pointSum = 0
        var statBar = []
        var statAxes = []
        var statPoints = []
        var newGraphSize = 0
        var ymax = []
        var pymax = []
        var pointsYMax = 0
        var stepsYMax = 0
        this.setState({stepsWalkedText: "steps walked these weeks",
                        pointsEarnedText: "points earned these weeks",
                        paddingSteps: '3%',
                        paddingPoints: '2%'})
        console.log("temparray1: ", tempArray1)
          db.transaction(tx => {
            tx.executeSql('select sum(dailysteps) as dailysteps, sum(dailypoints) as dailypoints, week, day, month, year from userstatdetails group by week order by year desc, month desc, week desc, day desc', [], (tx, results) => {
              console.log("results: ", results.rows.item(0))
              for (let i=0;i<results.rows.length;++i){
                tempArray1[6-i] = results.rows.item(i)['dailysteps']
                tempArray2[6-i] = results.rows.item(i)['dailypoints']
                ymax.push(results.rows.item(i)['dailysteps'])
                pymax.push(results.rows.item(i)['dailypoints'])

                if (results.rows.item(i)['month']>=10){
                  tempArray3[6-i] = results.rows.item(i)['day'].toString() + '.' +results.rows.item(i)['month'] + '.'
                  statAxes.push({'x': results.rows.item(i)['day'].toString() + '.' + results.rows.item(i)['month'] + '.' , 'y': results.rows.item(i)['dailysteps']})
              }
              else {
                tempArray3[6-i] = results.rows.item(i)['day'].toString() + '.0' +results.rows.item(i)['month'] + '.'
                statAxes.push({'x': results.rows.item(i)['day'].toString() + '.0' + results.rows.item(i)['month'] + '.' , 'y': results.rows.item(i)['dailysteps']})
              }
              statBar.push({'x': i, 'y': results.rows.item(i)['dailysteps']})
              statPoints.push({'x': i, 'y': results.rows.item(i)['dailypoints']})
                tempArray4[i] = results.rows.item(i)['dailysteps']
                tempArray5[i] = results.rows.item(i)['dailypoints']
                stepSum = stepSum + results.rows.item(i)['dailysteps']
                pointSum = pointSum + results.rows.item(i)['dailypoints']
                console.log("temp array in week: ", tempArray3)
              }
              console.log("temparray1: ", tempArray1)
              console.log("ymax in weeekly", Math.round(Math.max.apply(null, ymax)/1000)*1000)
              pointsYMax = Math.max.apply(null, pymax)
              var temp = pointsYMax.toString().length;
              if (temp>0){
                pointsYMax = (Math.floor(pointsYMax/Math.pow(10, temp-1))+1)*Math.pow(10,temp-1)
              }
              else{
                pointsYMax = 100
              }
              stepsYMax = Math.max.apply(null, ymax)
              temp = stepsYMax.toString().length;
              if (temp>1){
                stepsYMax = (Math.floor(stepsYMax/Math.pow(10, temp-1))+1)*Math.pow(10,temp-1)
              }
              else{
                stepsYMax = 100
              }

              if (this.state.statValue!=1){
                this.setState({yAxisRange: [0, pointsYMax], yLabel: 'Points'})
              }
              else {
                this.setState({yAxisRange: [0, stepsYMax], yLabel: 'Steps'})
              }
              if (statBar.length <= 7){
                newGraphSize = this.state.defaultGraphSize
              }
              else {
                newGraphSize = this.state.defaultGraphSize + statBar.length*35
              }
              this.setState({dailySteps: tempArray1, 
                dailyPoints: tempArray2, 
                stepsWalked: stepSum,
                pointsEarned: pointSum,
                graphSize: newGraphSize,
                xyData: statAxes,
                present: 'This week'})
            });
          });
          if (this.state.statValue!=1){
            this.setState({xdata: statPoints})
          }
          else{
            this.setState({xdata: statBar})
          }

      }

      _1month()
      {
        var tempArray1 = [0,0,0,0,0,0,0]
        var tempArray2 = [0,0,0,0,0,0,0]
        var tempArray3 = ['0','0','0','0','0','0','0']
        var tempArray4 = [0,0,0,0,0,0,0,0]
        var tempArray5 = [0,0,0,0,0,0,0,0]
        var stepSum = 0
        var pointSum = 0
        var statBar = []
        var statAxes = []
        var statPoints = []
        var newGraphSize = 0
        var ymax = []
        var pymax = []
        var pointsYMax = 0
        var stepsYMax = 0
        var months = {
          '0' : 'xx',
          '1' : 'Jan',
          '2' : 'Feb',
          '3' : 'Mar',
          '4' : 'Apr',
          '5' : 'May',
          '6' : 'Jun',
          '7' : 'Jul',
          '8' : 'Aug',
          '9' : 'Sep',
          '10' : 'Oct',
          '11' : 'Nov',
          '12' : 'Dec'
      }
        this.setState({stepsWalkedText: "steps walked these months",
                      pointsEarnedText: "points earned these months",
                      paddingSteps: '0%',
                      paddingPoints: '0%'})
        console.log("temparray1: ", tempArray1)
          db.transaction(tx => {
            tx.executeSql('select sum(dailysteps) as dailysteps, sum(dailypoints) as dailypoints, month, year, timeid from userstatdetails group by month order by statid desc limit 7', [], (tx, results) => {
              console.log("results: ", results.rows.item(0))
              for (let i=0;i<results.rows.length;++i){
                tempArray1[6-i] = results.rows.item(i)['dailysteps']
                tempArray2[6-i] = results.rows.item(i)['dailypoints']
                tempArray3[6-i] = months[results.rows.item(i)['month']] 
                tempArray4[7-i] = results.rows.item(i)['dailysteps']
                tempArray5[7-i] = results.rows.item(i)['dailypoints']
                stepSum = stepSum + results.rows.item(i)['dailysteps']
                pointSum = pointSum + results.rows.item(i)['dailypoints']
                ymax.push(results.rows.item(i)['dailysteps'])
                pymax.push(results.rows.item(i)['dailypoints'])
                statAxes.push({'x': months[results.rows.item(i)['month']] + ' ' + results.rows.item(i)['year'], 'y': results.rows.item(i)['dailysteps']})
                statBar.push({'x': i+1, 'y': results.rows.item(i)['dailysteps']})
                statPoints.push({'x': i+1, 'y': results.rows.item(i)['dailypoints']})
                console.log("sum(dailysteps): ",results.rows.item(i) )
              }
              console.log("xdata in months", statBar)
              console.log("axes in month", statAxes)
              pointsYMax = Math.max.apply(null, pymax)
              var temp = pointsYMax.toString().length;
              if (temp>0){
                pointsYMax = (Math.floor(pointsYMax/Math.pow(10, temp-1))+1)*Math.pow(10,temp-1)
              }
              else{
                pointsYMax = 100
              }
              stepsYMax = Math.max.apply(null, ymax)
              temp = stepsYMax.toString().length;
              if (temp>1){
                stepsYMax = (Math.floor(stepsYMax/Math.pow(10, temp-1))+1)*Math.pow(10,temp-1)
              }
              else{
                stepsYMax = 100
              }

              if (this.state.statValue!=1){
                this.setState({yAxisRange: [0, pointsYMax], yLabel: 'Points'})
              }
              else {
                this.setState({yAxisRange: [0, stepsYMax], yLabel: 'Steps'})
              }
              console.log("Points max in month", pointsYMax)
              if (statBar.length <= 7){
                newGraphSize = this.state.defaultGraphSize
              }
              else {
                newGraphSize = this.state.defaultGraphSize + statBar.length*35
              }
              console.log("temparray3 after for loop: ", tempArray3)
              this.setState({dailySteps: tempArray1, 
                dailyPoints: tempArray2, 
                stepsWalked: stepSum,
                pointsEarned: pointSum,
                graphSize: newGraphSize,
                xyData: statAxes,
                present: 'This month'
          })
            });
          });
          if (this.state.statValue!=1){
            console.log("inside month points if")
            console.log("new points range", pointsYMax)
            this.setState({xdata: statPoints})
          }
          else{
            this.setState({xdata: statBar})
          }        
      }

      componentDidMount() {
        this._daily(this.state.statValue)
      }
      _challenges=() => {
        this.setState({menuView: true})
    }

      changeButtonColor1= () => {
        this.setState({ opacity1: 1,opacity2: 0.5, opacity3: 0.5}); 
      }
      changeButtonColor2= () => {
        this.setState({ opacity2: 1,opacity1: 0.5, opacity3: 0.5});  
      }
      changeButtonColor3= () => {
        this.setState({ opacity3: 1,opacity2: 0.5, opacity1: 0.5});  
      }
      changeButtonColor4= () => {
        this.setState({ buttonColor4: 'rgb(254, 170, 58)', buttonColor2: '#7b899b', buttonColor1: '#7b899b', buttonColor3: '#7b899b' }); 
      }

      statValueStepButton= () => {
        this.setState({statValue: 1, stepsButton: 'bold', pointButton: 'normal', stepsButtonColor: 'rgb(228, 216, 197)', pointButtonColor: 'white', stepsLineColor: 'rgb(228, 216, 197)', pointLineColor: 'white', opacity1: 1,opacity2: 0.5, opacity3: 0.5})
        this._daily(1)
      }

      statValuePointButton = () => {
        console.log("button clicked")
        this.setState({statValue: 0, stepsButton: 'normal', pointButton: 'bold', stepsButtonColor: 'white', pointButtonColor: 'rgb(228, 216, 197)', stepsLineColor: 'white', pointLineColor: 'rgb(228, 216, 197)', opacity1: 1,opacity2: 0.5, opacity3: 0.5})
        this._daily(0)
      }
      buttonlog= () => {
        console.log("button clicked")
      }

    constructor(props) {
        super(props);
        
        this.state = {
            xdata:[{'x': 1, 'y': 1}],
            ydata:[],
            xyData: [{'x': 1, 'y': 1}],
            statData:[{}],
            stepsWalked: 10,
            pointsEarned: 10,
            position: "flex-end",
            opacity1: 1,
            opacity2: .5,
            opacity3: .5,
            stepsButton: 'bold',
            stepsButtonColor:  'rgb(228, 216, 197)',
            statValue: 1,
            pointButtonColor: 'white',
            pointButton: 'normal',
            marginPosition: '0%',
            dailySteps: [0,0,0,0,0,0,0],
            dailyPoints: [0,0,0,0,0,0,0],
            dailyStepsY: [0,0,0,0,0,0,0,10],
            dailyPointsY: [0,0,0,0,0,0,0,10],
            barData: [0,0,0,0,0,0,0],
            stepsWalkedText: "steps walked",
            pointsEarnedText: "points earned",
            paddingSteps:'18%',
            paddingPoints: '18%',
            defaultGraphSize: this._normalize(400),
            graphSize : this._normalize(400),
            yAxisRange : [0,100],
            present: 'Today',
            yLabel: 'Steps',
            stepsLineColor: 'rgb(228, 216, 197)',
            pointLineColor: 'white',
        };
        console.log("Global user id: ", uid)
        this.dimensions = {window, screen};
        this.offsetby = 10;
        this.hsize = this.dimensions.window.width - this.offsetby;
        this.boxWidth = this.hsize - 60;
        this.boxHeight = 50;
        this.buttonWidth = this.boxWidth - wp('50%')
        var p_total = 0;
        var moveMargin = 0
        try {
          db.transaction(tx => {
            tx.executeSql('select sum(dailysteps) as dailysteps, sum(dailypoints) as dailypoints, week, year from userstatdetails group by week order by week desc', [], (tx, results) => {
            moveMargin = (results.rows.item(0)['dailysteps']/70000)*20
            if (moveMargin < 20){
              console.log("movemargin: ", moveMargin.toString()+"%")
              this.setState({marginPosition: moveMargin.toString()+"%"})
              console.log("marginposition: ", this.state.marginPosition)
            }
            else{
              this.setState({marginPosition: "20%"})
            }
            });
    
        });
      }
      catch(err) {
        console.log(`----> (ERROR) <----`, err);
      }  
      
    }


    render() {
        const fill = 'rgb(254, 170, 58)'
        const data = this.state.xdata;
        const xvalue=[10,20,30,40,50,60,70]
            const axesSvg = { fontSize: 10, fill: 'white' };
            const verticalContentInset = { top: 10, bottom: 10 }
            const xAxisHeight = 30
            const tempValues = ['Mon', 'b', 'c', 'd', 'e', 'f', 'g']

            /* const ydata = [0, 200, 400, 600, 800, 1000, 1200, 1400, 1600] */
            console.log("xdata: ", this.state.xdata)
            console.log("rendered y axis: ", this.state.ydata)
            console.log("rendered barddata: ", this.state.barData)
            const graphData = this.state.xdata.length == 0 ? [{'x': 1, 'y': 1}] : this.state.xdata
            console.log("graphData: ", graphData)
/***************************************************************************************************************** */
        return (
            <ImageBackground
            source={back_image}
            style={{width: '100%', height: '100%'}}>
            <NavigationEvents
            onDidFocus={() => {this.componentDidMount()}}
            />
            <ScrollView>
                    <View style={{flexDirection: 'row', alignItems: 'center', paddingTop: '5%'}}>
                        <TouchableOpacity style={{paddingLeft: '20%', width: this._normalize(150), alignItems: 'center'}} onPress={() => {this.statValueStepButton()}}>
                            <Text allowFontScaling={false} style={{color: this.state.stepsButtonColor, fontWeight: this.state.stepsButton, fontSize: this._normalize(15)}}>        Steps        </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ width: this._normalize(150), alignItems: 'center'}} onPress={() => {this.statValuePointButton()}}>
                            <Text allowFontScaling={false} style={{color: this.state.pointButtonColor, fontWeight: this.state.pointButton, fontSize: this._normalize(15)}}>        Points        </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{flexDirection: 'row', paddingTop: '2%'}}>
                    <View style={{flex: 1, height: 4, backgroundColor: this.state.stepsLineColor}} />
                    <View style={{flex: 1, height: 4, backgroundColor: this.state.pointLineColor}} />
                    </View>
                    <View style={{ height: 10, width : '90%',  flexDirection: 'row', marginTop: '5%', marginLeft: wp('3%')}}>
                        <TouchableOpacity activeOpacity = { 1 }  onPress={() => {this._daily(),this.changeButtonColor1()}}>
                                <Image source={para} style={{ resizeMode: "contain",alignItems: "center", justifyContent: "center" ,width: this.buttonWidth, height: wp('6%'), opacity: this.state.opacity1}}/>
                                <Text allowFontScaling={false} style={{fontSize: this._normalize(13), position: "absolute",color: "white", marginLeft: '35%'}}>Daily</Text>
                        </TouchableOpacity>
                        <TouchableOpacity activeOpacity = { 1 } onPress={() => {this._1week(),this.changeButtonColor2()}} style={{left: -wp('3%')}} > 
                                <Image source={para} style={{ resizeMode: "contain",alignItems: "center",justifyContent: "center" ,width: this.buttonWidth, height: wp('6%'), opacity: this.state.opacity2}}/>
                                <Text allowFontScaling={false} style={{fontSize: this._normalize(13),  position: "absolute",color: "white", marginLeft: '35%'}}>Weekly</Text>
                        </TouchableOpacity>
                        <TouchableOpacity activeOpacity = { 1 } onPress={() => {this._1month(),this.changeButtonColor3()}} style={{left: -wp('6%')}}>  
                                <Image source={para} style={{ resizeMode: "contain",alignItems: "center",justifyContent: "center" ,width: this.buttonWidth, height: wp('6%'), opacity: this.state.opacity3}}/>
                                <Text allowFontScaling={false} style={{fontSize: this._normalize(13),  position: "absolute",color: "white", marginLeft: '30%'}}>Monthly</Text>   
                        </TouchableOpacity>
                    </View>
                    <View style={{height: '5%'}}/>
                    <View style={{ width: '100%', height: '40%'}} >
                    <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                    
                        <SlideBarChart
                        scrollable
                        style={{ marginTop: 64 }}
                        shouldCancelWhenOutside={false}
                        alwaysShowIndicator={false}
                        data= {this.state.xdata}
                        barSelectedColor= 'rgb(30,144,255)'
                        fillColor= 'rgb(254, 170, 58)'
                        style={{backgroundColor: "transparent"}}
                        axisWidth={32}
                        axisHeight={16}
                        width={this.state.graphSize}
                        height={250}
                        barWidth={10}
                        barSpacing={15}
                        paddingTop={50}
                        yRange={this.state.yAxisRange}
                        yAxisProps={{
                        numberOfTicks: 4,
                        markAverageLine: 'true',
                        showAverageLine: 'true',
                        axisAverageMarkerStyle: {fontSize: this._normalize(11), color: 'red'},
                        averageLineColor: 'red',
                        horizontalLineColor: 'rgb(105,105,105)',
                        verticalLineColor: 'rgb(105,105,105)',
                        axisLabel: this.state.yLabel,
                        axisLabelAlignment: 'aboveTicks',
                        axisLabelStyle: {fontSize: this._normalize(11), color: 'white'},
                        axisMarkerStyle: {fontSize: this._normalize(11), color: 'white'}
                        }}
                        xAxisProps={{
                        axisMarkerLabels: this.state.xyData.map(item => item.x.toString()),

                        specialStartMarker: this.state.present,
                        adjustForSpecialMarkers: true,

                        minimumSpacing: 2,
                        }}
                        toolTipProps={{
                        backgroundColor: '#d6d2d295',
                        toolTipTextRenderers: [
                            ({ selectedBarNumber }) => ({
                            text: graphData[selectedBarNumber].y.toString(),
                            }), 
                            () => ({ text: 'Value' }),
                        ],
                        }}
                    />
                    </ScrollView>
                </View>
                <View style= {{flexDirection:'column',flex:1, justifyContent: 'center', alignItems: 'center'}}>
                        <View style={ {flexDirection: 'row', width: this.boxWidth, height: this.boxHeight, backgroundColor: '#d6d2d295', marginBottom: '3%', marginTop: '4%',borderRadius:10, borderWidth: 1, borderColor: '#d6d2d295'}} >
                                <Text allowFontScaling={false} style={{textAlign: 'left', marginLeft: '4%', fontWeight: "bold", fontFamily: 'sans-serif', fontSize: this._normalize(25), width: 110}}>{this.state.stepsWalked}</Text>  
                                <Text allowFontScaling={false} style={{textAlign: 'right', marginTop: '3%', fontFamily: 'sans-serif', fontSize: this._normalize(13)}}>{this.state.stepsWalkedText}</Text>           
                        </View>
                        <View style={{flexDirection: 'row', width: this.boxWidth, height: this.boxHeight, backgroundColor: '#d6d2d295', borderRadius:10, borderWidth: 1, borderColor: '#d6d2d295'}} >
                            <Text allowFontScaling={false} style={{textAlign: 'left', marginLeft: '4%', fontWeight: "bold", fontFamily: 'sans-serif', fontSize: this._normalize(25), width: 110}}>{this.state.pointsEarned}</Text>  
                            <Text allowFontScaling={false} style={{textAlign: 'left', paddingTop: '3%', fontFamily: 'sans-serif', fontSize: this._normalize(13)}}>{this.state.pointsEarnedText}</Text> 
                            {this.cityLocation(),console.log('position',  this.state.position)}
                        </View>
                            {/* <View style={[styles.container, { justifyContent: 'this.state.position', marginLeft: 30,flexDirection: 'row'},{width: screenWidth, height: 38, marginBottom: '3%'}]}> */}
                            <View style={[styles.container, { justifyContent: 'flex-end', marginLeft: this.state.marginPosition,flexDirection: 'row'},{width: screenWidth, height: 200, marginBottom: '3%'}]}>
                                <Image source={city} style = {{ width: 500, height: 300}} />     
                            </View>
                </View>
                </ScrollView>
        </ImageBackground>
        )


    }
 
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        paddingTop: 64,
        paddingBottom: 32
      },
    button: {
      backgroundColor: "#DDDDDD"
    },
    line: {
      borderBottomWidth: 1,
      borderColor: '#f48a92'
    },
    parallelogram: {
      width: 150,
      height: 100
    },
    responsiveBox: {
      width: wp('77%'),
      height: hp('6%'),
      flexDirection: 'column',
      justifyContent: 'space-around',
      backgroundColor: '#d6d2d295', 
      borderRadius:10, 
      borderWidth: 1, 
      borderColor: '#d6d2d295' 
    }   
  });

  