import React from 'react'

import { ImageBackground, Image, Button, TouchableOpacity } from 'react-native';
import { StyleSheet, View, Text, TextInput, Dimensions, Alert, PixelRatio} from 'react-native';
import { NavigationEvents } from 'react-navigation';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import DeviceInfo from 'react-native-device-info';
import profile from './images/profile.png'
import as_icon from './images/as_icon.png'
import back_image from './src/backgrounds/app_background.png'
import { ScrollView } from 'react-native-gesture-handler';
import HorizontalProgress from './src/components/horizontalProgressBar/HorizontalProgress';
import HorizontalProgressDynamic from './src/components/horizontalProgressBar/HorizontalProgressDynamic'
import challenge_icon from './src/icons/challenge_complete.png'
import consecutive_challenge from './src/icons/consecutive_challenge.png'
import point_challenge from './src/icons/point_challenge.png'
import step_challenge from './src/icons/step_challenge.png'
import challenge_completed from './src/icons/challenge_complete.png'
import dot from './src/icons/dot.png'
import defaultImage from './android/app/src/main/assets/defaultImage.png'
const window = Dimensions.get("window");
const screen = Dimensions.get("screen");
const wscale = window.width / 320;

export default class challenges extends React.PureComponent {

 
componentDidMount(){
this._getChallenges()
}

_normalize(size) {
    const newSize = size * wscale 
    return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2
    
  }
_getChallenges(){
    var date = new Date().getDate();
    var month = new Date().getMonth() + 1; 
    var year = new Date().getFullYear();
    
    if (date+"-"+month+"-"+year === "10-6-2020"){
        console.log("Found Todays date: "+date+"-"+month+"-"+year)
    }
    db.transaction(tx => {
        tx.executeSql('select nickname, userid, useruniqueid, profileimage from userdetails', [], (tx, results) => {
        console.log("initial resultsaffected: ", results.rowsAffected)
        this.setState({userName: results.rows.item(0)['nickname'], avatarSource: {"uri":results.rows.item(0)['profileimage']}})
        });

    });
    db.transaction(tx => {
        tx.executeSql('select id, progress from userchallenges', [], (tx, results) => {
        console.log("initial challenge list resultsaffected: ", results.rowsAffected)
        this.setState({challengeProgress: [results.rows.item[0]['progress'], results.rows.item[1]['progress'],
                        results.rows.item[2]['progress'], results.rows.item[3]['progress']]})
        });

    });
    db.transaction(tx => {
        tx.executeSql('select sum(dailypoints) as dailypoints, sum(dailysteps) as dailysteps from userstatdetails', [], (tx, results) => {
          console.log("Total Points Earned in Challenges: ", results.rows.item(0)['dailypoints'])
          

          this.setState({
            challengeProgress: [0,results.rows.item(0)['dailypoints'],results.rows.item(0)['dailysteps'],0]
          });
        });
      });
      db.transaction(tx => {
        tx.executeSql('select userlevel from userdetails', [], (tx, results) => {
        console.log("userlevel: ", results.rows.item(0)['userlevel'])
        let y;
        if (results.rows.item(0)['userlevel'] == 0)
        {   
            let a = Math.floor((5+(25+(20*this.state.challengeProgress[1]))**0.5)/10)
            this.setState({level: a})
            console.log("Level Easy: ", this.state.level)
            let x = a + 1
            y = 5*x*(x-1)
        }
        else if (results.rows.item(0)['userlevel'] == 1)
        {
            let a = Math.floor((6+(36+(24*this.state.challengeProgress[1]))**0.5)/12)
            this.setState({level: a})
            console.log("Level Medium: ", this.state.level)
            let x = a + 1
            y = 6*x*(x-1)
            console.log("Next level Point ", y)
        }
        else if (results.rows.item(0)['userlevel'] == 2){
            let a = Math.floor((7+(49+(28*this.state.challengeProgress[1]))**0.5)/14)
            this.setState({level: a})
            console.log("Level Max: ", this.state.level)
            let x = a + 1
            y = 7*x*(x-1)
        }
        console.log("Total points:  ", this.state.challengeProgress[1])
        let c = (this.state.challengeProgress[1]*100)/y
        console.log("level percentage:  ", c)
        this.setState({levelPrcnt: c})
        db.transaction(tx => {
            tx.executeSql('update userdetails set levelprcnt=?, level=?', [this.state.levelPrcnt, this.state.level], (tx, results) => {
              console.log("levelprcnt updated in local database: ", results.rowsAffected)
            })
          })
        });

    });
/*     fetch(hostip+'challengeList?uid='+uid,{
        method: 'GET'
      })
      .then(res => res.json())
      .then((data) => {
        this.setState({challengeProgress: [data.challengeValues[0].target,data.challengeValues[1].target,
            data.challengeValues[2].target,data.challengeValues[3].target]})

       console.log("challenges.target ", data.challengeValues[1].target)
       db.transaction(tx => {
           for ( let i =0; i<4; i++) {
        tx.executeSql('update userchallenges set progress=? where id=?', [this.state.challengeProgress[i], i+1], (tx, results) => {
          console.log("challenges local results: ", results.rowsAffected)
        });
        }
      });
      })
      .catch(error => {
        console.log('There has been a problem with your fetch operation: ' + error.message);
        
        }) */
}

_challenges=() => {
    this.setState({menuView: true})
}

_exception(appfunction='unknown', errorH='unknown', errorM='unknown'){
    let devicemodel = DeviceInfo.getModel();
    let sysversion = DeviceInfo.getSystemVersion()
    let devicebrand = DeviceInfo.getBrand()
    let sysos = DeviceInfo.getSystemName()
    console.log("Device Model: ", devicemodel)
    console.log("System Version: ", sysversion)
    console.log("Brand: ", devicebrand)
    console.log("OS: ", sysos)
    console.log("Function: ", appfunction)
    fetch(hostip+'exceptions', {
    method: 'POST',
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        userId: uid,
        brand: devicebrand,
        device: devicemodel,
        deviceOs: sysos,
        softwareVersion: sysversion,
        method: appfunction,
        errorHeader: errorH,
        errorMessage: errorM
    })
    })
    .then((response) => response.json())
    .then((responseData) => {
        console.log("Response",responseData )
        if (responseData.response == 'Success')
        {
            console.log("Error updated in server")
        }
    })
    .catch(error => {
    console.log('Error: ' + error.message);
    }) 
}
_events=() => {
    this.setState({menuView: false})
    console.log("inside events tab: ", hostip, " ", uid)
    this._exception("events", "head", "message")

}
constructor(props) {
    super(props);
    this.state = {
        userName: '',
        menuView: true,
        challengeProgress: [0,0,0,0],
        challenges: [{"id": "1", "name": "Complete the 3 daily challenges", "desc": "7 days in a row", "target": "7", "uri":"asset:/consecutive_challenge.png"},
                    {"id": "2", "name": "Earn 20,000 pts", "desc": '', "target": "20000", "uri": "asset:/point_challenge.png"},
                    {"id": "3", "name": "Walk 200,000", "desc": '', "target": "200000", "uri": "asset:/step_challenge.png"},
                    {"id": "4", "name": "Complete 5 non daily challenges", "desc": '', "target": "5", "uri": "asset:/challenge_complete.png"}],
        avatarSource: {"uri": "asset:/defaultImage.png"},
        level: 1,
        levelPrcnt: 0
    }
    this.dimensions = {window, screen};
    this.offsetby = 10;
    this.hsize = this.dimensions.window.width - this.offsetby;
    this.boxWidth = this.hsize - 20;
    this.boxHeight = 90;
    
};
    render() {



/***************************************************************************************************************** */
        return (
            <ImageBackground
            source={back_image}
            style={{width: '100%', height: '100%'}}>
                <ScrollView>
                    <View style = {{flexDirection: 'column', alignItems: 'center'}}>
                        <NavigationEvents
                        onDidFocus={() => {this._getChallenges()}}
                        />
                        <View style={{alignItems: 'center', marginTop:'10%'}}>
                            <Image source={this.state.avatarSource} style = {{width:100,height:100, borderRadius: 100/2, borderWidth: 5, borderColor: 'white'}}/>
                            <Text allowFontScaling={false} style={{alignItems: 'center', fontSize: this._normalize(14), paddingTop: '2%', color: 'white'}}>{this.state.userName}</Text>
                        </View>
                        <View style={{alignItems: "center", justifyContent: "center"}}>
                            <HorizontalProgressDynamic
                            boxWidth={this.boxWidth-this._normalize(100)}
                            boxHeight={25}
                            leftEdge="10"
                            rightEdge="10"
                            bgColor="#ffffff"
                            progressColor="rgb(255,152,75)"
                            progressPcnt={this.state.levelPrcnt}
                            />
                            <Text allowFontScaling={false} style={{color: 'white', fontWeight: 'bold', fontSize: this._normalize(15), color: 'rgb(254, 170, 58)'}}>You are at Level {this.state.level}                
                            </Text>
                        </View>
                        <View style={{flexDirection: 'row', paddingLeft: '0%', paddingTop: '5%'}}>
                            <TouchableOpacity onPress={() => {this._challenges()}}>
                                <Text allowFontScaling={false} style={{color: 'white', fontWeight: 'bold', fontSize: this._normalize(15)}}>Challenges</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{paddingLeft: '20%'}} onPress={() => {this._events()}}>
                                <Text allowFontScaling={false} style={{color: 'white', fontWeight: 'bold', fontSize: this._normalize(15)}}>Events</Text>
                            </TouchableOpacity>
                        </View>
                        {this.state.menuView ? (
                        <View style={{width: '100%', alignItems: 'center'}}>
                            {
                            this.state.challenges.map((item, index) => (
                            <View key = {item.id} style={{width: this.boxWidth, height: this.boxHeight, backgroundColor: 'rgb(254, 170, 58)', marginTop: '2%', borderRadius: 10}}>
                                    <Text allowFontScaling={false} style={{color: 'white', marginLeft: '5%', fontSize: this._normalize(12)}}>{item.name}</Text>
                                    
                                    <Text allowFontScaling={false} style={{color: 'white', marginLeft: '5%', fontSize: this._normalize(12)}}>{item.desc}</Text>

                                    <View style={{width: 120, marginLeft: '32%'}}>
                                    <Text allowFontScaling={false} style={{color: 'white', textAlign: 'right', fontSize: this._normalize(12) }}>{this.state.challengeProgress[item.id-1]}/{item.target}</Text>
                                    </View>
                                    <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
                                        <HorizontalProgress
                                        boxWidth={250}
                                        boxHeight={25}
                                        bgColor="white"
                                        progressColor='rgb(65, 84, 105)'
                                        progressPcnt={(this.state.challengeProgress[item.id-1]/item.target)*100}
                                        edgeCurve={10}
                                        />
                                        <Image source={{uri: item.uri}} style = {{justifyContent: 'center', alignItems: 'center',top: -wp('11%'), width: wp('17%'), height: wp('17%'), resizeMode: 'contain'}}/>
                                    </View>
                                </View>

                                    ))
                                }

                            
                        </View>)
                  :
                  <View>
                    <Text allowFontScaling={false} style={{color: 'rgb(254, 170, 58)', fontSize: this._normalize(18), paddingTop: '30%', fontWeight: 'bold'}}>Fitness events coming soon !</Text>
                  </View>
              }                 
                    </View>
                </ScrollView>
        </ImageBackground>
        )


    }
 
}
const styles = StyleSheet.create ({
    item: {
       flexDirection: 'column',
       width: '100%',
       height: 200,
       marginLeft: '5%',
       marginRight: '10%',
       marginTop: '10%',
       marginBottom: '3%',
       alignItems: 'center',
       position: 'relative'
       
       
    },
    imageStyle: {
        width: '100%',
        height: '100%',
    }
 })


