import React from 'react'

import { ImageBackground, Image, Button, TouchableOpacity, NativeModules } from 'react-native';
import { StyleSheet, View, Text, TextInput, Alert} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator,StackNavigator   } from 'react-navigation-stack';
import { createAppContainer }  from 'react-navigation';
import GoogleFit, { Scopes } from 'react-native-google-fit';
import { openDatabase } from 'react-native-sqlite-storage';
import SQLite from 'react-native-sqlite-storage';
import ImagePicker from 'react-native-image-picker';
import Dates from './src/helpers/Dates';
import defaultImage from './android/app/src/main/assets/defaultImage.png'
import uploadImage from './android/app/src/main/assets/upload.png'
import profile from './profile.png'
import RadioForm from 'react-native-simple-radio-button';
import TabApp from './App'
import back_image from './src/backgrounds/app_background.png'
var rData;


/* var db = openDatabase({ name: 'kwappDatabase.sqlite3' }); */
 
var radio_props = [
    {label: 'Beginner (5000 daily step goal)', value: 0 },
    {label: 'Medium (7500 daily step goal)', value: 1 },
    {label: 'Master (10000 daily step goal)', value: 2 }
  ];

  const options = {
    title: 'Select Avatar',
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };
  
  class registration extends React.Component {

    errorCB(err) {
        console.log("SQL Error: " + err);
      }
       
      successCB() {
        console.log("SQL executed fine");
      }
       
      openCB() {
        console.log("Database OPENED");
      }
      broofa() {
        return 'xxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });
    }

    _uploadImage(){
      console.log("function called")
      ImagePicker.showImagePicker(options, (response) => {
         
          if (response.didCancel) {
            console.log('User cancelled image picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else {
            const source = { uri: response.uri };
          if (source){
              console.log("inside source log")
              }
            // You can also display the image using data:
            // const source = { uri: 'data:image/jpeg;base64,' + response.data };
              console.log("profile image url: ",source)
              console.log("image absolute path ", response.uri)
            this.setState({
              avatarSource: source,
              avatarPath: response.uri
            });
          }
        });
  }

  _googleAuth(){
    // Check for google fit access
    
    // Toasting for trying to login to google fit
    NativeModules.NativeProcess.toast("Please login to your Google account...", NativeModules.NativeProcess.SHORT);

    // For googlefit
    const options = {
      scopes: [
        Scopes.FITNESS_ACTIVITY_READ
      ],
    }

    GoogleFit.authorize(options)
      .then ((authresult) => {
        if (authresult.success) {
          console.log ("GoogleFit auth sucess!");

          // Start Recording subscribtion
          const options = {
            scopes: [
              Scopes.FITNESS_ACTIVITY_READ_WRITE,
            ],
          }

          // subscribe
          GoogleFit.startRecording ((callback) => {
            // This starts the google fit recording if in case its not yet
            // **** Don't know how to stop it though :P
          });

          // Toasting for trying to login to google fit
          NativeModules.NativeProcess.toast("Log in success. Tracking!", NativeModules.NativeProcess.SHORT);
          this._ifAuthSuccess()
        } else {
          console.log (`GoogleFit auth denied! ${authresult.message}`);

          // Toasting for trying to login to google fit
          NativeModules.NativeProcess.toast("Log in Failed.", NativeModules.NativeProcess.SHORT);
        }
      })
      .catch ((error) => {console.log (`GoogleFit auth error! ${error}`)})
  }

  _ifAuthSuccess(){
    console.log("google authentication successfull. Function called")
    db.transaction(tx => {
      /*                                 tx.executeSql('insert into userdetails (nickname) values (?) ', [that.state.tvalue], (tx, results) => {
                                        console.log("initial resultsaffected: ", results.rowsAffected)
                                      }); */
                                      for (let i = 0; i < 7; ++i) {
                                        tx.executeSql('insert into userstat (statid, day, d_pointsearned) values (?,?,?) ', [i+1, 1, 1], (tx, results) => {
                                          console.log("initial resultsaffected: ", results.rowsAffected)
                                        });
                                      }
      
                                    });
                                    db.transaction(tx => {
                                      tx.executeSql('update userlogin set userid=?', [uid], (tx, results) => {
                                        console.log("userlogin updated results: ", results.rowsAffected)
                                      });
                                    });
                                    console.log("that.state.tvalue: ", that.state.tvalue)
                                    db.transaction(tx => {
                                      tx.executeSql('insert into userdetails (nickname,userid,useruniqueid, userlevel, profileimage) values (?,?,?,?,?) ', [that.state.tvalue, uid, usruniqueid, that.state.rvalue, that.state.avatarPath], (tx, results) => {
                                        console.log("initial user resultsaffected: ", results.rowsAffected)
                                      });              
                                    });
                                    db.transaction(tx => {
                                      for (let i = 0; i < 4; ++i) {
                                        tx.executeSql('insert into userchallenges (id, progress) values (?,?) ', [i+1, 1], (tx, results) => {
                                          console.log("initial challenge table resultsaffected: ", results.rowsAffected)
                                        });
                                      }             
                                    });
                                    let today = Dates._ddmmyyyy ();
                                    var year = new Date().getFullYear();
                                    db.transaction(tx => {
                                      tx.executeSql("insert into userstatdetails (dailysteps, dailypoints, timeid, day, week, month, year) values (0, 0, ?, ?, ?, ?, ?)", [today, Dates._getDay(), Dates._getWeek(), Dates._getMonth(), year], (tx, results) => {
                                        // insert sql completed
                                        console.log("Date changed! Insert row. resultsaffected: ", results.rowsAffected);
                                      });
                                    });
                                    that.props.navigation.navigate("Home", {userClass: radio_value});
  }
      componentDidMount() {
        console.log("************************************New Execution************************************")
        /* SQLite.deleteDatabase({name: 'tfayas5', location: 'Library'}); */

              const { db } = this.state;
              var that = this;
              console.log("Starting db execution 1")
              console.log("initialchecks: ", this.state.initialchecks)
              console.log("tvalue: ",this.state.tvalue )
              console.log("rvalue: ",this.state.rvalue )
              let screenValue = 2
              db.transaction(function(tx) {
                tx.executeSql(
                  "SELECT name FROM sqlite_master WHERE type='table' AND name='userlogin'",
                  [],
                  (tx, results) => {
                    console.log("check for login table: ", results.rows.item(0)['name'])
                    
                  },
                );
              });

              db.transaction(function(tx) {
                tx.executeSql(
                  "select loginstatus, userid from userlogin",
                  [],
                  (tx, results) => {
                    console.log("inside execution db1: ", results.rows.item(0))
                   if (!results.rows.item(0)){
                     console.log("no data")
                     that.setState({initialchecks: false})
                     screenValue=0
                   }
                    
                    if (results.rows.item(0)['loginstatus'] == 1)
                    {
                      uid = results.rows.item(0)['userid']
                      that.props.navigation.replace("Home",{userClass : "Master"})
                    }
                    else {
                      screenValue = 2
                      this.setState({initialchecks: false})
                    }
                  },
                );
              });
      }


      register_user = () => {
      
        var that = this;
        var radio_value;
        //alert(user_name, user_contact, user_address);
        var usruniqueid = this.broofa();
        console.log("Starting db execution")
              db.transaction(function(tx) {
                tx.executeSql(
                  "insert into userlogin (userid, loginstatus) values (1,1)",
                  /* "select * from uselogin", */
                  [],
                  (tx, results) => {
                    
                    if (results.rowsAffected > 0) {
                      
                      console.log("results.rowsAffected: ", results.rowsAffected)
                      console.log("this.state.tdata: ", that.state.tvalue)
                      console.log("this.state.rdata: ", that.state.rvalue)
                      if (that.state.rvalue == 0) {
                        radio_value = that.state.rvalue
                    } else if (that.state.rvalue == 1) {
                      console.log("inside if 2")
                      radio_value = that.state.rvalue
                    }
                    else {
                      console.log("inside if 3")
                      radio_value = that.state.rvalue
                    }
                        console.log("inside try")
                      fetch(hostip+'register', {
                      method: 'POST',
                      headers: {
                          Accept: 'application/json',
                          'Content-Type': 'application/json',
                      },
                      body: JSON.stringify({
                          userLevel: that.state.rvalue+1,
                          nickname: that.state.tvalue,
                          userStatus: 1,
                          userUniqueId: usruniqueid
                      }),
                      
                      })
                      .then((response) => response.json())
                      .then((responseData) => {
                          console.log("Response",responseData )
                          console.log("Response",responseData.userId)
                          console.log("Response",responseData.response)
                          console.log("Response",responseData["response"])
                          uid = responseData.userId;
                          if (responseData.response == 'Success')
                          {
                              console.log("inside success if")
                              db.transaction(tx => {
/*                                 tx.executeSql('insert into userdetails (nickname) values (?) ', [that.state.tvalue], (tx, results) => {
                                  console.log("initial resultsaffected: ", results.rowsAffected)
                                }); */
                                for (let i = 0; i < 7; ++i) {
                                  tx.executeSql('insert into userstat (statid, day, d_pointsearned) values (?,?,?) ', [i+1, 1, 1], (tx, results) => {
                                    console.log("initial resultsaffected: ", results.rowsAffected)
                                  });
                                }

                              });
                              db.transaction(tx => {
                                tx.executeSql('update userlogin set userid=?', [uid], (tx, results) => {
                                  console.log("userlogin updated results: ", results.rowsAffected)
                                });
                              });
                              console.log("that.state.tvalue: ", that.state.tvalue)
                              db.transaction(tx => {
                                tx.executeSql('insert into userdetails (nickname,userid,useruniqueid, userlevel, profileimage) values (?,?,?,?,?) ', [that.state.tvalue, uid, usruniqueid, that.state.rvalue, that.state.avatarPath], (tx, results) => {
                                  console.log("initial user resultsaffected: ", results.rowsAffected)
                                });              
                              });
                              db.transaction(tx => {
                                for (let i = 0; i < 4; ++i) {
                                  tx.executeSql('insert into userchallenges (id, progress) values (?,?) ', [i+1, 1], (tx, results) => {
                                    console.log("initial challenge table resultsaffected: ", results.rowsAffected)
                                  });
                                }             
                              });
                              let today = Dates._ddmmyyyy ();
                              var year = new Date().getFullYear();
/*                               db.transaction(tx => {
                                tx.executeSql("insert into userstatdetails (dailysteps, dailypoints, timeid, day, week, month, year) values (0, 0, ?, ?, ?, ?, ?)", [today, Dates._getDay(), Dates._getWeek(), Dates._getMonth(), year], (tx, results) => {
                                  // insert sql completed
                                  console.log("Date changed! Insert row. resultsaffected: ", results.rowsAffected);
                                });
                              }); */
                              that.props.navigation.navigate("Home", {userClass: radio_value});
                          }
                      })
                      .catch(function(error) {
                        if (error){
                          console.log("Error : ", error)
                        Alert.alert(
                          "Registration failed",
                          "Please try again",
                          [
                            { text: "OK", onPress: () => console.log("OK Pressed") }
                          ],
                          { cancelable: false }
                        )
                        db.transaction(function(tx) {
                          tx.executeSql(
                            "delete from userlogin",
                            [],
                            (tx, results) => {
                              console.log("User deleted due to error: ", results.rowsAffected)
                             
                            },
                          );
                        });
                      }
                      });
                    
                    } 
                  }
                );
              });
              
  
      };



     register(){
        this.props.navigation.replace("Home");
    /*         console.log("register function called", global.myvar)
        global.myvar=1
        fetch('http://192.168.0.103:5000/register', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            userId: 1,
            userLevel: 1,
            nickname: 'Maverick',
            userStatus: 1
        }),
        
        })
        .then((response) => response.json())
        .then((responseData) => {
            console.log("Response",responseData  )
            if (responseData = 'Success')
            {
                console.log("inside function if")
                this.props.navigation.navigate("Home");
            }
        })
        ; */
    }


   




    constructor(props) {
      console.log("inside constructor")
      
      super(props);
      
      global.db = SQLite.openDatabase({name: "tfayas199", createFromLocation : "~kwappappdata.sqlite", location: 'Library'}, this.openCB,this.errorCB);
      global.hostip = 'http://52.29.223.120:5151/'
/*       global.hostip = 'http://192.168.0.101:5000/' */
      global.uid=0; 
      this.state = { 
        db, 
        initialchecks: true, 
        loginstatus: 0,
        tvalue:'',
        rvalue: 0,
        avatarSource: defaultImage,
        avatarPath: "asset:/defaultImage.png"
      };


      
    }





        render(){
/***************************************************************************************************************** */
          console.log("inside render")
          console.log("initialchecks render", this.state.initialchecks)
          
        return (
            <ImageBackground
            source={back_image}
            style={{width: '100%', height: '100%'}} >
              
              
              
              {this.state.initialchecks ?  (
                <View></View>)
                  :
                  <View style={{flexDirection:'column', flex:1}}>
                    <View style={{alignItems: 'center', marginTop:'10%'}}>
                      <TouchableOpacity onPress={() => {this._uploadImage()}}>
                        <Image source={this.state.avatarSource} style = {{width:100,height:100, borderRadius: 100/2, borderWidth: 5, borderColor: 'white'}}/>
                        <Image source={uploadImage} style={{width: 25, height: 25, right: -70, top: -15}}/>
                      </TouchableOpacity>
                    </View>
                    <View style={{alignItems: 'center', marginTop:'5%'}}>
                        <Text style={{fontSize:30, color:'rgb(254, 170, 58)'}}> Welcome to KWapp!</Text>
                    </View>
                    <View style={{alignItems: 'center', marginTop:'5%'}}>
                        <Text style={{fontSize:18, color:'white'}}> Start earning points</Text>
                        <Text style={{fontSize:18, color:'white'}}> immediately!</Text>
                    </View>
                    <View style={{alignItems: 'center', marginTop:'5%'}}>
                        <Text style={{fontSize:18, color:'rgb(254, 170, 58)'}}> Create a nickname(optional)</Text>
                        <TextInput style={{ height: 40,width:290, borderColor: 'gray',backgroundColor:'white', borderWidth: 1, borderRadius: 10,textAlign: 'center' }} onChangeText={text => this.setState({tvalue:text})} />
                        </View>
                    <View style={{marginLeft:'2%', marginTop:'5%'}}>
                        <Text style={{fontSize:20, color:'rgb(254, 170, 58)'}}> Please choose a fitness level</Text>
                    </View>
                    <View style={{marginLeft:'2%', marginTop:'1%'}}>
                        <RadioForm
                        radio_props={radio_props}
                        initial={0}
                        buttonColor={'white'}
                        buttonStyle={{buttonInnerColor:'red',buttonOuterColor:'red'}}
                        labelColor={'rgb(254, 170, 58)'}
                        onPress={(value) => {this.setState({rvalue:value})}}
                        />
                    </View>
                    <View style={[{alignItems: 'center',  marginTop:'5%'}]}>
                       {/*  <TouchableOpacity onPress={() => this.props.navigation.replace("Home")} style={{height:40, alignItems:'center', width: 150, backgroundColor: 'rgb(254, 170, 58)',borderRadius: 10}}> */}
                        <TouchableOpacity onPress={() => {this.register_user()}} style={{height:40, alignItems:'center', width: 150, backgroundColor: 'rgb(254, 170, 58)',borderRadius: 10}}>
                        {/* <TouchableOpacity onPress={() => {this._googleAuth()}} style={{height:40, alignItems:'center', width: 150, backgroundColor: 'rgb(254, 170, 58)',borderRadius: 10}}> */}
                            <Text style={{color: 'white',fontSize: 25}}> Done </Text>
                        </TouchableOpacity>
                    </View>
                    
                  </View>
              }
        </ImageBackground>
        ) 
        } 
}  

class HomeScreen extends React.Component {  
    render() {  
      return (  
          <TabApp/>  
      );  
    }  
  } 

const Stack = createStackNavigator({
        Signup: {
          screen: registration,
          navigationOptions: {
            title: 'Home',
            headerShown: false,
            headerLeft: () => null
          }
        },
        Home: {
          screen: HomeScreen,
          navigationOptions: {
            title: 'Home',
            headerShown: false,
            headerLeft: () => null,
            params: {
              userClass: 'jane',
            }        

          }
        }
      },
      {
        initialRouteName: 'Signup',
        
      },
      {
        unmountInactiveRoutes: true
      }
    );

    class regApp extends React.Component {
        
        render() {
          return <Stack />;
        }
      }

  const AppNavigation = createAppContainer(Stack);

  export default AppNavigation;
/*   export default class regApp extends React.Component {
    render() {
      return (
          <AppNavigation/>
      );
    }
  } */
/* const  SimpleAppNavigator = StackNavigator({
    Regist: { screen: registration },
    Homescreen: { screen: Homescreen }
    
  });
  
  const AppNavigation = () => (
    <SimpleAppNavigator  />
  );

 export default class regApp extends React.Component {
  render() {
    return (
        <AppNavigation/>
    );
  }
} */


  const styles = StyleSheet.create ({
    item: {
       flexDirection: 'column',
       width: '100%',
       height: 200,
       marginLeft: '5%',
       marginRight: '10%',
       marginTop: '10%',
       marginBottom: '3%',
       alignItems: 'center',
       position: 'relative'
       
       
    },
    container: {  
        flex: 1,  
        justifyContent: 'center',  
        alignItems: 'center'  
    }
 })