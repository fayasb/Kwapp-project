import React, { Component } from 'react';
import { StyleSheet, View, Button, SafeAreaView, Text, Alert,} from 'react-native';
import { ECharts } from 'react-native-echarts-wrapper';
import { createBottomTabNavigatorr} from 'react-navigation';  
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs'; 
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import TabNavigator from './App'
import { ImageBackground } from 'react-native';

export default class fitness extends Component {
    option = {
        xAxis: {
            type: 'category',
            data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
        },
        yAxis: {
            type: 'value'
        },
        series: [{
            data: [820, 932, 901, 934, 1290, 1330, 1320],
            type: 'bar',
            barWidth: 6
        }]
    };
    _View;get View() {
    return this._View;
  }
set View(value) {
    this._View = value;
  }

        render() {
            return (
    /*           <View style={{flex: 1, flexDirection: 'row'}}>
                <View>
                    <Button
                    title="Press me"
                    onPress={() => Alert.alert('Simple Button pressed')}
                    />
                </View>
            </View> */
    ///////////////////////////////////////////////////////////////////////////////////////////////////
            <ImageBackground
                source={require('./background.png')}
                style={{width: '100%', height: '100%'}}>
            
            <View style={{flex: 1, flexDirection: 'column'}}>
                <View style={{ width: 400, height: 10, justifyContent: 'center', flexDirection: 'row', alignItems: 'center', marginTop: 70}}>
                    <Button
                    title="1 Week"
                    onPress={() => Alert.alert('Simple Button1 pressed')}
                    />
                    <Button
                    title="1 Month"
                    onPress={() => Alert.alert('Simple Button2 pressed')}
                    />
                    <Button
                    title="6 Month"
                    onPress={() => Alert.alert('Simple Button3 pressed')}
                    />
                    <Button
                    title="1 Year"
                    onPress={() => Alert.alert('Simple Button4 pressed')}
                    />
                </View>
                    <View style={styles.chartContainer, {width: '80%', height: '50%', alignSelf : 'center', backgroundColor: 'transparent'}}>
                    <ECharts
                        option={this.option}
                        backgroundColor="rgba(93, 169, 81, 0.3)"
                        opacity="0"
                    />
                    </View>
                <View style= {{flexDirection:'column',flex:1}}>
                    <View style={styles.rectangle, {marginLeft: '15%', width: 250, height: 30, backgroundColor: '#d6d2d2', marginBottom: '3%', borderRadius:10, borderWidth: 1, borderColor: '#d6d2d2'}} >
                        <Text style={{textAlign: 'left', marginLeft: '4%'}}>
                            Steps walked
                        </Text>
                    </View>
                    <View style={styles.rectangle, {marginLeft: '15%', width: 250, height: 30, backgroundColor: '#d6d2d2', marginBottom: '3%', borderRadius:10, borderWidth: 1, borderColor: '#d6d2d2'}} >
                        <Text style={{textAlign: 'left', marginLeft: '4%'}}>
                            Points earned
                        </Text>
                    </View>
                    <View style={styles.rectangle, {marginLeft: '15%', width: 250, height: 38, backgroundColor: '#d6d2d2', marginBottom: '3%', borderRadius:10, borderWidth: 1, borderColor: '#d6d2d2'}} >
                        <Text style={{textAlign: 'left', marginLeft: '4%'}}>
                            CO2 emissions saved by walking
                        </Text>
                    </View>
                </View>
                <View>
                    <TabNavigator/>
                </View>
            </View>
            </ImageBackground>
            );
        }
    }

const styles = StyleSheet.create({
    chartContainer: {
        flex: 1
        
    },
});