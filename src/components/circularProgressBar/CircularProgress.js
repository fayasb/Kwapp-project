import React from 'react';
import Svg, { Circle, Path, Rect } from "react-native-svg";

const bgLayerOpacity = "0.2";
const progressLayerOpacity = "1";


function generateArc(percentage, cx, cy, radius) {

    // console.log(`cprogress: Percentage:${percentage}, cx:${cx}, cy:${cy}, radius:${radius}`);

    if (percentage === 100) percentage = 99.999
    const a = percentage * 2 * Math.PI / 100 // angle (in radian) depends on percentage
    const r = radius // radius of the circle
    var rx = r,
        ry = r,
        xAxisRotation = 0,
        largeArcFlag = 1,
        sweepFlag = 1,
        xadjust = r * Math.sin(a),
        yadjust = r * Math.cos(a),
        x = cx + xadjust,
        y = cy - yadjust
    if (percentage <= 50) {
        largeArcFlag = 0;
    } else {
        largeArcFlag = 1
    }

    return `A${rx} ${ry} ${xAxisRotation} ${largeArcFlag} ${sweepFlag} ${x} ${y}`;
}

const circularProgress = (props) => (
    <Svg width={props.boxWidth} height={props.boxHeight} viewBox={`0 0 ${props.boxWidth} ${props.boxHeight}`}>
        <Circle
            stroke={props.bgColor}
            strokeWidth={props.width}
            strokeOpacity={bgLayerOpacity}
            r={props.r}
            cx={props.cx}
            cy={props.cy}
            fill="none"
        />
        <Path
            stroke={props.progressColor}
            strokeWidth={props.width}
            strokeDasharray={props.dashArray}
            strokeOpacity={progressLayerOpacity}
            d={`M${props.cx} ${(props.cy) - props.r} 
          ${generateArc(props.progressPcnt, props.cx, props.cy, props.r)}`}
        />
        {/* <Rect
            x="1"
            y="1"
            width={props.boxWidth - 2}
            height={props.boxHeight - 2}
            fill="none"
            stroke="red"
            strokeWidth="1"
        /> */}
    </Svg>
);

export default circularProgress;