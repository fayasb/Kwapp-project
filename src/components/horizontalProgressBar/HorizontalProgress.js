import React from 'react';
import Svg, { Rect } from 'react-native-svg';

const heightOffset = 10;
const bgOpacity = "0.6";

function generateProgressOffset(percentage, width) {
  if (percentage > 100) percentage = 100;

  // console.log (`hprogress : ${percentage * width / 100}`);

  return percentage * width / 100;
};

const horizontalProgress = (props) => (
  <Svg width={props.boxWidth} height={props.boxHeight} viewBox={`0 0 ${props.boxWidth} ${props.boxHeight}`}>
    <Rect
      x="10"
      y="5"
      rx={props.edgeCurve}
      width={props.boxWidth - 10}
      height={props.boxHeight - heightOffset}
      fill={props.bgColor}
      fillOpacity={bgOpacity}
    />
    {
      (props.progressPcnt > 0) ?
        <Rect
          x="10"
          y="5"
          rx={props.edgeCurve}
          width={generateProgressOffset(props.progressPcnt, props.boxWidth)}
          height={props.boxHeight - heightOffset}
          fill={props.progressColor}
        /> : null
    }
    {/* <Rect
            x="1"
            y="1"
            width={props.boxWidth - 2}
            height={props.boxHeight - 2}
            fill="none"
            stroke="red"
            strokeWidth="1"
        /> */}
  </Svg>
);

export default horizontalProgress;