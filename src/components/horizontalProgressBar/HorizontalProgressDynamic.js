import React from 'react';
import Svg, { Path, Rect } from 'react-native-svg';

const heightOffset = 10;
const bgOpacity = "0.6";

function generateProgressOffset(percentage, width) {
  if (percentage > 100) percentage = 100;

  // console.log (`hprogress : ${percentage * width / 100}`);

  return percentage * width / 100;
};

function generatePathForRect(width, height, left = 0, right = 0, ltop = 0, lbottom = 0, rtop = 0, rbottom = 0) {
  if ((width === null) || (height === null)) {
    // dont draw anything
    console.log("Width/Height null. Not drawing anything.");
    return null;
  } else {
    // check for the rest of the parameters
    let path = "";
    // forcing curves not to cross the height limit
    left = (2 * left) > height ? (height / 2) : left;
    right = (2 * right) > height ? (height / 2) : right;
    // move to the starting position to start the path
    path = `M 10 ${5 + left} `;
    // draw the top left curve if it exists
    if (left > 0) path = path + `Q 10 5 ${10 + left} 5 `;
    // draw horizontal line for top edge
    path = path + `H ${width - right} `;
    // draw the top right curve if it exists
    if (right > 0) path = path + `Q ${width} 5 ${width} ${5 + right} `;
    // draw vertical line for right edge 
    path = path + `V ${height - right} `;
    // draw the right bottom curve if it exists
    if (right > 0) path = path + `Q ${width} ${height} ${width - right} ${height} `;
    // draw horizontal line for bottom edge
    path = path + `H ${10 + left} `;
    // draw the left bottom curve if it exists
    if (left > 0) path = path + `Q 10 ${height} 10 ${height - left} `;
    // close the path
    path = path + "Z";

    return path;
  }
}

const horizontalProgressDynamic = (props) => (
  <Svg width={props.boxWidth} height={props.boxHeight} viewBox={`0 0 ${props.boxWidth} ${props.boxHeight}`}>
    <Path
      d={generatePathForRect(props.boxWidth, parseInt(props.boxHeight - (heightOffset / 2)), parseInt(props.leftEdge), parseInt(props.rightEdge))}
      fill={props.bgColor}
      fillOpacity={bgOpacity}
    />
    {
      (props.progressPcnt > 0) ? 
      <Path
        d={generatePathForRect(generateProgressOffset(props.progressPcnt, props.boxWidth), Math.floor(props.boxHeight - (heightOffset / 2)), parseInt(props.leftEdge), parseInt(props.rightEdge))}
        fill={props.progressColor}
      /> : null
    }
  </Svg>
);

export default horizontalProgressDynamic;




{/* <Path 
d={`M 10 10 
    Q 10 5 15 5 
    H ${props.boxWidth - 5} 
    Q ${props.boxWidth} 5 ${props.boxWidth} 10
    V ${props.boxHeight - heightOffset}
    Q ${props.boxWidth} ${props.boxHeight - (heightOffset / 2)} ${props.boxWidth - 5} ${props.boxHeight - (heightOffset / 2)}
    H 15 
    Q 10 ${props.boxHeight - (heightOffset / 2)} 10 ${props.boxHeight - heightOffset}
    Z`}
fill={props.bgColor}
fillOpacity={bgOpacity}
/>
<Rect
x="10"
y="5"
rx={props.edgeCurve}
width={props.boxWidth - 10}
height={props.boxHeight - heightOffset}
fill={props.bgColor}
fillOpacity={bgOpacity}
/>
{
(props.progressPcnt > 0) ?
  <Rect
    x="10"
    y="5"
    rx={props.edgeCurve}
    width={generateProgressOffset(props.progressPcnt, props.boxWidth)}
    height={props.boxHeight - heightOffset}
    fill={props.progressColor}
  /> : null
}
<Rect
      x="1"
      y="1"
      width={props.boxWidth - 2}
      height={props.boxHeight - 2}
      fill="none"
      stroke="red"
      strokeWidth="1"
  /> */}