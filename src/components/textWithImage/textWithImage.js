import React from 'react';
import {StyleSheet, View, Image, Text, Dimensions, PixelRatio} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
const window = Dimensions.get("window");
const screen = Dimensions.get("screen");
const wscale = window.width / 320;


const _normalize = (size) => {
  const newSize = size * wscale 
  return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2
}
const textWithImage = (props) => (
    <View style={styles.headerStyle}>
        <Image source={props.source} style={styles.txtBgImgStyle} />
        <Text allowFontScaling={false} style={styles.textStyle}>
            {props.children}
        </Text>
    </View>
);

const styles = StyleSheet.create({
  headerStyle: {
    // borderColor: "grey",
    // borderWidth: 1,
    // flex: 1,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
  textStyle: {
    fontSize: _normalize(19),
    position: "absolute",
    // top: 0, left: 0, right: 0, bottom: 0,
    color: "white",
    fontStyle: "italic",
    fontWeight: "bold",
  },
  txtBgImgStyle: {
    // borderColor: "red",
    // borderWidth: 1,
    width: wp('70%'),
    resizeMode: 'contain',
    alignItems: "center",
    justifyContent: "center",
  },
});

export default textWithImage;