/////////////////////////////////////////////////////////////////////////
//
// Dates helper

Dates = {
  /////////////////////////////////////////////////////////////////////////
  //
  // get date in ddmmyyyy format
  _ddmmyyyy: (opts) => {
    // get date values
    var date = new Date();
    
    // checking if date is provided
    if (opts != null) {
      if (opts['format'] == 'yyyy-mm-dd') {
        date = new Date (opts['value']);
      }
    }

    // console.log (`current date: ${date.getDate()}:${date.getMonth()}:${date.getFullYear()}`)
    let dd = date.getDate();
    let mm = date.getMonth() + 1;  // getMonth returns values 0-11, so adding 1

    return [(dd > 9 ? '' : '0') + dd, (mm > 9 ? '' : '0') + mm, date.getFullYear()].join('');

  },
  /////////////////////////////////////////////////////////////////////////
  //
  // get day
  _getDay: (opts) => {
    var date = new Date();
    
    // checking if date is provided
    if (opts != null) {
      if (opts['format'] == 'yyyy-mm-dd') {
        date = new Date (opts['value']);
      }
    }

    return '' + date.getDate();
  },
  /////////////////////////////////////////////////////////////////////////
  // get week
  // Returns the week number for this date.  dowOffset is the day of week the week
  // * "starts" on for your locale - it can be from 0 to 6. If dowOffset is 1 (Monday),
  // * the week returned is the ISO 8601 week number.
  _getWeek: (dowOffset = 0, opts) => {

    var date = new Date();
    
    // checking if date is provided
    if (opts != null) {
      if (opts['format'] == 'yyyy-mm-dd') {
        date = new Date (opts['value']);
      }
    }

    // dowOffset = typeof (dayOffset) == 'number' ? dowOffset : 0;  // default dowOffset to 0
    var newYear = new Date (date.getFullYear(), 0, 1);
    var day = newYear.getDay () - dowOffset;
    day = (day >=0 ? day : day + 7);
    var dayNum = Math.floor ((date.getTime() - newYear.getTime() - 
      (date.getTimezoneOffset() - newYear.getTimezoneOffset())*60000)/86400000) + 1;
    
    var weekNum;
    //if the year starts before the middle of a week
    if (day < 4) {
      weekNum = Math.floor ((dayNum + day - 1) / 7) + 1;
      if (weekNum > 52) {
        var nYear = new Date (date.getFullYear() + 1, 0, 1);
        var nday = nYear.getDay () - dowOffset;
        nday = nday >= 0 ? nday : nday + 7;
        /*if the next year starts before the middle of
          the week, it is week #1 of that year*/
        weekNum = nday < 4 ? 1 : 53;
      }
    } else {
      weekNum = Math.floor ((dayNum + day + 1) / 7);
    }

    return '' + weekNum;
  },
  /////////////////////////////////////////////////////////////////////////
  //
  // get month
  _getMonth: (opts) => {
    var date = new Date();
    
    // checking if date is provided
    if (opts != null) {
      if (opts['format'] == 'yyyy-mm-dd') {
        date = new Date (opts['value']);
      }
    }

    return '' + (date.getMonth() + 1);
  },
  /////////////////////////////////////////////////////////////////////////
  //
  // get year
  _getYear: (opts) => {
    var date = new Date();
    
    // checking if date is provided
    if (opts != null) {
      if (opts['format'] == 'yyyy-mm-dd') {
        date = new Date (opts['value']);
      }
    }

    return '' + date.getFullYear();
  },
  /////////////////////////////////////////////////////////////////////////
  //
  // get date
  _getDate: (opts) => {
    var date = new Date();
    
    // checking if date is provided
    if (opts != null) {
      if (opts['format'] == 'yyyy-mm-dd') {
        date = new Date (opts['value']);
      }
    }

    return date.toISOString();
  }
};

export default Dates;