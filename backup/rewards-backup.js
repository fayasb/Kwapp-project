import React, { useEffect, useState } from 'react'
import { BarChart, Grid } from 'react-native-svg-charts'
import { ImageBackground, Image } from 'react-native';
import { StyleSheet, View, Button, SafeAreaView, Text, Alert,UIManager,LayoutAnimation,TouchableOpacity, ScrollView} from 'react-native';
import city from "./cityscape.png";


export default class rewards extends React.PureComponent {
 
    state = {
        brands: [{"brand": "netflix","id":"1"},
                {"brand": "amazon","id":"2"},
                {"brand": "sony playstation","id":"3"},
                {"brand": "netflix","id":"4"},
                {"brand": "amazon","id":"5"},
                {"brand": "sony playstation","id":"6"},
                {"brand": "netflix","id":"7"},
                {"brand": "amazon","id":"8"},
                {"brand": "sony playstation","id":"9"}]
     }

  

    render() {

/***************************************************************************************************************** */
        return (
            <ImageBackground
            source={require('./background.png')}
            style={{width: '100%', height: '100%'}}>
                <ScrollView>
                    {
                this.state.brands.map((item, index) => (
                    <View key = {item.id} style = {styles.item}>
                            <Text>{item.brand}</Text>
                    </View>
                ))
                }
                </ScrollView>

        </ImageBackground>
        )


    }
 
}
const styles = StyleSheet.create ({
    item: {
       flexDirection: 'column',
       width: '90%',
       height: 100,
       backgroundColor: 'powderblue',
       marginLeft: '5%',
       marginRight: '5%',
       marginTop: '5%'
    }
 })


