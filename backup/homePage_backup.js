/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * 
 */

import React, { useState } from 'react';
import { Text, View, StyleSheet, ImageBackground, Dimensions } from 'react-native';

// Import custom components
import CircularProgress from './src/components/circularProgressBar/CircularProgress';
import HorizontalProgress from './src/components/horizontalProgressBar/HorizontalProgress';
import CascadingCircularProgress from './src/components/circularProgressBar/CascadingCircularProgress';
import TextWithImgae from './src/components/textWithImage/textWithImage';

// Gather app window and screen dimensions
const window = Dimensions.get("window");
const screen = Dimensions.get("screen");

// Image handler for the app background
const bgimage = require('./src/backgrounds/app_background.png');
const classTxtBgImage = require("./src/backgrounds/class_background.png");
const mascotImg = require("./src/backgrounds/mascot.png");

export default function App() {
  
  // Refresh dimensions
  const [dimensions, setDimensions] = useState({ window, screen });

  const onChange = ({ window, screen }) => {
    setDimensions({ window, screen });
  }

  // Points and leveling progress
  const profileLevelPcnt = "50"
  const progPcnt = "55";

  // Handler variables
  const offsetby = 96;
  const hsize = dimensions.window.width - offsetby;
  const vsize = (dimensions.window.height / 2) - offsetby;
  const strokeWidth = 4;
  const radius = ((hsize - strokeWidth) / 2) - (offsetby / 2);
  const boxWidth = hsize - 20;
  const boxHeight = 28;
  const progressCx = hsize / 2;
  const progressCy = vsize / 2;

  // Variables for profile pic view
  const profileViewPcnt = 20;
  const profileWidth = profileViewPcnt * hsize / 100;
  const profileCx = profileWidth / 2;
  const profileCy = profileWidth / 2;
  const profileStrokeWidth = 3;
  const profileRadius = (profileWidth - profileStrokeWidth) / 2;

  return (
    <>
      <View style={styles.containerStyle}>
        <ImageBackground source={bgimage} style={styles.imageStyle}>
          <View style={styles.headerContainerStyle}>
            <View style={styles.classContainerStyle}>
              <TextWithImgae source={classTxtBgImage}>
                Master class
              </TextWithImgae>
            </View>
            <View style={styles.profileContainerStyle}>
              <CircularProgress
                boxWidth={profileWidth}
                boxHeight={profileWidth}
                bgColor="white"
                width={profileStrokeWidth}
                r={profileRadius}
                cx={profileCx}
                cy={profileCy}
                progressColor="white"
                dashArray="1, 0"
                progressPcnt={profileLevelPcnt}
              />
            </View>
          </View>
          <View style={styles.statsContainerStyle}>
            <View style={styles.pointProgressContainer}>
              <CascadingCircularProgress
                boxWidth={hsize}
                boxHeight={vsize}
                bgColor="white"
                width={strokeWidth}
                r={radius}
                cx={progressCx}
                cy={progressCy}
                fillColor='transparent'
                progressColor="rgb(255,152,75)"
                dashArray="5, 1"
                progressPcnt={progPcnt}
              />
              <HorizontalProgress
                boxWidth={boxWidth}
                boxHeight={boxHeight}
                edgeCurve="3"
                bgColor="#ffffff"
                progressColor="rgb(255,152,75)"
                progressPcnt={progPcnt}
              />
              <Text style={styles.textStyle}>{progPcnt}/100 points earned today</Text>
            </View>
          </View>
          <View style={styles.footerContainerStyle}>
            <View style={styles.notificationContainerStyle}>
              <Text style={styles.notificationTextStyle}>
                You have met your goal four days in a row! Just one more until the next achievement!
              </Text>
            </View>
            <View style={styles.mascottContainerStyles}>
              <Text style={styles.placeholderStyle}>The Mascott</Text>
            </View>
          </View>
        </ImageBackground>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between",
  },
  imageStyle: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
  },
  textStyle: {
    color: "white",
    fontSize: 15,
    fontWeight: "bold",
  },
  headerTextStyle: {
    color: "white",
    fontSize: 15,
    fontWeight: "bold",
    fontFamily: "sans-serif-light",
  },
  headerContainerStyle: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "grey",
    flexDirection: "row",
    padding: 20,
    width: "100%",
    height: "20%",
    justifyContent: "flex-start",
    alignItems: "center",
  },
  statsContainerStyle: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "grey",
    flexDirection: "column",
    padding: 20,
    width: "100%",
    height: "50%",
    justifyContent: "center",
    alignItems: "center",
  },
  footerContainerStyle: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "grey",
    flexDirection: "row",
    padding: 20,
    width: "100%",
    height: "30%",
    justifyContent: "center",
    alignItems: "center",
  },
  classContainerStyle: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "blue",
    padding: 20,
    width: "70%",
    justifyContent: "center",
    alignItems: "center",
  },
  profileContainerStyle: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "grey",
    // padding: 10,
    width: "30%",
    justifyContent: "center",
    alignItems: "center",
  },
  roundProgressContainer: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "grey",
    height: "70%",
    justifyContent: "center",
    alignItems: "center",
  },
  pointProgressContainer: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "grey",
    height: "30%",
    justifyContent: "center",
    alignItems: "center",
  },
  notificationContainerStyle: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "grey",
    width: "75%",
    justifyContent: "center",
    alignItems: "center",
  },
  notificationTextStyle: {
    color: "white",
    fontSize: 12,
    fontWeight: "bold",
  },
  mascottContainerStyles: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "grey",
    width: "25%",
    justifyContent: "center",
    alignItems: "center",
  },
  placeholderStyle: {
    color: "#282c34",
    fontSize: 12,
    fontWeight: "bold",
  },
});

{/* <Text style={styles.textStyle}>This is the header view</Text> 
<Text style={styles.textStyle}>Profile picture</Text>
<Text style={styles.textStyle}>This is the current stats</Text>


  placeholderStyle: {
    color: "#282c34",
    fontSize: 12,
    fontWeight: "bold",
  },
*/ }