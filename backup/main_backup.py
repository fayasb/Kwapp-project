#!/usr/bin/env python

import pymysql
from app import app
from config import mysql
from flask import jsonify
from flask import flash, request
import json
import ast

		
@app.route('/register', methods=['POST'])

def usrRegister():
	try:
		_json = request.json
		userLevel = _json['userLevel']
		nickname = _json['nickname']
		userStatus = _json['userStatus']
		userUniqueId = _json['userUniqueId']	
		if userLevel and nickname and userStatus and userUniqueId and request.method == 'POST':			
			sqlQuery = "INSERT INTO userregistration(userLevel, nickname, userStatus, userUniqueId) VALUES(%s, %s, %s, %s)"
			bindData = (userLevel, nickname, userStatus, userUniqueId)
			conn = mysql.connect()
			cursor = conn.cursor()
			cursor.execute(sqlQuery, bindData)
			userId = cursor.lastrowid
			conn.commit()
			respone = json.loads('{\"response\": \"Success\", \"userId\": '+str(userId)+'}')
			return respone
		else:
			return not_found()
	except Exception as e:
		respone = jsonify('Failed')
		respone.status_code = 200
		print(e)
	finally:
		cursor.close() 
		conn.close()
###########################################################################################################################
@app.route('/userDetails', methods=['GET'])

def userDetails():
	try:
		conn = mysql.connect()
		cursor = conn.cursor(pymysql.cursors.DictCursor)
		cursor.execute("SELECT userLevel, userUniqueId, nickname, userStatus, gender, birthday, location FROM userregistration")
		userData = cursor.fetchall()
		userData=ast.literal_eval(json.dumps(userData))
		js="{\"userLevel\":"+str(userData[0]['userLevel'])+",\"userUniqueId\":\""+str(userData[0]['userUniqueId'])+"\",\"nickname\":\""+str(userData[0]['nickname'])+"\",\"userStatus\": "+str(userData[0]['userStatus'])+",\"gender\": \""+str(userData[0]['gender'])+"\",\"birthday\": \""+str(userData[0]['birthday'])+"\",\"location\": \""+str(userData[0]['location'])+"\"}"
		response = json.loads(js)
		return response
	except Exception as e:
		print(e)
	finally:
		cursor.close() 
		conn.close()
###########################################################################################################################
@app.route('/statDaily', methods=['GET'])

def statDaily():
	try:
		conn = mysql.connect()
		cursor1 = conn.cursor(pymysql.cursors.DictCursor)
		cursor1.execute("SELECT userId, pointsEarned, day, week, stepsWalked, timeId FROM userstat where userId="+str(request.args['uid'])+" order by userStatId LIMIT 7")
		statData = cursor1.fetchall()
		cursor2 = conn.cursor(pymysql.cursors.DictCursor)
		cursor2.execute("SELECT stepsWalked,pointsEarned FROM currentdaystats")
		dailyData = cursor2.fetchall()
		cursor3 = conn.cursor(pymysql.cursors.DictCursor)
		input = "{"
		for i in range(len(statData)):
			statData[i]=ast.literal_eval(json.dumps(statData[i]))
			cursor3.execute("SELECT day,week, month FROM timedetails where timeid in ("+str(statData[i]['timeId'])+")")
			timeData = cursor3.fetchall()
			timeData[0]=ast.literal_eval(json.dumps(timeData[0]))
			js="\"day"+str(statData[i]['day'])+"\": {\"userId\":"+str(statData[i]['userId'])+",\"pointsEarned\":"+str(statData[i]['pointsEarned'])+",\"week\": "+str(statData[i]['week'])+",\"day\": "+str(timeData[0]['day'])+"."+str(timeData[0]['month'])+",\"stepsWalked\": "+str(statData[i]['stepsWalked'])+"}"
			input=input+js+","
		input=input[:-1]
		input=input+",\"dailyPointsEarned\":"+str(dailyData[0]['pointsEarned'])+",\"dailyStepsWalked\":"+str(dailyData[0]['stepsWalked'])+"}"
		response = json.loads(input)
		return response
	except Exception as e:
		print(e)
	finally:
		cursor1.close() 
		cursor2.close()
		cursor3.close() 
		conn.close()

######################################################################################################################
@app.route('/statMonth', methods=['GET'])

def statMonth():
	try:
		conn = mysql.connect()
		cursor1 = conn.cursor(pymysql.cursors.DictCursor)
		cursor1.execute("SELECT month, stepsWalked, pointsEarned FROM statmonth where userId="+str(request.args['uid']))
		statData = cursor1.fetchall()
		cursor2 = conn.cursor(pymysql.cursors.DictCursor)
		cursor2.execute("SELECT stepsWalked,pointsEarned FROM currentdaystats where userId="+str(request.args['uid']))
		dailyData = cursor2.fetchall()
		input = "{"
		for i in range(len(statData)):
			statData[i]=ast.literal_eval(json.dumps(statData[i]))
			js="\"month"+str(statData[i]['month'])+"\": {\"stepsWalked\":"+str(statData[i]['stepsWalked'])+",\"pointsEarned\":"+str(statData[i]['pointsEarned'])+"}"
			input=input+js+","
		input=input[:-1]
		input=input+",\"dailyPointsEarned\":"+str(dailyData[0]['pointsEarned'])+",\"dailyStepsWalked\":"+str(dailyData[0]['stepsWalked'])+"}"
		response = json.loads(input)
		return response
	except Exception as e:
		print(e)
	finally:
		cursor1.close()
		cursor2.close()
		conn.close()
######################################################################################################################
@app.route('/statWeek', methods=['GET'])

def statWeek():
	try:
		conn = mysql.connect()
		cursor1 = conn.cursor(pymysql.cursors.DictCursor)
		cursor1.execute("SELECT week, stepsWalked, pointsEarned FROM statweek where userId="+str(request.args['uid']))
		statData = cursor1.fetchall()
		cursor2 = conn.cursor(pymysql.cursors.DictCursor)
		cursor2.execute("SELECT stepsWalked,pointsEarned FROM currentdaystats where userId="+str(request.args['uid']))
		dailyData = cursor2.fetchall()
		input = "{"
		for i in range(len(statData)):
			statData[i]=ast.literal_eval(json.dumps(statData[i]))
			js="\"week"+str(statData[i]['week'])+"\": {\"stepsWalked\":"+str(statData[i]['stepsWalked'])+",\"pointsEarned\":"+str(statData[i]['pointsEarned'])+"}"
			input=input+js+","
		input=input[:-1]
		input=input+",\"dailyPointsEarned\":"+str(dailyData[0]['pointsEarned'])+",\"dailyStepsWalked\":"+str(dailyData[0]['stepsWalked'])+"}"
		response = json.loads(input)
		return response
	except Exception as e:
		print(e)
	finally:
		cursor1.close()
		cursor2.close()
		conn.close()

######################################################################################################################
@app.route('/statYear', methods=['GET'])

def statYear():
	try:
		conn = mysql.connect()
		cursor1 = conn.cursor(pymysql.cursors.DictCursor)
		cursor1.execute("SELECT year, stepsWalked, pointsEarned FROM statyear where userId="+str(request.args['uid']))
		statData = cursor1.fetchall()
		cursor2 = conn.cursor(pymysql.cursors.DictCursor)
		cursor2.execute("SELECT stepsWalked,pointsEarned FROM currentdaystats where userId="+str(request.args['uid']))
		dailyData = cursor2.fetchall()
		input = "{"
		for i in range(len(statData)):
			statData[i]=ast.literal_eval(json.dumps(statData[i]))
			js="\"year"+str(statData[i]['year'])+"\": {\"stepsWalked\":"+str(statData[i]['stepsWalked'])+",\"pointsEarned\":"+str(statData[i]['pointsEarned'])+"}"
			input=input+js+","
		input=input[:-1]
		input=input+",\"dailyPointsEarned\":"+str(dailyData[0]['pointsEarned'])+",\"dailyStepsWalked\":"+str(dailyData[0]['stepsWalked'])+"}"
		response = json.loads(input)
		return response
	except Exception as e:
		print(e)
	finally:
		cursor1.close()
		cursor2.close()
		conn.close()




######################################################################################################################


@app.route('/getRewards', methods=['GET'])

def getRewards():
	try:
		conn = mysql.connect()
		cursor = conn.cursor(pymysql.cursors.DictCursor)
		cursor.execute("SELECT id, brand, rate, url FROM rewards")
		rewardData = cursor.fetchall()
		input = "{\"rewards\": ["
		for i in range(len(rewardData)):
			rewardData[i]=ast.literal_eval(json.dumps(rewardData[i]))
			print ("brand: "+str(rewardData[i]['brand']))
			js="{\"id\": "+str(rewardData[i]['id'])+",\"brand\": \""+str(rewardData[i]['brand'])+"\",\"rate\": "+str(rewardData[i]['rate'])+",\"uri\": \""+str(rewardData[i]['url'])+"\"}"
			input=input+js+","
		input=input[:-1]
		input=input+"]}"
		response = json.loads(input)
		return response
	except Exception as e:
		print(e)
	finally:
		cursor.close() 
		conn.close()

######################################################################################################################

	"""			
@app.route('/emp/<int:id>')
def emp(id):
	try:
		conn = mysql.connect()
		cursor = conn.cursor(pymysql.cursors.DictCursor)
		cursor.execute("SELECT id, name, email, phone, address FROM rest_emp WHERE id =%s", id)
		empRow = cursor.fetchone()
		respone = jsonify(empRow)
		respone.status_code = 200
		return respone
	except Exception as e:
		print(e)
	finally:
		cursor.close() 
		conn.close()

@app.route('/update', methods=['PUT'])
def update_emp():
	try:
		_json = request.json
		_id = _json['id']
		_name = _json['name']
		_email = _json['email']
		_phone = _json['phone']
                _address = _json['address']
                if _name and _email and _phone and _address and _id and request.method == 'PUT':			
			sqlQuery = "UPDATE rest_emp SET name=%s, email=%s, phone=%s, address=%s WHERE id=%s"
			bindData = (_name, _email, _phone, _address, _id,)
			conn = mysql.connect()
			cursor = conn.cursor()
			cursor.execute(sqlQuery, bindData)
			conn.commit()
			respone = jsonify('Employee updated successfully!')
			respone.status_code = 200
			return respone
		else:
			return not_found()	
                except Exception as e:
		 print(e)
	        finally:
		 cursor.close() 
		 conn.close()
@app.route('/delete/<int:id>', methods=['DELETE'])
def delete_emp(id):
	try:
		conn = mysql.connect()
		cursor = conn.cursor()
		cursor.execute("DELETE FROM rest_emp WHERE id =%s", (id,))
		conn.commit()
		respone = jsonify('Employee deleted successfully!')
		respone.status_code = 200
		return respone
	except Exception as e:
		print(e)
	finally:
		cursor.close() 
		conn.close()
		
@app.errorhandler(404)
def not_found(error=None):
    message = {
        'status': 404,
        'message': 'Record not found: ' + request.url,
    }
    respone = jsonify(message)
    respone.status_code = 404
    return respone
	"""			
if __name__ == "__main__":
	"""     app.run(host='192.168.0.103', port=5000, debug=True) """
	app.run(host='192.168.0.103', port=5000, debug=True)