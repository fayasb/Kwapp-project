/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * 
 */

import React, { useState, Component } from 'react';
import { Text, View, StyleSheet, ImageBackground, Dimensions } from 'react-native';

// Import custom components
import CircularProgress from './src/components/circularProgressBar/CircularProgress';
import HorizontalProgress from './src/components/horizontalProgressBar/HorizontalProgress';
import CascadingCircularProgress from './src/components/circularProgressBar/CascadingCircularProgress';
import TextWithImgae from './src/components/textWithImage/textWithImage';
import defaultImage from './android/app/src/main/assets/defaultImage.png'

// Gather app window and screen dimensions
const window = Dimensions.get("window");
const screen = Dimensions.get("screen");

// Image handler for the app background
const bgimage = require('./src/backgrounds/app_background.png');
const classTxtBgImage = require("./src/backgrounds/class_background.png");
const mascotImg = require("./src/backgrounds/mascot.png");

export default class App extends Component {
  // Refresh dimensions
  // const [dimensions, setDimensions] = useState({ window, screen });

  // const onChange = ({ window, screen }) => {
  //   setDimensions({ window, screen });
  // }
  initialCall() {
    console.log("initial function calleld")
  fetch(hostip+'homeRequest?uid='+uid)
    .then((response) => response.json())
    .then((json) => {
      console.log("json: ", json)
      this.setState({stepsWalked: json.stepsWalked, pointProg: json.pointProg, stepsGoal: json.stepsGoal, profileLevelPcnto: json.profileLevelPcnto, userClass: json.userClass})
       console.log("points from fetch ", json)
      db.transaction(tx => {
        tx.executeSql('update userstat set d_pointsearned=?, day=? where statid=7 ', [json.pointProg, json.stepsWalked], (tx, results) => {
          console.log("home afterfetch1 resultsaffected: ", results.rowsAffected)
          console.log("updated points: ", json.pointProg)
        });
    });
  })
    .catch(error => {
      console.log('There has been a problem with your fetch operation: ' + error.message);
      
      }) 
console.log("steps walked: ", this.state.stepsWalked)
  }
componentDidMount() {
    console.log("componentdidmount callled------------------------------------------------------------------------------")
fetch(hostip+'homeRequest?uid='+uid)
.then((response) => response.json())
.then((json) => {
  console.log("json: ", json)
  this.setState({stepsWalked: json.stepsWalked, pointProg: json.pointProg, stepsGoal: json.stepsGoal, profileLevelPcnt: json.profileLevelPcnt, userClass: json.userClass})
   console.log("points from fetch ", json)
  db.transaction(tx => {
    tx.executeSql('update userstat set d_pointsearned=?, day=? where statid=7 ', [json.pointProg, json.stepsWalked], (tx, results) => {
      console.log("home afterfetch1 resultsaffected: ", results.rowsAffected)
      console.log("updated points: ", json.pointProg)
    });
});
})
.catch(error => {
  console.log('There has been a problem with your fetch operation: ' + error.message);
  
  }) 
console.log("steps walked: ", this.state.stepsWalked)
this.forceUpdate()
}
  constructor (props) {
    super(props);

    this.dimensions = {window, screen};

    // State
    this.state = {

      userClass: "Beginner",
      profileLevelPcnt: "99",
      stepsWalked: "7500",
      stepsGoal: "10000",
      pointProg: "75",
      avatarSource: defaultImage

    }

    // Points and leveling progress
    // this.userClass = "Beginner";
    // this.profileLevelPcnt = "99";
    // this.stepsWalked = "3750";
    // this.stepsGoal = "10000";
    // this.pointProg = "75";
  
    // Handler variables
    this.offsetby = 96;
    this.hsize = this.dimensions.window.width - this.offsetby;
    this.vsize = (this.dimensions.window.height / 2) - this.offsetby;
    this.strokeWidth = 4;
    this.radius = ((this.hsize - this.strokeWidth) / 2) - (this.offsetby / 2);
    this.boxWidth = this.hsize - 20;
    this.boxHeight = 28;
    this.progressCx = this.hsize / 2;
    this.progressCy = this.vsize / 2;
  
    // Variables for profile pic view
    this.profileViewPcnt = 20;
    this.profileWidth = this.profileViewPcnt * this.hsize / 100;
    this.profileCx = this.profileWidth / 2;
    this.profileCy = this.profileWidth / 2;
    this.profileStrokeWidth = 3;
    this.profileRadius = (this.profileWidth - this.profileStrokeWidth) / 2;
    db.transaction(tx => {
      tx.executeSql('select day, d_pointsearned from userstat where statid = 7', [], (tx, results) => {
        console.log("homepage steps and points: ", results.rows.item(0))
        this.setState({stepsWalked: results.rows.item(0)['day'], pointProg: results.rows.item(0)['d_pointsearned']})
      });
    });
    db.transaction(tx => {
      tx.executeSql('select userlevel, profileimage from userdetails', [], (tx, results) => {
        console.log("userlevel from localfb: ", results.rows.item(0)['userlevel'])
        if (results.rows.item(0)['userlevel'] == 0) {
            console.log("inside if 1")
          this.setState({userClass: 'Beginner'});
        } else if (results.rows.item(0)['userlevel'] == 1) {
          console.log("inside if 2")
          this.setState({userClass: 'Medium'});
        }
        else {
          console.log("inside if 3")
          this.setState({userClass: 'Master'});
        }
      });
    });
  }

  render () {
    return (
      <>
        <View style={styles.containerStyle}>
          <ImageBackground source={bgimage} style={styles.imageStyle}>
            <View style={styles.headerContainerStyle}>
              <View style={styles.classContainerStyle}>
                <TextWithImgae source={classTxtBgImage}>
                  {this.state.userClass} challenger
                </TextWithImgae>
              </View>
              <View style={styles.profileContainerStyle}>
                <CircularProgress
                  boxWidth={this.profileWidth}
                  boxHeight={this.profileWidth}
                  bgColor="white"
                  width={this.profileStrokeWidth}
                  r={this.profileRadius}
                  cx={this.profileCx}
                  cy={this.profileCy}
                  progressColor="white"
                  dashArray="1, 0"
                  progressPcnt={this.state.profileLevelPcnt}
                />
              </View>
            </View>
            <View style={styles.statsContainerStyle}>
              <View style={styles.pointProgressContainer}>
                <CascadingCircularProgress
                  boxWidth={this.hsize}
                  boxHeight={this.vsize}
                  bgColor="white"
                  width={this.strokeWidth}
                  r={this.radius}
                  cx={this.progressCx}
                  cy={this.progressCy}
                  progressColor="rgb(255,152,75)"
                  dashArray="5, 1"
                  steps={this.state.stepsWalked}
                  stepsGoal={this.state.stepsGoal}
                />
                <HorizontalProgress
                  boxWidth={this.boxWidth}
                  boxHeight={this.boxHeight}
                  edgeCurve="3"
                  bgColor="#ffffff"
                  progressColor="rgb(255,152,75)"
                  progressPcnt={this.state.pointProg}
                />
                <Text style={styles.textStyle}>{this.state.pointProg}/100 points earned today</Text>
              </View>
            </View>
            <View style={styles.footerContainerStyle}>
              <View style={styles.notificationContainerStyle}>
                <Text style={styles.notificationTextStyle}>
                  You have met your goal four days in a row! Just one more until the next achievement!
                </Text>
              </View>
              <View style={styles.mascottContainerStyles}>
                <Text style={styles.placeholderStyle}>The Mascott</Text>
              </View>
            </View>
          </ImageBackground>
        </View>
      </>
    );
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between",
  },
  imageStyle: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
  },
  textStyle: {
    color: "white",
    fontSize: 15,
    fontWeight: "bold",
  },
  headerTextStyle: {
    color: "white",
    fontSize: 15,
    fontWeight: "bold",
    fontFamily: "sans-serif-light",
  },
  headerContainerStyle: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "blue",
    flexDirection: "row",
    paddingLeft: 15,
    width: "100%",
    height: "20%",
    justifyContent: "space-around",
    alignItems: "center",
  },
  statsContainerStyle: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "grey",
    flexDirection: "column",
    padding: 20,
    width: "100%",
    height: "50%",
    justifyContent: "center",
    alignItems: "center",
  },
  footerContainerStyle: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "grey",
    flexDirection: "row",
    padding: 20,
    width: "100%",
    height: "30%",
    justifyContent: "center",
    alignItems: "center",
  },
  classContainerStyle: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "red",
    padding: 20,
    width: "70%",
    justifyContent: "center",
    alignItems: "center",
  },
  profileContainerStyle: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "red",
    // padding: 10,
    width: "30%",
    justifyContent: "center",
    alignItems: "center",
  },
  roundProgressContainer: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "grey",
    height: "70%",
    justifyContent: "center",
    alignItems: "center",
  },
  pointProgressContainer: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "grey",
    height: "30%",
    justifyContent: "center",
    alignItems: "center",
  },
  notificationContainerStyle: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "grey",
    width: "75%",
    justifyContent: "center",
    alignItems: "center",
  },
  notificationTextStyle: {
    color: "white",
    fontSize: 12,
    fontWeight: "bold",
  },
  mascottContainerStyles: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "grey",
    width: "25%",
    justifyContent: "center",
    alignItems: "center",
  },
  placeholderStyle: {
    color: "#282c34",
    fontSize: 12,
    fontWeight: "bold",
  },
});

{/* <Text style={styles.textStyle}>This is the header view</Text> 
<Text style={styles.textStyle}>Profile picture</Text>
<Text style={styles.textStyle}>This is the current stats</Text>


  placeholderStyle: {
    color: "#282c34",
    fontSize: 12,
    fontWeight: "bold",
  },
*/ }