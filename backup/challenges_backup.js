import React from 'react'

import { ImageBackground, Image, Button, TouchableOpacity } from 'react-native';
import { StyleSheet, View, Text, TextInput, Dimensions} from 'react-native';
import profile from './images/profile.png'
import as_icon from './images/as_icon.png'
import back_image from './src/backgrounds/app_background.png'
import { ScrollView } from 'react-native-gesture-handler';
import HorizontalProgress from './src/components/horizontalProgressBar/HorizontalProgress';
import challenge_icon from './src/icons/challenge_complete.png'
import consecutive_challenge from './src/icons/consecutive_challenge.png'
import point_challenge from './src/icons/point_challenge.png'
import step_challenge from './src/icons/step_challenge.png'
import challenge_completed from './src/icons/challenge_complete.png'
import dot from './src/icons/dot.png'
const window = Dimensions.get("window");
const screen = Dimensions.get("screen");


export default class challenges extends React.PureComponent {

 
componentDidMount(){
    db.transaction(tx => {
        tx.executeSql('select nickname, userid, useruniqueid from userdetails', [], (tx, results) => {
        console.log("initial resultsaffected: ", results.rowsAffected)
        this.setState({userName: results.rows.item(0)['nickname']})
        });

    });
}
_challenges=() => {
    this.setState({menuView: true})
}

_events=() => {
    this.setState({menuView: false})
}
constructor(props) {
    super(props);
    this.state = {
        userName: '',
        menuView: true,
        challenge1: 1,
        challenge2: 1000,
        challenge3: 9000000,
        challenge4: 1,
        challenges: [{"name": "Complete the 3 daily challenges", "desc": "7 days in a row", "target": 7, "uri":"asset:/consecutive_challenge.png"},
                    {"name": "Earn 20,000 pts", "desc": '', "target": 20000, "uri": "asset:/point_challenge.png"},
                    {"name": "Walk 200,000", "desc": '', "target": 200000, "uri": "asset:/step_challenge.png"}]
    }
    this.dimensions = {window, screen};
    this.offsetby = 300;
    this.hsize = this.dimensions.window.width - this.offsetby;
    this.boxWidth = this.hsize - 20;
    this.boxHeight = 28;
};
    render() {



/***************************************************************************************************************** */
        return (
            <ImageBackground
            source={back_image}
            style={{width: '100%', height: '100%'}}>
                <ScrollView>
                    <View style = {{flexDirection: 'column', alignItems: 'center'}}>
                        <View style={{alignItems: 'center', marginTop:'10%'}}>
                            <Image source={profile} style = {{width:100,height:100, borderRadius: 100/2, borderWidth: 5, borderColor: 'white'}}/>
                            <Text style={{alignItems: 'center', fontSize: 15, paddingTop: '2%', color: 'white'}}>{this.state.userName}</Text>
                        </View>
                        <View style={{alignItems: 'center', flexDirection: 'column', paddingTop: '5%'}}>
                            <Text style={{color: 'white', fontSize: 15}}>Daily Challenge Progress</Text>
                        </View>
                        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                        <HorizontalProgress
                            boxWidth={70}
                            boxHeight={25}
                            edgeCurve="10"
                            bgColor="white"
                            progressColor='rgb(254, 170, 58)'
                            progressPcnt={100}
                            />
                            <HorizontalProgress
                            boxWidth={70}
                            boxHeight={25}
                            edgeCurve="3"
                            bgColor="white"
                            progressColor='rgb(254, 170, 58)'
                            progressPcnt={100}
                            />
                            <HorizontalProgress
                            boxWidth={70}
                            boxHeight={25}
                            bgColor="white"
                            progressColor='rgb(254, 170, 58)'
                            progressPcnt={20}
                            edgeCurve="3"
                            />
                        </View>
                        <View style={{flexDirection: 'row', paddingLeft: '0%', paddingTop: '5%'}}>
                            <TouchableOpacity onPress={() => {this._challenges()}}>
                                <Text style={{color: 'white', fontWeight: 'bold', fontSize: 18}}>Challenges</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{paddingLeft: '20%'}} onPress={() => {this._events()}}>
                                <Text style={{color: 'white', fontWeight: 'bold', fontSize: 18}}>Events</Text>
                            </TouchableOpacity>
                        </View>
                        {this.state.menuView ? (
                        <View style={{width: '100%', alignItems: 'center'}}>
                        <View style={{width: '90%', height: 90, backgroundColor: 'rgb(254, 170, 58)', marginTop: '2%', borderRadius: 10}}>
                            <Text style={{color: 'white', marginLeft: '5%'}}>Complete the 3 daily challenges</Text>
                            
                            <Text style={{color: 'white', marginLeft: '5%'}}>7 days in a row</Text>

                            <View style={{width: 50, marginLeft: '52%'}}>
                                <Text style={{color: 'white', textAlign: 'right', }}>{this.state.challenge1}/7</Text>
                            </View>
                            <View style={{flexDirection: 'row'}}>
                                <HorizontalProgress
                                boxWidth={250}
                                boxHeight={25}
                                bgColor="white"
                                progressColor='rgb(65, 84, 105)'
                                progressPcnt={20}
                                edgeCurve={10}
                                />
                                <Image source={consecutive_challenge} style = {{justifyContent: 'center', alignItems: 'center', top: -55, left: 35}}/>
                            </View>
                        </View>
                        <View style={{width: '90%', height: 90, backgroundColor: 'rgb(254, 170, 58)', marginTop: '2%', borderRadius: 10}}>
                            <Text style={{color: 'white', marginLeft: '5%'}}>Earn 20,000 pts</Text>
                            <Text style={{color: 'white', marginLeft: '5%'}}></Text>
                            <View style={{width: 120, marginLeft: '32%'}}>
                                <Text style={{color: 'white', textAlign: 'right', }}>{this.state.challenge2}/20000</Text>
                            </View>
                            <View style={{flexDirection: 'row'}}>
                                <HorizontalProgress
                                boxWidth={250}
                                boxHeight={25}
                                bgColor="white"
                                progressColor='rgb(65, 84, 105)'
                                progressPcnt={20}
                                edgeCurve={10}
                                />
                                <Image source={point_challenge} style = {{justifyContent: 'center', alignItems: 'center', top: -45, left: 35}}/>
                            </View>
                        </View>
                        <View style={{width: '90%', height: 90, backgroundColor: 'rgb(254, 170, 58)', marginTop: '2%', borderRadius: 10}}>
                            <Text style={{color: 'white', marginLeft: '5%'}}>Walk 200,000 steps</Text>
                            <Text style={{color: 'white', marginLeft: '5%'}}></Text>
                            <View style={{width: 150, marginLeft: '24%'}}>
                                <Text style={{color: 'white', textAlign: 'right', }}>{this.state.challenge3}/200,000</Text>
                            </View>
                            <View style={{flexDirection: 'row'}}>
                                <HorizontalProgress
                                boxWidth={250}
                                boxHeight={25}
                                bgColor="white"
                                progressColor='rgb(65, 84, 105)'
                                progressPcnt={20}
                                edgeCurve={10}
                                />
                                <Image source={step_challenge} style = {{justifyContent: 'center', alignItems: 'center', top: -55, left: 45}}/>
                            </View>
                        </View>
                        <View style={{width: '90%', height: 90, backgroundColor: 'rgb(254, 170, 58)', marginTop: '2%', borderRadius: 10}}>
                            <Text style={{color: 'white', marginLeft: '5%'}}>Complete 5 non-daily challenges</Text>
                            <Text style={{color: 'white', marginLeft: '5%'}}></Text>
                            <View style={{width: 50, marginLeft: '52%'}}>
                                <Text style={{color: 'white', textAlign: 'right', }}>{this.state.challenge4}/5</Text>
                            </View>
                            <View style={{flexDirection: 'row'}}>
                                <HorizontalProgress
                                boxWidth={250}
                                boxHeight={25}
                                bgColor="white"
                                progressColor='rgb(65, 84, 105)'
                                progressPcnt={20}
                                edgeCurve={10}
                                />
                                <Image source={challenge_completed} style = {{justifyContent: 'center', alignItems: 'center', top: -55, left: 45}}/>
                            </View>
                        </View>
                        </View>)
                  :
                  <View>
                    <Text style={{color: 'rgb(254, 170, 58)', fontSize: 20, paddingTop: '30%', fontWeight: 'bold'}}>Fitness events coming soon !</Text>
                  </View>
              }                 
                    </View>
                </ScrollView>
        </ImageBackground>
        )


    }
 
}
const styles = StyleSheet.create ({
    item: {
       flexDirection: 'column',
       width: '100%',
       height: 200,
       marginLeft: '5%',
       marginRight: '10%',
       marginTop: '10%',
       marginBottom: '3%',
       alignItems: 'center',
       position: 'relative'
       
       
    }
 })


