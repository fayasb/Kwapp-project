import React, { useEffect, useState } from 'react'
import { BarChart, Grid, XAxis, YAxis  } from 'react-native-svg-charts'
import { ImageBackground, Image, Dimensions  } from 'react-native';
import { StyleSheet, View, Button, SafeAreaView, Text, TextInput, Alert,UIManager,LayoutAnimation,TouchableOpacity} from 'react-native';
import city from "./cityscape.png";
import * as scale from 'd3-scale';
import para from './para_snipped.png'
import back_image from './src/backgrounds/app_background.png'
UIManager.setLayoutAnimationEnabledExperimental &&
  UIManager.setLayoutAnimationEnabledExperimental(true);
  const screenWidth = Math.round(Dimensions.get('window').width);
  const contentInset = {top: 10}
 
export default class week extends React.PureComponent {
  errorCB(err) {
    console.log("SQL Error: " + err);
  }
   
  successCB() {
    console.log("SQL executed fine");
  }
   
  openCB() {
    console.log("Database OPENED");
  }
  



      componentDidMount() {
        console.log("inside componentdidmount")
        fetch(hostip+'statDaily?uid='+uid,{
          method: 'GET'
        })
        .then(res => res.json())
        .then((data) => {
          if (this.state.statValue != 1){
            console.log("when button for points is selected")
          this.setState({ data1: data.day1.pointsEarned, data2: data.day2.pointsEarned,
            data3: data.day3.pointsEarned, data4: data.day4.pointsEarned, data5: data.day5.pointsEarned, 
            data6: data.day6.pointsEarned, data7: data.day7.pointsEarned,stepsWalked: data.day7.stepsWalked,
             pointsEarned: data.day7.pointsEarned, sdata: [data.day1.pointsEarned, data.day2.pointsEarned, data.day3.pointsEarned, data.day4.pointsEarned, data.day5.pointsEarned, data.day6.pointsEarned, data.day7.pointsEarned],
            pdata: [data.day1.pointsEarned, data.day2.pointsEarned, data.day3.pointsEarned, data.day4.pointsEarned, data.day5.pointsEarned, data.day6.pointsEarned, data.day7.pointsEarned],
            sdata: [data.day1.stepsWalked, data.day2.stepsWalked, data.day3.stepsWalked, data.day4.stepsWalked, data.day5.stepsWalked, data.day6.stepsWalked, data.day7.stepsWalked],
          xdata: [data.day1.day, data.day2.day, data.day3.day, data.day4.day, data.day5.day, data.day6.day, data.day7.day]})
        }
        else {
          console.log("when button for steps is selected")
          this.setState({ data1: data.day1.stepsWalked, data2: data.day2.stepsWalked,
            data3: data.day3.stepsWalked, data4: data.day4.stepsWalked, data5: data.day5.stepsWalked, 
            data6: data.day6.stepsWalked, data7: data.day7.stepsWalked,stepsWalked: data.day7.stepsWalked,
             pointsEarned: data.day7.pointsEarned, sdata: [data.day1.pointsEarned, data.day2.pointsEarned, data.day3.pointsEarned, data.day4.pointsEarned, data.day5.pointsEarned, data.day6.pointsEarned, data.day7.pointsEarned],
            pdata: [data.day1.pointsEarned, data.day2.pointsEarned, data.day3.pointsEarned, data.day4.pointsEarned, data.day5.pointsEarned, data.day6.pointsEarned, data.day7.pointsEarned],
            sdata: [data.day1.stepsWalked, data.day2.stepsWalked, data.day3.stepsWalked, data.day4.stepsWalked, data.day5.stepsWalked, data.day6.stepsWalked, data.day7.stepsWalked],
          xdata: [data.day1.day, data.day2.day, data.day3.day, data.day4.day, data.day5.day, data.day6.day, data.day7.day]})
        }
         console.log("daily api called")
  
          console.log("sdata: ", this.state.sdata)
          console.log("sid: ", this.state.sid[2])
  
        console.log("Starting db execution")
        /* let placeholders = this.state.sid.map((sid, sdata) => '(?,?)').join(','); */
        let placeholders = this.state.sid.map((item, i) => '('+ this.state.sid[i]+','+ this.state.sdata[i]+')').join(',');
        /*========================================================================================== */
        db.transaction(tx => {
          for (let i = 0; i < this.state.sdata.length; ++i) {
            tx.executeSql('update userstat set d_pointsearned=?, day=?, xdatad=? where statid=? ', [this.state.pdata[i], this.state.sdata[i], this.state.xdata[i], i+1], (tx, results) => {
              console.log("resultsaffected: ", results.rowsAffected)
            });
          }
          
        });
        /*=============================================================================================== */
         /* db.transaction(function(tx) {
          tx.executeSql(
            "insert into userstat (statid, day) VALUES "+ placeholders,
             "update userstat set statid=?, day=? "+ placeholders, 
            [],
            (tx, results) => {
              if (results.rowsAffected > 0) {
                console.log("results.rowsAffected: ", results.rowsAffected)
  
                .catch(function(error) {
                  if (error){
                    console.log("Error : ", error)
                }
                });
              
              } 
            }
          );
        });  */
        
        })
        .catch(error => {
          console.log('There has been a problem with your fetch operation: ' + error.message);
          })    
        
      }


      cityLocation() {
          console.log("city function called")
        if (this.state.stepsWalked > 5000 && this.state.stepsWalked <= 10000) {
            this.state.position = 'center'
        } else if (this.state.stepsWalked > 10000)
        {
            this.state.position = 'flex-start'
        }
        else {
            this.state.position = 'flex-end'
        }
    }      

      _daily() {
        var p_total = 0;
        var s_total = 0;
        db.transaction(tx => {
          tx.executeSql('select d_pointsearned, day, xdatad from userstat where statid in (1,2,3,4,5,6,7)', [], (tx, results) => {
            console.log("results: ", results.rows.item(0))
            p_total = results.rows.item(0)['d_pointsearned'] + results.rows.item(1)['d_pointsearned'] + results.rows.item(2)['d_pointsearned'] + results.rows.item(3)['d_pointsearned'] + results.rows.item(4)['d_pointsearned'] + results.rows.item(5)['d_pointsearned'] + results.rows.item(6)['d_pointsearned']
            if (this.state.statValue != 1){
            this.setState({
              data1: results.rows.item(0)['d_pointsearned'], data2: results.rows.item(1)['d_pointsearned'],
          data3: results.rows.item(2)['d_pointsearned'], data4: results.rows.item(3)['d_pointsearned'], data5: results.rows.item(4)['d_pointsearned'], 
          data6: results.rows.item(5)['d_pointsearned'], data7: results.rows.item(6)['d_pointsearned'],stepsWalked: results.rows.item(0)['day'],
           pointsEarned: results.rows.item(0)['d_pointsearned'],xdata: [results.rows.item(0)['xdatad'], results.rows.item(1)['xdatad'], results.rows.item(2)['xdatad'], results.rows.item(3)['xdatad'], results.rows.item(4)['xdatad'], results.rows.item(5)['xdatad'], results.rows.item(6)['xdatad']],
           sdata: [results.rows.item(0)['day'],results.rows.item(1)['day'],
           results.rows.item(2)['day'], results.rows.item(3)['day'], results.rows.item(4)['day'], 
           results.rows.item(5)['day'], results.rows.item(6)['day']],
           pdate: [results.rows.item(0)['d_pointsearned'],results.rows.item(1)['d_pointsearned'],
           results.rows.item(2)['d_pointsearned'], results.rows.item(3)['d_pointsearned'], results.rows.item(4)['d_pointsearned'], 
           results.rows.item(5)['d_pointsearned'], results.rows.item(6)['d_pointsearned']]
            });
            }
            else {
              this.setState({
                data1: results.rows.item(0)['day'], data2: results.rows.item(1)['day'],
            data3: results.rows.item(2)['day'], data4: results.rows.item(3)['day'], data5: results.rows.item(4)['day'], 
            data6: results.rows.item(5)['day'], data7: results.rows.item(6)['day'],stepsWalked: results.rows.item(0)['day'],
             pointsEarned: results.rows.item(0)['d_pointsearned'],xdata: [results.rows.item(0)['xdatad'], results.rows.item(1)['xdatad'], results.rows.item(2)['xdatad'], results.rows.item(3)['xdatad'], results.rows.item(4)['xdatad'], results.rows.item(5)['xdatad'], results.rows.item(6)['xdatad']],
             sdata: [results.rows.item(0)['day'],results.rows.item(1)['day'],
           results.rows.item(2)['day'], results.rows.item(3)['day'], results.rows.item(4)['day'], 
           results.rows.item(5)['day'], results.rows.item(6)['day']],
           pdate: [results.rows.item(0)['d_pointsearned'],results.rows.item(1)['d_pointsearned'],
           results.rows.item(2)['d_pointsearned'], results.rows.item(3)['d_pointsearned'], results.rows.item(4)['d_pointsearned'], 
           results.rows.item(5)['d_pointsearned'], results.rows.item(6)['d_pointsearned']]
              });  
            }
          });
        });
        fetch(hostip+'statDaily?uid='+uid,{
          method: 'GET'
        })
        .then(res => res.json())
        .then((data) => {
            p_total = data.day1.pointsEarned + data.day2.pointsEarned + data.day3.pointsEarned + data.day4.pointsEarned + 
            data.day5.pointsEarned + data.day6.pointsEarned + data.day7.pointsEarned
            s_total = data.day1.stepsWalked + data.day2.stepsWalked + data.day3.stepsWalked + data.day4.stepsWalked + 
            data.day5.stepsWalked + data.day6.stepsWalked + data.day7.stepsWalked
            if (this.state.statValue != 1){
            this.setState({ data1: data.day1.pointsEarned, data2: data.day2.pointsEarned,
            data3: data.day3.pointsEarned, data4: data.day4.pointsEarned, data5: data.day5.pointsEarned, 
            data6: data.day6.pointsEarned, data7: data.day7.pointsEarned,stepsWalked: data.day7.stepsWalked,
             pointsEarned: data.day7.pointsEarned, pdata: [data.day1.pointsEarned, data.day2.pointsEarned, data.day3.pointsEarned, data.day4.pointsEarned, data.day5.pointsEarned, data.day6.pointsEarned, data.day7.pointsEarned],
             sdata: [data.day1.stepsWalked, data.day2.stepsWalked, data.day3.stepsWalked, data.day4.stepsWalked, data.day5.stepsWalked, data.day6.stepsWalked, data.day7.stepsWalked],
             xdata: [data.day1.day, data.day2.day, data.day3.day, data.day4.day, data.day5.day, data.day6.day, data.day7.day]})
            }
            else {
              this.setState({ data1: data.day1.stepsWalked, data2: data.day2.stepsWalked,
                data3: data.day3.stepsWalked, data4: data.day4.stepsWalked, data5: data.day5.stepsWalked, 
                data6: data.day6.stepsWalked, data7: data.day7.stepsWalked,stepsWalked: data.day7.stepsWalked,
                 pointsEarned: data.day7.pointsEarned, pdata: [data.day1.pointsEarned, data.day2.pointsEarned, data.day3.pointsEarned, data.day4.pointsEarned, data.day5.pointsEarned, data.day6.pointsEarned, data.day7.pointsEarned],
                 sdata: [data.day1.stepsWalked, data.day2.stepsWalked, data.day3.stepsWalked, data.day4.stepsWalked, data.day5.stepsWalked, data.day6.stepsWalked, data.day7.stepsWalked],
                 xdata: [data.day1.day, data.day2.day, data.day3.day, data.day4.day, data.day5.day, data.day6.day, data.day7.day]})              
            }
         console.log("daily function called")
         let placeholders = this.state.sid.map((item, i) => '('+ this.state.sdata[i]+','+ this.state.sid[i]+')').join(',');
         console.log("placeholder: ", this.state.sdata.length)
         /*============================================================================= */
         db.transaction(tx => {
          for (let i = 0; i < this.state.sdata.length; ++i) {
            tx.executeSql('update userstat set d_pointsearned=?, day=?, xdatad=? where statid=? ', [this.state.pdata[i], this.state.sdata[i], this.state.xdata[i], i+1], (tx, results) => {
              console.log("day resultsaffected: ", results.rowsAffected)
            });
          }
          
        });
        /*======================================================================================*/
  /*        db.transaction(function(tx) {
          tx.executeSql(
            "update userstat set day=? where statid in (1,2,3,4,5,6,7) ", this.state.sdata,
            (tx, results) => {
              if (results.rowsAffected > 0) {
                console.log("results.rowsAffected: ", results.rowsAffected)
  
                .catch(function(error) {
                  if (error){
                    console.log("Error : ", error)
                }
                });
              
              } 
            }
          );
        }); */
        })
        .catch(error => {
          console.log('There has been a problem with your fetch operation: ' + error.message);
          })  
      }

      _1month()
      {
        var p_total = 0;
        var s_total = 0;
        db.transaction(tx => {
          tx.executeSql('select m_pointsearned, month, xdatam from userstat where statid in (1,2,3,4,5,6,7)', [], (tx, results) => {
            console.log("results: ", results.rows.item(0))
            p_total = results.rows.item(0)['m_pointsearned'] + results.rows.item(1)['m_pointsearned'] + results.rows.item(2)['m_pointsearned'] + results.rows.item(3)['m_pointsearned'] + results.rows.item(4)['m_pointsearned'] + results.rows.item(5)['m_pointsearned'] + results.rows.item(6)['m_pointsearned']
            s_total = results.rows.item(0)['month'] + results.rows.item(1)['month'] + results.rows.item(2)['month'] + results.rows.item(3)['month'] + results.rows.item(4)['month'] + results.rows.item(5)['month'] + results.rows.item(6)['month']
            if (this.state.statValue != 1){
            this.setState({
              data1: results.rows.item(0)['m_pointsearned'], data2: results.rows.item(1)['m_pointsearned'],
          data3: results.rows.item(2)['m_pointsearned'], data4: results.rows.item(3)['m_pointsearned'], data5: results.rows.item(4)['m_pointsearned'], 
          data6: results.rows.item(5)['m_pointsearned'], data7: results.rows.item(6)['m_pointsearned'],stepsWalked: s_total,
           pointsEarned:p_total, xdata: [results.rows.item(0)['xdatam'], results.rows.item(1)['xdatam'], results.rows.item(2)['xdatam'], results.rows.item(3)['xdatam'], results.rows.item(4)['xdatam'], results.rows.item(5)['xdatam'], results.rows.item(6)['xdatam']],
           sdata: [results.rows.item(0)['month'],results.rows.item(1)['month'],
           results.rows.item(2)['month'], results.rows.item(3)['month'], results.rows.item(4)['month'], 
           results.rows.item(5)['month'], results.rows.item(6)['month']],
           pdate: [results.rows.item(0)['m_pointsearned'],results.rows.item(1)['m_pointsearned'],
           results.rows.item(2)['m_pointsearned'], results.rows.item(3)['m_pointsearned'], results.rows.item(4)['m_pointsearned'], 
           results.rows.item(5)['m_pointsearned'], results.rows.item(6)['m_pointsearned']]
            });
              }
              else {
                this.setState({
                  data1: results.rows.item(0)['month'], data2: results.rows.item(1)['month'],
              data3: results.rows.item(2)['month'], data4: results.rows.item(3)['month'], data5: results.rows.item(4)['month'], 
              data6: results.rows.item(5)['month'], data7: results.rows.item(6)['month'],stepsWalked: s_total,
               pointsEarned:p_total, xdata: [results.rows.item(0)['xdatam'], results.rows.item(1)['xdatam'], results.rows.item(2)['xdatam'], results.rows.item(3)['xdatam'], results.rows.item(4)['xdatam'], results.rows.item(5)['xdatam'], results.rows.item(6)['xdatam']],
               sdata: [results.rows.item(0)['month'],results.rows.item(1)['month'],
           results.rows.item(2)['month'], results.rows.item(3)['month'], results.rows.item(4)['month'], 
           results.rows.item(5)['month'], results.rows.item(6)['month']],
           pdate: [results.rows.item(0)['m_pointsearned'],results.rows.item(1)['m_pointsearned'],
           results.rows.item(2)['m_pointsearned'], results.rows.item(3)['m_pointsearned'], results.rows.item(4)['m_pointsearned'], 
           results.rows.item(5)['m_pointsearned'], results.rows.item(6)['m_pointsearned']]
                }); 
              }
          });
        });

        fetch(hostip+'statMonth?uid='+uid,{
            method: 'GET'
          })
          .then(res => res.json())
          .then((data) => {
            p_total = data.month1.pointsEarned + data.month2.pointsEarned + data.month3.pointsEarned + data.month4.pointsEarned + 
            data.month5.pointsEarned + data.month6.pointsEarned + data.month7.pointsEarned
            s_total = data.month1.stepsWalked + data.month2.stepsWalked + data.month3.stepsWalked + data.month4.stepsWalked + 
            data.month5.stepsWalked + data.month6.stepsWalked + data.month7.stepsWalked
            if (this.state.statValue != 1){
            this.setState({ data1: data.month1.pointsEarned, data2: data.month2.pointsEarned,
              data3: data.month3.pointsEarned, data4: data.month4.pointsEarned, data5: data.month5.pointsEarned, 
              data6: data.month6.pointsEarned, data7: data.month7.pointsEarned,stepsWalked: s_total,
               pointsEarned: p_total, pdata: [data.month1.pointsEarned, data.month2.pointsEarned, data.month3.pointsEarned, data.month4.pointsEarned, data.month5.pointsEarned, data.month6.pointsEarned, data.month7.pointsEarned],
               sdata: [data.month1.stepsWalked, data.month2.stepsWalked, data.month3.stepsWalked, data.month4.stepsWalked, data.month5.stepsWalked, data.month6.stepsWalked, data.month7.stepsWalked],
               xdata: [data.month1.month, data.month2.month, data.month3.month, data.month4.month, data.month5.month, data.month6.month, data.month7.month]})
              }
              else {
                this.setState({ data1: data.month1.stepsWalked, data2: data.month2.stepsWalked,
                  data3: data.month3.stepsWalked, data4: data.month4.stepsWalked, data5: data.month5.stepsWalked, 
                  data6: data.month6.stepsWalked, data7: data.month7.stepsWalked,stepsWalked: s_total,
                   pointsEarned: p_total, pdata: [data.month1.pointsEarned, data.month2.pointsEarned, data.month3.pointsEarned, data.month4.pointsEarned, data.month5.pointsEarned, data.month6.pointsEarned, data.month7.pointsEarned],
                   sdata: [data.month1.stepsWalked, data.month2.stepsWalked, data.month3.stepsWalked, data.month4.stepsWalked, data.month5.stepsWalked, data.month6.stepsWalked, data.month7.stepsWalked],
                   xdata: [data.month1.month, data.month2.month, data.month3.month, data.month4.month, data.month5.month, data.month6.month, data.month7.month]})                
              }
           console.log("month function called")
           db.transaction(tx => {
            for (let i = 0; i < this.state.sdata.length; ++i) {
              tx.executeSql('update userstat set m_pointsearned=?, month=?, xdatam=?  where statid=? ', [this.state.pdata[i], this.state.sdata[i], this.state.xdata[i], i+1], (tx, results) => {
                console.log("month resultsaffected: ", results.rowsAffected)
              });
            }
            
          });
          })
          .catch(error => {
            console.log('There has been a problem with your fetch operation: ' + error.message);
            })  
      }

      _1week()
      {
        console.log("inside week function")
        var p_total = 0;
        var s_total = 0;
        db.transaction(tx => {
          tx.executeSql('select w_pointsearned, week, xdataw from userstat where statid in (1,2,3,4,5,6,7)', [], (tx, results) => {
            console.log("results: ", results.rows.item(0))
            p_total = results.rows.item(0)['w_pointsearned'] + results.rows.item(1)['w_pointsearned'] + results.rows.item(2)['w_pointsearned'] + results.rows.item(3)['w_pointsearned'] + results.rows.item(4)['w_pointsearned'] + results.rows.item(5)['w_pointsearned'] + results.rows.item(6)['w_pointsearned']
            s_total = results.rows.item(0)['week'] + results.rows.item(1)['week'] + results.rows.item(2)['week'] + results.rows.item(3)['week'] + results.rows.item(4)['week'] + results.rows.item(5)['week'] + results.rows.item(6)['week']
            if (this.state.statValue != 1){
            this.setState({
              data1: results.rows.item(0)['w_pointsearned'], data2: results.rows.item(1)['w_pointsearned'],
          data3: results.rows.item(2)['w_pointsearned'], data4: results.rows.item(3)['w_pointsearned'], data5: results.rows.item(4)['w_pointsearned'], 
          data6: results.rows.item(5)['w_pointsearned'], data7: results.rows.item(6)['w_pointsearned'],stepsWalked: s_total,
           pointsEarned:p_total,xdata: [results.rows.item(0)['xdataw'], results.rows.item(1)['xdataw'], results.rows.item(2)['xdataw'], results.rows.item(3)['xdataw'], results.rows.item(4)['xdataw'], results.rows.item(5)['xdataw'], results.rows.item(6)['xdataw']],
           sdata: [results.rows.item(0)['week'],results.rows.item(1)['week'],
           results.rows.item(2)['week'], results.rows.item(3)['week'], results.rows.item(4)['week'], 
           results.rows.item(5)['week'], results.rows.item(6)['week']],
           pdate: [results.rows.item(0)['w_pointsearned'],results.rows.item(1)['w_pointsearned'],
           results.rows.item(2)['w_pointsearned'], results.rows.item(3)['w_pointsearned'], results.rows.item(4)['w_pointsearned'], 
           results.rows.item(5)['w_pointsearned'], results.rows.item(6)['w_pointsearned']]
            });
            }
            else {
              this.setState({
                data1: results.rows.item(0)['week'], data2: results.rows.item(1)['week'],
            data3: results.rows.item(2)['week'], data4: results.rows.item(3)['week'], data5: results.rows.item(4)['week'], 
            data6: results.rows.item(5)['week'], data7: results.rows.item(6)['week'],stepsWalked: s_total,
             pointsEarned:p_total,xdata: [results.rows.item(0)['xdataw'], results.rows.item(1)['xdataw'], results.rows.item(2)['xdataw'], results.rows.item(3)['xdataw'], results.rows.item(4)['xdataw'], results.rows.item(5)['xdataw'], results.rows.item(6)['xdataw']],
             sdata: [results.rows.item(0)['week'],results.rows.item(1)['week'],
             results.rows.item(2)['week'], results.rows.item(3)['week'], results.rows.item(4)['week'], 
             results.rows.item(5)['week'], results.rows.item(6)['week']],
             pdate: [results.rows.item(0)['w_pointsearned'],results.rows.item(1)['w_pointsearned'],
             results.rows.item(2)['w_pointsearned'], results.rows.item(3)['w_pointsearned'], results.rows.item(4)['w_pointsearned'], 
             results.rows.item(5)['w_pointsearned'], results.rows.item(6)['w_pointsearned']]
              });
              }
          });
        });

        fetch(hostip+'statWeek?uid='+uid,{
            method: 'GET'
          })
          .then(res => res.json())
          .then((data) => {
            p_total = data.week1.pointsEarned + data.week2.pointsEarned + data.week3.pointsEarned + data.week4.pointsEarned + 
            data.week5.pointsEarned + data.week6.pointsEarned + data.week7.pointsEarned
            s_total = data.week1.stepsWalked + data.week2.stepsWalked + data.week3.stepsWalked + data.week4.stepsWalked + 
            data.week5.stepsWalked + data.week6.stepsWalked + data.week7.stepsWalked
            console.log("week total steps: ", s_total)
            if (this.state.statValue != 1) {
            this.setState({ data1: data.week1.pointsEarned, data2: data.week2.pointsEarned,
              data3: data.week3.pointsEarned, data4: data.week4.pointsEarned, data5: data.week5.pointsEarned, 
              data6: data.week6.pointsEarned, data7: data.week7.pointsEarned,stepsWalked: s_total,
               pointsEarned: p_total, pdata: [data.week1.pointsEarned, data.week2.pointsEarned, data.week3.pointsEarned, data.week4.pointsEarned, data.week5.pointsEarned, data.week6.pointsEarned, data.week7.pointsEarned],
               sdata: [data.week1.stepsWalked, data.week2.stepsWalked, data.week3.stepsWalked, data.week4.stepsWalked, data.week5.stepsWalked, data.week6.stepsWalked, data.week7.stepsWalked],
               xdata: [data.week1.week, data.week2.week, data.week3.week, data.week4.week, data.week5.week, data.week6.week, data.week7.week]})
              }
              else {
                this.setState({ data1: data.week1.stepsWalked, data2: data.week2.stepsWalked,
                  data3: data.week3.stepsWalked, data4: data.week4.stepsWalked, data5: data.week5.stepsWalked, 
                  data6: data.week6.stepsWalked, data7: data.week7.stepsWalked,stepsWalked: s_total,
                   pointsEarned: p_total, pdata: [data.week1.pointsEarned, data.week2.pointsEarned, data.week3.pointsEarned, data.week4.pointsEarned, data.week5.pointsEarned, data.week6.pointsEarned, data.week7.pointsEarned],
                   sdata: [data.week1.stepsWalked, data.week2.stepsWalked, data.week3.stepsWalked, data.week4.stepsWalked, data.week5.stepsWalked, data.week6.stepsWalked, data.week7.stepsWalked],
                   xdata: [data.week1.week, data.week2.week, data.week3.week, data.week4.week, data.week5.week, data.week6.week, data.week7.week]})                
              }
           console.log("week function called")
           db.transaction(tx => {
            for (let i = 0; i < this.state.sdata.length; ++i) {
              tx.executeSql('update userstat set w_pointsearned=?, week=?, xdataw=? where statid=? ', [this.state.pdata[i], this.state.sdata[i], this.state.xdata[i], i+1], (tx, results) => {
                console.log("week resultsaffected: ", results.rowsAffected)
              });
            }
            
          });
          })
          .catch(error => {
            console.log('There has been a problem with your fetch operation: ' + error.message);
            })  
      }

      _1year()
      {
        fetch('http://192.168.0.103:5000/statYear',{
            method: 'GET'
          })
          .then(res => res.json())
          .then((data) => {
            this.setState({ data1: data.year1.pointsEarned, data2: data.year2.pointsEarned,
              data3: data.year3.pointsEarned, data4: data.year4.pointsEarned, data5: data.year5.pointsEarned, 
              data6: data.year6.pointsEarned, data7: data.year7.pointsEarned,stepsWalked: data.dailyStepsWalked,
               pointsEarned: data.dailyPointsEarned})
  
           console.log("week function called")
          })
          .catch(console.log)
      }



      changeButtonColor1= () => {
        this.setState({ opacity1: 1,opacity2: 0.5, opacity3: 0.5}); 
      }
      changeButtonColor2= () => {
        this.setState({ opacity2: 1,opacity1: 0.5, opacity3: 0.5});  
      }
      changeButtonColor3= () => {
        this.setState({ opacity3: 1,opacity2: 0.5, opacity1: 0.5});  
      }
      changeButtonColor4= () => {
        this.setState({ buttonColor4: 'rgb(254, 170, 58)', buttonColor2: '#7b899b', buttonColor1: '#7b899b', buttonColor3: '#7b899b' }); 
      }

      statValueStepButton= () => {
        this.setState({statValue: 1, stepsButton: 1, pointButton: .5, data1: this.state.sdata[0], data2: this.state.sdata[1],
          data3: this.state.sdata[2], data4: this.state.sdata[3], data5: this.state.sdata[4], data6: this.state.sdata[5], data7: this.state.sdata[6]})
      }

      statValuePointButton = () => {
        this.setState({statValue: 0, pointButton: 1, stepsButton: .5, data1: this.state.pdata[0], data2: this.state.pdata[1],
        data3: this.state.pdata[2], data4: this.state.pdata[3], data5: this.state.pdata[4], data6: this.state.pdata[5], data7: this.state.pdata[6]})
      }
      buttonlog= () => {
        console.log("button clicked")
      }

    constructor(props) {
        super(props);
        
        this.state = {
            data1: 10,
            data2: 10,
            data3: 10,
            data4: 10,
            data5: 10,
            data6: 10,
            data7: 10,
            day1: '',
            day2: '',
            day3: '',
            day4: '',
            day5: '',
            day6: '',
            day7: '',
            xdata:[],
            stepsWalked: 10,
            pointsEarned: 10,
            position: "flex-end",
            opacity1: 1,
            opacity2: .5,
            opacity3: .5,
            sdata: [],
            sid: [1,2,3,4,5,6,7],
            stepsButton: 1,
            statValue: 1,
            pointButton: .5,
            marginPosition: '0%'
        };
        console.log("Global user id: ", uid)

        var p_total = 0;
        db.transaction(tx => {
          tx.executeSql('select day,d_pointsearned, xdatad from userstat where statid in (1,2,3,4,5,6,7)', [], (tx, results) => {
            console.log("results: ", results.rows.item(0))
            p_total = results.rows.item(0)['day'] + results.rows.item(1)['day'] + results.rows.item(2)['day'] + results.rows.item(3)['day'] + results.rows.item(4)['day'] + results.rows.item(5)['day'] + results.rows.item(6)['day']
            this.setState({
              data1: results.rows.item(0)['d_pointsearned'], data2: results.rows.item(1)['d_pointsearned'],
          data3: results.rows.item(2)['d_pointsearned'], data4: results.rows.item(3)['d_pointsearned'], data5: results.rows.item(4)['d_pointsearned'], 
          data6: results.rows.item(5)['d_pointsearned'], data7: results.rows.item(6)['d_pointsearned'],stepsWalked: results.rows.item(0)['day'],
           pointsEarned:results.rows.item(0)['d_pointsearned'],xdata: [results.rows.item(0)['xdatad'], results.rows.item(1)['xdatad'], results.rows.item(2)['xdatad'], results.rows.item(3)['xdatad'], results.rows.item(4)['xdatad'], results.rows.item(5)['xdatad'], results.rows.item(6)['xdatad']]
            });
          });
        });
      
    }


    render() {
        const fill = 'rgb(254, 170, 58)'
        const data = [this.state.data1,this.state.data2,this.state.data3,this.state.data4,
            this.state.data5,this.state.data6,this.state.data7];
        const xvalue=[10,20,30,40,50,60,70]
            const axesSvg = { fontSize: 10, fill: 'white' };
            const verticalContentInset = { top: 10, bottom: 10 }
            const xAxisHeight = 30

            /* const ydata = [0, 200, 400, 600, 800, 1000, 1200, 1400, 1600] */
            const ydata = [0, this.state.data1,this.state.data2,this.state.data3,this.state.data4,
              this.state.data5,this.state.data6,this.state.data7]
            console.log("inside render: ", this.state.xdata)
            console.log("rendered y axis: ", this.state.data7)


/***************************************************************************************************************** */
        return (
            <ImageBackground
            source={back_image}
            style={{width: '100%', height: '100%'}}>
                    <View style={{ marginLeft: 50, height: 10, width : 90, marginLeft: 170,justifyContent: 'center', alignItems: 'center', flexDirection: 'row', paddingBottom: 20, paddingTop: 20}}>
                        <TouchableOpacity activeOpacity = { 1 }  onPress={() => {this._daily(),this.changeButtonColor1()}}>
                            <View style={{flex: 1,flexDirection: "row",paddingTop: 50,alignItems: "center",justifyContent: "center"}}>
                                <Image source={para} style={{ resizeMode: "contain",alignItems: "center",justifyContent: "center" ,width: 130, height: 130, marginRight: 70, opacity: this.state.opacity1}}/>
                                <Text style={{fontSize: 15, position: "absolute",color: "white", paddingTop: 50, paddingRight: 70}}>Daily</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity activeOpacity = { 1 } onPress={() => {this._1week(),this.changeButtonColor2()}}>
                            <View style={{flex: 1,flexDirection: "row",paddingTop: 50,alignItems: "center",justifyContent: "center",marginRight: 20}}>
                                <Image source={para} style={{ resizeMode: "contain",alignItems: "center",justifyContent: "center" ,width: 130, height: 130, opacity: this.state.opacity2}}/>
                                <Text style={{fontSize: 15,  position: "absolute",color: "white", paddingTop: 50}}>Weekly</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity activeOpacity = { 1 } onPress={() => {this._1month(),this.changeButtonColor3()}}>
                            <View style={{flex: 1,flexDirection: "row",paddingTop: 50,alignItems: "center",justifyContent: "center", marginLeft: 30}}>
                                <Image source={para} style={{ resizeMode: "contain",alignItems: "center",justifyContent: "center" ,width: 130, height: 130, opacity: this.state.opacity3}}/>
                                <Text style={{fontSize: 15,  position: "absolute",color: "white", paddingTop: 50}}>Monthly</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                          <TouchableOpacity activeOpacity = { 1 } onPress={() => {this.statValueStepButton()}}>
                              <View style={{flex: 1,paddingTop: 50,alignItems: "center",justifyContent: "center", marginLeft: 50}}>
                                  <Image source={para} style={{ resizeMode: "contain",alignItems: "center",justifyContent: "center" ,width: 130, height: 130, opacity: this.state.stepsButton}}/>
                                  <Text style={{fontSize: 15,  position: "absolute",color: "white", paddingTop: 50}}>Steps</Text>
                              </View>
                          </TouchableOpacity>
                          <TouchableOpacity activeOpacity = { 1 } onPress={() => {this.statValuePointButton()}}>
                              <View style={{flex: 1,paddingTop: 50,alignItems: "center",justifyContent: "center", marginRight: '77%'}}>
                                  <Image source={para} style={{ resizeMode: "contain",alignItems: "center",justifyContent: "center" ,width: 130, height: 130, opacity: this.state.pointButton}}/>
                                  <Text style={{fontSize: 15,  position: "absolute",color: "white", paddingTop: 50}}>Points</Text>
                              </View>
                          </TouchableOpacity>
                    </View>
                    <View style={{ flexDirection: "row" }} >
                  <YAxis
                    data={ydata}
                    style={{
                      backgroundColor: "transparent",
                      flexGrow: 0,
                    }}
                    svg={axesSvg}
                    contentInset={{ top: 40, bottom: 35 }}
                    formatLabel={(value, index) => value}
                    numberOfTicks={7}
                  />
                  <View style={{ flex: 1, marginLeft: 5 }}>
                <BarChart
                    style={{ height: 300 }}
                    data={ data }
                    svg={{ fill }}
                    gridMin={0}
                    contentInset={{ top: 40, bottom: 10, left: 5, right: 5}}
                    spacingInner={0.8}
                    spacingOuter={0.4}
                    numberOfTicks={7}

                >
                    <Grid/>
                </BarChart>
                <View style={{width: '100%', height: 25, opacity: .6,backgroundColor: '#8ab0bb', textAlignVertical: "center"}}>
                  <XAxis
                      data={this.state.xdata}
                      style={{paddingTop: 6}}
                      formatLabel={(value,index) => `${this.state.xdata[index]}`}
                      contentInset={{left: 40, right: 25}}
                      svg={{ fontSize: 15, fontWeight: "bold", fill: 'rgb(255,255,255)' }}
                  />
                </View>
                </View>
                </View>
            <View style= {{flexDirection:'column',flex:1}}>
                    <View style={ {flexDirection: 'row',marginLeft: '15%', width: 320, height: 50, backgroundColor: '#d6d2d295', marginBottom: '3%', marginTop: '4%',borderRadius:10, borderWidth: 1, borderColor: '#d6d2d295'}} >
                            <Text style={{textAlign: 'left', marginLeft: '4%', fontWeight: "bold", fontFamily: 'sans-serif', fontSize: 25, width: 100}}>{this.state.stepsWalked}</Text>  
                            <Text style={{textAlign: 'right', marginTop: '3%', fontFamily: 'sans-serif', paddingLeft: '32%'}}>steps walked</Text>           
                    </View>
                    <View style={{flexDirection: 'row',marginLeft: '15%', width: 320, height: 50, backgroundColor: '#d6d2d295', borderRadius:10, borderWidth: 1, borderColor: '#d6d2d295'}} >

                        <Text style={{textAlign: 'left', marginLeft: '4%', fontWeight: "bold", fontFamily: 'sans-serif', fontSize: 25, width: 100}}>{this.state.pointsEarned}</Text>  
                        <Text style={{textAlign: 'right', paddingTop: '3%', fontFamily: 'sans-serif', paddingLeft: '32%'}}>points earned</Text> 
                        {this.cityLocation(),console.log('position',  this.state.position)}
                    </View>
                    {/* <View style={[styles.container, { justifyContent: 'this.state.position', marginLeft: 30,flexDirection: 'row'},{width: screenWidth, height: 38, marginBottom: '3%'}]}> */}
                    <View style={[styles.container, { justifyContent: 'flex-end', marginLeft: this.state.marginPosition,flexDirection: 'row'},{width: screenWidth, height: 38, marginBottom: '3%'}]}>
                        
                        
                        <Image source={city} style = {{ width: 500, height: 200}} />
                        
                    </View>
            </View>

        </ImageBackground>
        )


    }
 
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        paddingTop: 64,
        paddingBottom: 32
      },
    button: {
      backgroundColor: "#DDDDDD"
    },
    line: {
      borderBottomWidth: 1,
      borderColor: '#f48a92'
    },
    parallelogram: {
      width: 150,
      height: 100
    }   
  });

  