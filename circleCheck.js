import React, { useEffect, useState } from 'react'
import { BarChart, Grid } from 'react-native-svg-charts'
import { ImageBackground, Image } from 'react-native';
import { StyleSheet, View, Button, SafeAreaView, Alert,UIManager,LayoutAnimation,TouchableOpacity, ScrollView} from 'react-native';
import city from "./cityscape.png";
import Svg, { Circle, Path, Rect, Text } from "react-native-svg";


const bgLayerOpacity = "0.2";
const progressLayerOpacity = "1";
const firstCascade = "1.25";
const secondCascade = "2.5";

const secondaryColor = "#C56490";


function generateArc(percentage, cx, cy, radius) {

    console.log(`cprogress: Percentage:${percentage}, cx:${cx}, cy:${cy}, radius:${radius}`);

    if (percentage === 100) percentage = 99.999
    const a = percentage * 2 * Math.PI / 100 // angle (in radian) depends on percentage
    const r = radius // radius of the circle
    var rx = r,
        ry = r,
        xAxisRotation = 0,
        largeArcFlag = 1,
        sweepFlag = 1,
        xadjust = r * Math.sin(a),
        yadjust = r * Math.cos(a),
        x = cx + xadjust,
        y = cy - yadjust
    if (percentage <= 50) {
        largeArcFlag = 0;
    } else {
        largeArcFlag = 1
    }

    return `A${rx} ${ry} ${xAxisRotation} ${largeArcFlag} ${sweepFlag} ${x} ${y}`;
}


const CascadingCircularProgress = (props) => (
    <Svg width={props.boxWidth} height={props.boxHeight} viewBox={`0 0 ${props.boxWidth} ${props.boxHeight}`}>
        {/* Outer progress : Full radius*/}
        <Circle
            stroke={props.bgColor}
            strokeWidth={props.width}
            strokeOpacity={bgLayerOpacity}
            r={props.r}
            cx={props.cx}
            cy={props.cy}
            fill="none"
        />
        <Path
            stroke={props.progressColor}
            strokeWidth={props.width}
            strokeDasharray={props.dashArray}
            strokeOpacity={progressLayerOpacity}
            d={`M${props.cx} ${(props.cy) - props.r} 
          ${generateArc(props.progressPcnt, props.cx, props.cy, props.r)}`}
        />
        {/* First Inner progress : Radius reduced by firstCascade times width */}
        <Circle
            stroke={props.bgColor}
            strokeWidth={props.width}
            strokeOpacity={bgLayerOpacity}
            r={props.r - (firstCascade * props.width)}
            cx={props.cx}
            cy={props.cy}
            fill="none"
        />
        <Path
            stroke={props.progressColor}
            strokeWidth={props.width}
            strokeDasharray={props.dashArray}
            strokeOpacity={progressLayerOpacity}
            d={`M${props.cx} ${(props.cy) - (props.r - (firstCascade * props.width))} 
          ${generateArc(props.progressPcnt, props.cx, props.cy, props.r - (firstCascade * props.width))}`}
        />
        {/* Second Inner progress : Radius reduced by secondCascade times width */}
        <Circle
            stroke={props.bgColor}
            strokeWidth={props.width}
            strokeOpacity={bgLayerOpacity}
            r={props.r - (secondCascade * props.width)}
            cx={props.cx}
            cy={props.cy}
            fill="none"
        />
        <Path
            stroke={props.progressColor}
            strokeWidth={props.width}
            strokeDasharray={props.dashArray}
            strokeOpacity={progressLayerOpacity}
            d={`M${props.cx} ${(props.cy) - (props.r - (secondCascade * props.width))} 
          ${generateArc(props.progressPcnt, props.cx, props.cy, props.r - (secondCascade * props.width))}`}
        />
        <Text
            stroke="white"
            fill="white"
            fontSize="15"
            x={props.cx - 25}
            y={props.cy - 20}
            textAnchor="start"
        >
            {props.progressPcnt}/100%
        </Text>
        <Text
            stroke="white"
            fill="white"
            fontSize="12"
            x={props.cx - 30}
            y={props.cy + 10}
            textAnchor="start"
        >
            of daily goal
        </Text>
        <Text
            stroke="white"
            fill="white"
            fontSize="12"
            x={props.cx - 25}
            y={props.cy + 30}
            textAnchor="start"
        >
            completed
        </Text>
        {/* <Rect
            x="1"
            y="1"
            width={props.boxWidth - 2}
            height={props.boxHeight - 2}
            fill="none"
            stroke="red"
            strokeWidth="1"
        /> */}
    </Svg>
);


export default class pcheck extends React.PureComponent {
  

    render() {

/***************************************************************************************************************** */
        return (
            <ImageBackground
            source={require('./background.png')}
            style={{width: '100%', height: '100%'}}>
            <CascadingCircularProgress/>

        </ImageBackground>
        )


    }
 
}



