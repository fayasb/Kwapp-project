import React from 'react';  
import {ImageBackground, Image, StyleSheet, Text,TextInput,TouchableOpacity, View,Button, Dimensions, PixelRatio} from 'react-native';  
import { createBottomTabNavigator, createAppContainer} from 'react-navigation';  
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';  
import Icon from 'react-native-vector-icons/Ionicons'; 
import profile from './profile.png'
import newrewards from './newRewards'
import week from './week-butt'
import statpage from './statNextButton'
import App from './homePageClass'
import settings from './settings'
import challenges from './challenges'
import googleFitExample from './kwapFit'
import settingsImage from './android/app/src/main/assets/settings_icon.png'
import rewardsImage from './android/app/src/main/assets/rewards_icon.png'
import challengeImage from './android/app/src/main/assets/challenge_icon.png'
import homeImage from './android/app/src/main/assets/home_icon.png'
import statsImage from './android/app/src/main/assets/stats_icon.png'
import profImage from './profileImage'
class HomeScreen extends React.Component {  
  render() {  
    return (  
        <View style={styles.container}>  
          <Text>Home Screen</Text>  
        </View>  
    );  
  }  
}  
class ProfileScreen extends React.Component {  
  render() {  
    return (  
        <View style={styles.container}>  
          <Text>Profile Screen</Text>  
        </View>  
    );  
  }  
}  

class StepScreen extends React.Component {  
    render() {  
      return (  
          <View style={styles.container}>  
            <Text>Step Screen</Text>  
          </View>  
      );  
    }  
  }  

class ImageScreen extends React.Component {  
    render() {  
        return (  
            <View style={styles.container}>  
                <Text>Image Screen</Text>  
            </View>  
        );  
    }  
}  
class CartScreen extends React.Component {  
    render() {  
        return (  
            <View style={styles.container}>  
                <Text>Cart Screen</Text>  
            </View>  
        );  
    }  
}  
class Regist extends React.Component {  
    render() {  
        return (  
            <View style={styles.container}>  
                <Text>Registration</Text>  
            </View>  
        );  
    }  
}  

const {
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
  } = Dimensions.get('window');
  
  // based on phone's scale
  const scale = SCREEN_WIDTH / 320;
const norm = (size) => {
    const newSize = size * scale 
    let temp = size / PixelRatio.getFontScale()
    return Math.round(PixelRatio.roundToNearestPixel(temp))
    
  };


const styles = StyleSheet.create({  
    container: {  
        flex: 1,  
        justifyContent: 'center',  
        alignItems: 'center'  
    },
});  
const TabNavigator = createMaterialBottomTabNavigator( 
    { 

        Home: { screen: App,  
            navigationOptions:{                  
                tabBarOptions: { allowFontScaling: false },
                tabBarLabel: <Text style={{fontSize: norm(11)}}>Home</Text>,  
                tabBarIcon: ({ tintColor }) => (  
                    <View>  
                        <Image source={homeImage} style={{width: 23, height: 23, resizeMode: 'contain'}}/> 
                    </View>),
                activeColor: '#f1f1f1',  
                inactiveColor: '#f1f1f1',  
                barStyle: { backgroundColor: '#546b85' }  
            }
        },  
        Profile: { screen: statpage,  
            navigationOptions:{  
                tabBarLabel: <Text style={{fontSize: norm(11)}}>Stat</Text>,  
                tabBarIcon: ({ tintColor }) => (  
                    <View>  
                        <Image source={statsImage} style={{width: 23, height: 23, resizeMode: 'contain'}}/> 
                    </View>),  
                activeColor: '#f1f1f1',  
                inactiveColor: '#f1f1f1',  
                barStyle: { backgroundColor: '#546b85' }, 
                allowFontScaling: false 
            }  
        },  
        Steps: { screen: challenges,  
            navigationOptions:{  
                tabBarLabel: <Text style={{fontSize: norm(11)}}>Achievements</Text>,  
                tabBarIcon: ({ tintColor }) => (  
                    <View>  
                        <Image source={challengeImage} style={{width: 23, height: 23, resizeMode: 'contain'}}/>  
                    </View>),  
                activeColor: '#f1f1f1',  
                inactiveColor: '#f1f1f1',  
                barStyle: { backgroundColor: '#546b85' },  
            }  
        },  
        Image: { screen: newrewards,  
            navigationOptions:{  
                tabBarLabel: <Text style={{fontSize: norm(11)}}>Rewards</Text>,  
                tabBarIcon: ({ tintColor }) => (  
                    <View>  
                        <Image source={rewardsImage} style={{width: 23, height: 23, resizeMode: 'contain'}}/>   
                    </View>),  
                activeColor: '#f1f1f1',  
                inactiveColor: '#f1f1f1',  
                barStyle: { backgroundColor: '#546b85' },  
            }  
        }, 
        Cart: {  
            screen: settings,  
            navigationOptions:{  
                tabBarLabel: <Text style={{fontSize: norm(11)}}>Settings</Text>,  
                tabBarIcon: ({ tintColor }) => (  
                    <View>  
                        <Image source={settingsImage} style={{width: 23, height: 23, resizeMode: 'contain'}}/> 
                    </View>), 
                activeColor: '#f1f1f1',  
                inactiveColor: '#f1f1f1',  
                barStyle: { backgroundColor: '#546b85' } 
            }  
        },  
    },  
    {  
      initialRouteName: "Home",  
      activeColor: '#f1f1f1',  
      inactiveColor: '#f1f1f1',  
      barStyle: { backgroundColor: '#546b85' },  
      allowFontScaling: false
    },
    {
        tabBarOptions: {
        allowFontScaling: false
        }
    }
);  

const AppTabNavigation = createAppContainer(TabNavigator);  
export class TabApp extends React.Component {
    render() {
      return (
          <AppTabNavigation/>
      );
    }
  }
export default TabApp;  
