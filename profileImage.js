import ImagePicker from 'react-native-image-picker';
import { View, Text, Image, styles, ImageBackground, Button, TouchableOpacity } from 'react-native';
import React from 'react';  
import defaultImage from './android/app/src/main/assets/defaultImage.png'

// More info on all the options is below in the API Reference... just some common use cases shown here
const options = {
  title: 'Select Avatar',
  customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};
 
/**
 * The first arg is the options object for customization (it can also be null or omitted for default options),
 * The second arg is the callback which sends object: response (more info in the API Reference)
 */
export default class profImage extends React.Component {

    _uploadImage(){
        console.log("function called")
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);
           
            if (response.didCancel) {
              console.log('User cancelled image picker');
            } else if (response.error) {
              console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
              console.log('User tapped custom button: ', response.customButton);
            } else {
              const source = { uri: response.uri };
           
              // You can also display the image using data:
              // const source = { uri: 'data:image/jpeg;base64,' + response.data };
           
              this.setState({
                avatarSource: source,
              });
            }
          });
    }
    componentDidMount(){

    }

constructor(props){
    super(props);
    this.state = {
        avatarSource: defaultImage
    }
};

render() {

    /***************************************************************************************************************** */
            return (
                <ImageBackground
                source={require('./background.png')}
                style={{width: '100%', height: '100%'}}>
    
                    <View style={{flex: 1,flexDirection: "row",alignItems: "center",justifyContent: "center"}}>
                    <TouchableOpacity activeOpacity = { 1 } onPress={() => {this._uploadImage()}} style={{height:40, alignItems:'center', width: 150, backgroundColor: 'rgb(254, 170, 58)',borderRadius: 10}}>
                            <Text style={{color: 'white',fontSize: 25}}> Done </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{flex: 1, alignItems: 'center'}}>
                        <Text>Uploaded Image</Text>
                        <Image source={this.state.avatarSource} style={{width: 100, height: 100,borderRadius: 100/2, borderWidth: 5, borderColor: 'white'}} />
                    </View>
            </ImageBackground>
            )
    
    
        }
     
    }

