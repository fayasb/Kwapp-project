package com.demo;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import java.util.ArrayList;
import java.util.List;

public class AccelerometerTracker implements SensorEventListener {

    // private variables to monitor sensors
    private SensorManager sensorManager;
    private Sensor mAccelerometer;
    private Context mContext;

    // object to store sensor data
    private class sensorData {
        public float[] data = new float[3];

        // ctor
        public sensorData (float x, float y, float z) {
            data[0] = x;
            data[1] = y;
            data[2] = z;
        }
    }

    // private variables handling the step tracking and calculation
    private List<sensorData> sensorRead = new ArrayList<sensorData>();
    private List<sensorData> toProcess = new ArrayList<sensorData>();
    private int stepSum = 0;
    private double prevAcceleration = 0;
    private double accelThreshold = 4.2;

    // constructor
    AccelerometerTracker (Context currentContext) {

        mContext = currentContext;
        sensorManager = (SensorManager) mContext.getSystemService(Context.SENSOR_SERVICE);

        if (sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {
            mAccelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        } else {
            // no accelerometer found
            mAccelerometer = null;
            System.out.println("Error! No Accelerometer found.");
        }
    }


    @Override
    public final void onAccuracyChanged (Sensor sensor, int accuracy) {
        // Nothing to be done here
    }

    @Override
    public final void onSensorChanged (SensorEvent event) {
        // Sensor change received
        sensorData d = new sensorData(event.values[0], event.values[1], event.values[2]);
        // Add to sensor read array
        sensorRead.add(d);
    }

    public void startSensor () {
        // Starts the sensor listener
        System.out.println("Starting sensor tracking...");

        if (mAccelerometer != null) {
            try {
                sensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
                System.out.println("Sensor tracking started.");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Error: no accelerometer.");
        }
    }


    public void stopSensor () {
        // Stop the sensor listener
        sensorManager.unregisterListener(this);
        System.out.println("Sensor tracking stopped!");
    }


    public void moveToProcess () {
        // copy sensorRead to toProcess and clear sensorRead
        for (sensorData temp : sensorRead) {

            sensorData newData = new sensorData(temp.data[0], temp.data[1], temp.data[2]);

            toProcess.add(newData);
        }

        // Clear sensorRead to read new data
        sensorRead.clear();

        System.out.println("Moved to toProcess...");
    }


    public int processSteps () {
        // Process accelerometer values in toProcess and clear toProcess
        List<Integer> stepArray = new ArrayList<Integer>();

        for (sensorData temp : toProcess) {

            // follows sum squared acceleration logic : sqrt(x^2 + y^2 + z^2)
            double result = Math.sqrt(Math.pow(temp.data[0], 2) + Math.pow(temp.data[1], 2) + Math.pow(temp.data[2], 2));

            if ((prevAcceleration != 0) && ((result - prevAcceleration) > accelThreshold)) {
                stepArray.add(1);
            } else {
                stepArray.add(0);
            }

            prevAcceleration = result;
        }

        System.out.println("Finished processing current batch of sensor data.");
        System.out.println(stepArray.toString());

        // keeping it basic
        // calculating sum of stepArray
        int result = 0;
        for (Integer t : stepArray) result += t;

        System.out.println("Current step result:" + result);
        stepSum = result;

        // clearing the toProcess pool
        toProcess.clear();

        return stepSum;
    }



}
