package com.demo;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;

import androidx.core.app.NotificationCompat;

import java.util.Timer;
import java.util.TimerTask;

import javax.annotation.Nullable;

public class TrackingService extends Service {

    public static final String CHANNEL_ID = "ForegroundTrackingServiceChannel";

    // For counting steps
    private int counter = 0;
    private Timer updateCounter = new Timer();
    private Long startTimeStamp;

    private AccelerometerTracker aTracker;

    // function runs when the service object is created
    @Override
    public void onCreate () {
        super.onCreate();
    }

    // function assigning a notification channel
    // applicable only on android versions greater that Android O
    public void createNotificationChannel () {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Foreground Tracking Service Channel",
                    NotificationManager.IMPORTANCE_LOW
            );

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }

    // function runs when the service starts
    @Override
    public int onStartCommand (Intent intent, int flags, int startId) {
        String input = intent.getStringExtra("inputExtra");
        // defining the notification channel
        createNotificationChannel();
        // creating a notification for the foreground service
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("kWapp tracking!")
                .setContentText(input)
                .setContentIntent(pendingIntent)
                .build();

        startForeground(1, notification);

        // do heavy work on background thread
        // starting the sensor reads

        aTracker = new AccelerometerTracker(this);
        aTracker.startSensor();

        System.out.println("Foreground service is running!");

        // starting sensor processing
        // timed for every 5 seconds
        System.out.println("Scheduling broadcast of steps...");

        updateCounter.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    aTracker.moveToProcess();

                    counter = aTracker.processSteps();

                    sendCounter();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 0, 5000);

        // stopSelf()

        return START_NOT_STICKY;
    }

    // destroy everything when foreground service is stopped
    @Override
    public void onDestroy () {
        super.onDestroy();

    }

    // broadcast the read steps
    public void sendCounter () {
        System.out.println("Broadcasting counter...");
        // create an intent and broadcast with current counter
        Intent currCount = new Intent()
                .setAction("FETCHCOUNT")
                .putExtra("counter", counter);

        sendBroadcast(currCount);
    }


    @Nullable
    @Override
    public IBinder onBind (Intent intent) {
        return null;
    }
}
