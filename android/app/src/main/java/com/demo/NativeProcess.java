package com.demo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;

import java.util.HashMap;
import java.util.Map;

public class NativeProcess extends ReactContextBaseJavaModule {

    // for react context
    private static ReactApplicationContext reactContext;

    // for Toast messages
    private static final String DURATION_SHORT_KEY = "SHORT";
    private static final String DURATION_LONG_KEY = "LONG";

    // for Foreground service
    private static Boolean isStarted = false;

    // for counting steps
    private int currentCounter = -10;

    // constructor
    NativeProcess (ReactApplicationContext context) {
        super (context);
        reactContext = context;
    }

    // defining name for this native module
    @Override
    public String getName () {
        return "NativeProcess";
    }

    // defining constants to measure toast time
    @Override
    public Map<String, Object> getConstants () {
        final Map<String, Object> constants = new HashMap<>();

        constants.put(DURATION_LONG_KEY, Toast.LENGTH_LONG);
        constants.put(DURATION_SHORT_KEY, Toast.LENGTH_SHORT);

        return constants;
    }

    // defining method to toast messages
    // method is made visible to react side
    @ReactMethod
    public void toast (String message, int duration) {

        Toast.makeText(getReactApplicationContext(), message, duration)
                .show();
    }

    // starting the foreground service
    @ReactMethod
    public void startService () {

        System.out.println("Starting foreground service for tracking.");

        Intent serviceIntent = new Intent(getCurrentActivity(), TrackingService.class);
        serviceIntent.putExtra("inputExtra", "kWapp is tracking your steps.");

        ContextCompat.startForegroundService(getCurrentActivity(), serviceIntent);

        System.out.println("Started foreground service.");
    }


    // stopping the foreground service
    @ReactMethod
    public void stopService () {

        System.out.println("Stopping the foreground service.");

        if (isStarted) {

            // stopping the broadcast receiver
            stopTracking();

            Intent serviceIntent = new Intent(getCurrentActivity(), TrackingService.class);
            getCurrentActivity().stopService(serviceIntent);

            // could throw following exception
            // java.lang.NullPointerException: Attempt to invoke virtual method
            // 'java.lang.String android.content.Context.getPackageName()' on a null object reference

            System.out.println("Stopped foreground tracking service.");
        } else {
            System.out.println("Foreground tracking is not active.");
        }
    }

    // Broadcast receiver to receive updated counter
    private BroadcastReceiver receiveCount = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            System.out.println("Received valid broadcast... Parsing.");

            int count = (int) intent.getExtras().get("counter");

            System.out.println("Received count: " + count);

            if (currentCounter > 0) {
                currentCounter += count;
            } else {
                currentCounter = count;
            }
        }
    };

    // Function to get currently counted steps
    @ReactMethod
    public void getStatus (Callback successCallback) {

        System.out.println("Called getStatus...");

        System.out.println("Current count in Bulb: " + currentCounter);

        System.out.println("Invoking callback...");
        successCallback.invoke(null, currentCounter);
    }

    // start the broadcast receiver and track step count
    @ReactMethod
    public void startTracking () {
        System.out.println("Starting receiver...");

        try {
            // Registering broadcast receiver to receive updated count
            IntentFilter currCountFilter = new IntentFilter("FETCHCOUNT");
            getCurrentActivity().registerReceiver(receiveCount, currCountFilter);

            isStarted = true;

        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("Updated count in Bulb: " + currentCounter);

    }

    // stop the broadcast receiver
    @ReactMethod
    public void stopTracking () {

        System.out.println("Unregistering receiver...");
        getCurrentActivity().unregisterReceiver(receiveCount);

        isStarted = false;
    }

    // Function to check if tracking is active
    @ReactMethod
    public void  getIsStarted (Callback successCallback) {
        successCallback.invoke(null, isStarted);
    }

    // Function to reset count
    @ReactMethod
    public void resetCount (Callback successCallback) {
        int count = currentCounter;

        currentCounter = -10;

        successCallback.invoke(null, count);
    }
}
