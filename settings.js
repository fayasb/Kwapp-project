import React from 'react'

import { ImageBackground, Image, Button, TouchableOpacity, ScrollView } from 'react-native';
import { StyleSheet, View, Text, TextInput, Dimensions, PixelRatio } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import RNFetchBlob from 'rn-fetch-blob';
import Share from 'react-native-share';
import accSettings from './accSettings'
import back_image from './src/backgrounds/app_background.png'


const {
  width: SCREEN_WIDTH,
  height: SCREEN_HEIGHT,
} = Dimensions.get('window');

// based on phone's scale
const scale = SCREEN_WIDTH / 320;

export class settings extends React.PureComponent {

  profileSettings() {
    console.log("inside profile settings function")
    this.props.navigation.navigate("Profile")
  }
  _normalize(size) {
    const newSize = size * scale
    return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2

  }

  exportDatabase = () => {
    console.log("Exporting database...")
    console.log(`Directories available...`)
    console.log(`${RNFetchBlob.fs.dirs.DocumentDir}`)
    console.log("Copying db file to documents directory")

    RNFetchBlob.fs.cp('/data/data/com.demo/databases/tfayas199', `${RNFetchBlob.fs.dirs.DocumentDir}/kwappDB`)
      .then(() => {
        console.log("Copied!");

        console.log("sharing the file...");

        Share.open({
          title: "Share database file",
          message: "Sharing the local database file.",
          url: `file://${RNFetchBlob.fs.dirs.DocumentDir}/kwappDB`,
          subject: "kWapp DB file share",
        });
      })
      .catch((error) => {
        console.log(`Error! ${error}`)
      })
  }

  render() {


    /***************************************************************************************************************** */
    return (
      <ImageBackground
        source={back_image}
        style={{ width: '100%', height: '100%' }}>
        <View style={{ flexDirection: 'column', flex: 1 }}>
          <ScrollView>
            <View style={{ width: '100%', height: 100, alignItems: 'center', justifyContent: 'center' }}>
              <Text allowFontScaling={false} style={{ fontSize: this._normalize(30), color: 'white' }}>Settings</Text>
            </View>

            <TouchableOpacity onPress={() => { this.profileSettings() }} style={styles.box}>
              <Text allowFontScaling={false} style={{ color: 'rgb(254, 170, 58)', fontSize: this._normalize(22), marginTop: '5%' }}> Profile Settings </Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.box}>
              <Text allowFontScaling={false} style={{ color: 'rgb(254, 170, 58)', fontSize: this._normalize(22), marginTop: '5%' }}> Account Settings </Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.box}>
              <Text allowFontScaling={false} style={{ color: 'rgb(254, 170, 58)', fontSize: this._normalize(22), marginTop: '5%' }}> Privacy Policy </Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.box}>
              <Text allowFontScaling={false} style={{ color: 'rgb(254, 170, 58)', fontSize: this._normalize(22), marginTop: '5%' }}> Help </Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.box}>
              <Text allowFontScaling={false} style={{ color: 'rgb(254, 170, 58)', fontSize: this._normalize(22), marginTop: '5%' }}> Contact </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => { this.exportDatabase() }} style={styles.box}>
              <Text allowFontScaling={false} style={{ color: 'rgb(254, 170, 58)', fontSize: this._normalize(22), marginTop: '5%' }}> Export Data (test) </Text>
            </TouchableOpacity>

          </ScrollView>
        </View>
      </ImageBackground>
    )


  }

}

const norm = (size) => {
  const newSize = size * scale
  let temp = size / PixelRatio.getFontScale()
  return Math.round(PixelRatio.roundToNearestPixel(temp))

};

const Stack = createStackNavigator({
  Settings: {
    screen: settings,
    navigationOptions: {
      title: 'Settings',
      headerShown: false,
      headerLeft: () => null
    }
  },
  Profile: {
    screen: accSettings,
    navigationOptions: {
      title: <Text allowFontScaling={false} style={{ fontSize: norm(22), marginTop: '5%' }}> Profile Settings </Text>,
      allowFontScaling: false,
    }
  }
},
  {
    initialRouteName: 'Settings',

  },
);

class regApp extends React.Component {

  render() {
    return <Stack />;
  }
}

const AppNavigation = createAppContainer(Stack);

export default AppNavigation;


const styles = StyleSheet.create({
  item: {
    flexDirection: 'column',
    width: '100%',
    height: 200,
    marginLeft: '5%',
    marginRight: '10%',
    marginTop: '10%',
    marginBottom: '3%',
    alignItems: 'center',
    position: 'relative'


  },
  box: {
    backgroundColor: 'rgb(84, 106, 133)',
    marginLeft: '5%',
    width: '90%',
    height: 120,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: 'rgb(176, 186, 198)',
    borderWidth: 3
  }
})
