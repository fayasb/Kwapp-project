import React, { useEffect, useState, PureComponent } from 'react'
import { NavigationEvents } from 'react-navigation';
import { ImageBackground, Image, Dimensions, PixelRatio  } from 'react-native';
import { StyleSheet, View, Button, SafeAreaView, Text, TextInput, Alert,UIManager,LayoutAnimation,TouchableOpacity} from 'react-native';
import {SlideBarChart} from 'react-native-slide-charts'
import city from "./cityscape.png";
import * as scale from 'd3-scale';
import para from './para_snipped.png'
import back_image from './src/backgrounds/app_background.png'
import { ScrollView } from 'react-native-gesture-handler';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const window = Dimensions.get("window");
const screen = Dimensions.get("screen");
const wscale = window.width / 320;
UIManager.setLayoutAnimationEnabledExperimental &&
  UIManager.setLayoutAnimationEnabledExperimental(true);
  const screenWidth = Math.round(Dimensions.get('window').width);
  const contentInset = {top: 10}
 
export default class statpage extends React.PureComponent {
  errorCB(err) {
    console.log("SQL Error: " + err);
  }
   
  successCB() {
    console.log("SQL executed fine");
  }
   
  openCB() {
    console.log("Database OPENED");
  }
  



      

      _normalize(size) {
        const newSize = size * wscale 
        return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2
        
      }
      cityLocation() {
        if (this.state.stepsWalked > 5000 && this.state.stepsWalked <= 10000) {
            this.state.position = 'center'
        } else if (this.state.stepsWalked > 10000)
        {
            this.state.position = 'flex-start'
        }
        else {
            this.state.position = 'flex-end'
        }
    }      

    _getGraphWidth(arrayLength) {
      var newGraphSize= this.state.defaultGraphSize + arrayLength*40
      this.setState({graphSize: newGraphSize})
    }
     _rangeValue(xArray){
      var t = []
      for (let i=0; i<xArray.length ; ++i){
        t.push(xArray[i].y)
      }
      var max = Math.max.apply(null, t)
      var temp = max.toString().length;
      if (max>0){
        max= (Math.floor(max/Math.pow(10, temp-1))+1)*Math.pow(10,temp-1)
        console.log("max: ", max)
        return max
      }
      else {
        return 100
      }
    }

    _nextPrev(x){
      console.log(this.state.size)
      var stepsYMax, pointsYMax

        if (x==1){
          if (this.state.control<this.state.size-1){
            console.log("Next data")
            if (this.state.statValue==1){
              stepsYMax = this._rangeValue(this.state.arrayData[this.state.control+1]) 
              this.setState({xdata: this.state.arrayData[this.state.control+1], yAxisRange: [0, stepsYMax], xyData: this.state.arrayAxes[this.state.control+1], control: this.state.control+1, prev: this.state.prev+1})
            }
            else {
              pointsYMax = this._rangeValue(this.state.arrayPointData[this.state.next])
              this.setState({xdata: this.state.arrayPointData[this.state.control], yAxisRange: [0, pointsYMax], xyData: this.state.arrayAxes[this.state.control+1], control: this.state.control+1, prev: this.state.prev+1})
            }
            if (this.state.control==0){
              this.setState({present: null})
            }
          }
        }
        if (x==0){
          console.log("prev")
          if (this.state.control>0){
            if (this.state.statValue==1){
              stepsYMax = this._rangeValue(this.state.arrayData[this.state.control-1])
              this.setState({xdata: this.state.arrayData[this.state.control-1], yAxisRange: [0, stepsYMax], xyData: this.state.arrayAxes[this.state.control-1], control: this.state.control-1, prev: this.state.prev-1})
            }
            else {
              pointsYMax = this._rangeValue(this.state.arrayPointData[this.state.control-1]) 
              this.setState({xdata: this.state.arrayPointData[this.state.control-1], yAxisRange: [0, pointsYMax], xyData: this.state.arrayAxes[this.state.control-1], control: this.state.control-1, prev: this.state.prev-1})
            }
          }
        }
    }

      _daily(x=1) {
        //daily points fetching from local db
        var statAxes = []
        var pointsYMax = 0
        var stepsYMax = 0
        var stepArray = []
        var pointArray = []

        db.transaction(tx => {
          tx.executeSql('select DISTINCT dailypoints, dailysteps, timeid, statid, day, month, year from userstatdetails group by timeid order by year desc, month desc, week desc, day desc', [], (tx, results) => {

            if (Math.floor(results.rows.length/7)==0){
              this.setState({size: 1, loopCond: results.rows.length%7})
            }       
            else{
              this.setState({size: Math.floor(results.rows.length/7), loopCond: 7})
            }

            //Stacking step data into array size of 7
            try{
              db.transaction(tx => {
                tx.executeSql('select DISTINCT dailypoints, dailysteps, timeid, statid, day, month, year from userstatdetails group by timeid order by year desc, month desc, week desc, day desc', [], (tx, results) => {

                  for (let i = 0; i < this.state.size; ++i){
                    var stepEachArray = []
                    var pointEachArray = []
                    var statEachAxes = []
                      for (let j = 7*i; j<7*i+(this.state.loopCond); ++j){
                        stepEachArray.push({'x': results.rows.item(j)['day'], 'y': results.rows.item(j)['dailysteps']})
                        pointEachArray.push({'x': results.rows.item(j)['day'], 'y': results.rows.item(j)['dailypoints']})
                        if (results.rows.item(j)['month']>=10){
                          statEachAxes.push({'x': results.rows.item(j)['day'].toString() + '.' + results.rows.item(j)['month'] + '.' , 'y': results.rows.item(i)['dailysteps']})
                        }
                        else {
                          statEachAxes.push({'x': results.rows.item(j)['day'].toString() + '.0' + results.rows.item(j)['month'] + '.' , 'y': results.rows.item(i)['dailysteps']})
                        }
                      }
                      stepArray.push(stepEachArray)
                      pointArray.push(pointEachArray)
                      statAxes.push(statEachAxes)

                    }
                  console.log("pointarray: ", pointArray)
                  
                  /* statBar=[{ x: 1, y: 1 },{ x: 2, y: 2 },{ x: 3, y: 3 },{ x: 4, y: 4 },{ x: 5, y: 5 },{ x: 6, y: 6 },{ x: 7, y: 7 },{ x: 8, y: 8 },{ x: 9, y: 9 },{ x: 10, y: 10 }] */
      
                 
                  pointsYMax = this._rangeValue(pointArray[0]) 
                  stepsYMax = this._rangeValue(stepArray[0])
                
                  
                  this.setState({
                    stepsWalkedText: "steps walked today",
                    pointsEarnedText: "points earned today",
                    paddingSteps: this.boxWidth - 265,
                    paddingPoints: this.boxWidth - 265,
                    stepsWalked: results.rows.item(0)['dailysteps'],
                    pointsEarned: results.rows.item(0)['dailypoints'],
                    xyData: statAxes[0],
                    present: 'Today',
                    arrayData: stepArray,
                    arrayPointData: pointArray,
                    arrayAxes: statAxes,
                    yAxisRange: this.state.statValue==0 ? [0, pointsYMax]: [0, stepsYMax],
                    yLabel: this.state.statValue==0 ? 'Points' : 'Steps',
                    xdata: this.state.statValue==0 ? pointArray[0] : stepArray[0]
                  });
                });
              });
            }
            catch(err) {
              console.log(`----> (ERROR INSIDE DAILY FUNC) <----`, err);
            }
          });
        });
        
      }


      _1week()
      {
        var stepSum = 0
        var pointSum = 0
        var statAxes = []
        var pointsYMax = 0
        var stepsYMax = 0
        var stepArray = []
        var pointArray = []
        db.transaction(tx => {
          tx.executeSql('select sum(dailysteps) as dailysteps, sum(dailypoints) as dailypoints, week, day, month, year from userstatdetails group by week order by year desc, month desc, week desc, day desc', [], (tx, results) => {
            
            if (Math.floor(results.rows.length/7)==0){
              this.setState({size: 1, loopCond: results.rows.length%7})
            }       
            else{
              this.setState({size: Math.floor(results.rows.length/7), loopCond: 7})
            }
            console.log("size: ", this.state.size)
            try{
              db.transaction(tx => {
                tx.executeSql('select sum(dailysteps) as dailysteps, sum(dailypoints) as dailypoints, week, day, month, year from userstatdetails group by week order by year desc, month desc, week desc, day desc', [], (tx, results) => {
       
                //stacking week data in arrays of size 7
                for (let i = 0; i < this.state.size; ++i){
                  var stepEachArray = []
                  var pointEachArray = []
                  var statEachAxes = []
                    for (let j = 7*i; j<7*i+(this.state.loopCond); ++j){
                      stepEachArray.push({'x': results.rows.item(j)['day'], 'y': results.rows.item(j)['dailysteps']})
                      pointEachArray.push({'x': results.rows.item(j)['day'], 'y': results.rows.item(j)['dailypoints']})
                      if (results.rows.item(j)['month']>=10){
                        statEachAxes.push({'x': results.rows.item(j)['day'].toString() + '.' + results.rows.item(j)['month'] + '.' , 'y': results.rows.item(i)['dailysteps']})
                      }
                      else {
                        statEachAxes.push({'x': results.rows.item(j)['day'].toString() + '.0' + results.rows.item(j)['month'] + '.' , 'y': results.rows.item(i)['dailysteps']})
                      }
                    }
                    stepArray.push(stepEachArray)
                    pointArray.push(pointEachArray)
                    statAxes.push(statEachAxes)
                  }
                  console.log("steparray: ", stepArray)
                  for (let i=0;i<results.rows.length;++i){
                    stepSum = stepSum + results.rows.item(i)['dailysteps']
                    pointSum = pointSum + results.rows.item(i)['dailypoints']
                  }
                  
                  pointsYMax = this._rangeValue(pointArray[0]) 
                  stepsYMax = this._rangeValue(stepArray[0])
                  console.log("stepsymax: ", stepsYMax)

                  this.setState({
                    stepsWalkedText: "steps walked these weeks",
                    pointsEarnedText: "points earned these weeks",
                    paddingSteps: '3%',
                    paddingPoints: '2%',
                    stepsWalked: stepSum,
                    pointsEarned: pointSum,
                    xyData: statAxes[0],
                    present: 'This week',
                    yLabel: this.state.statValue==0 ? 'Points' : 'Steps',
                    arrayPointData: pointArray,
                    arrayData: stepArray,
                    yAxisRange: this.state.statValue==0 ? [0, pointsYMax]: [0, stepsYMax],
                    xdata: this.state.statValue==0 ? pointArray[0] : stepArray[0]
                  })
                });
              });
            }
            catch(err) {
              console.log(`----> (ERROR INSIDE WEEK FUNC) <----`, err);
            }
          });
        });
          
          

      }

      _1month()
      {
        var stepSum = 0
        var pointSum = 0
        var statAxes = []
        var pointsYMax = 0
        var stepsYMax = 0
        var stepArray = []
        var pointArray = []
        var months = {
          '0' : 'xx',
          '1' : 'Jan',
          '2' : 'Feb',
          '3' : 'Mar',
          '4' : 'Apr',
          '5' : 'May',
          '6' : 'Jun',
          '7' : 'Jul',
          '8' : 'Aug',
          '9' : 'Sep',
          '10' : 'Oct',
          '11' : 'Nov',
          '12' : 'Dec'
      }
      db.transaction(tx => {
        tx.executeSql('select sum(dailysteps) as dailysteps, sum(dailypoints) as dailypoints, month, year, timeid from userstatdetails group by month order by statid desc', [], (tx, results) => {
          console.log("results: ", results.rows.item(0))
          if (Math.floor(results.rows.length/7)==0){
            this.setState({size: 1, loopCond: results.rows.length%7})
          }       
          else{
            this.setState({size: Math.floor(results.rows.length/7), loopCond: 7})
          }
          console.log("size: ", this.state.size)
          try{
            db.transaction(tx => {
              tx.executeSql('select sum(dailysteps) as dailysteps, sum(dailypoints) as dailypoints, month, year, timeid from userstatdetails group by month order by statid desc limit 7', [], (tx, results) => {
                //month data is stacked in arrays of size 7
                for (let i = 0; i < this.state.size; ++i){
                  var stepEachArray = []
                  var pointEachArray = []
                  var statEachAxes = []
                    for (let j = 7*i; j<7*i+(this.state.loopCond); ++j){
                      stepEachArray.push({'x': results.rows.item(j)['month'], 'y': results.rows.item(j)['dailysteps']})
                      pointEachArray.push({'x': results.rows.item(j)['month'], 'y': results.rows.item(j)['dailypoints']})
                      statEachAxes.push({'x': months[results.rows.item(j)['month']] + ' ' + results.rows.item(j)['year'] , 'y': results.rows.item(j)['dailysteps']})
                    }
                    stepArray.push(stepEachArray)
                    pointArray.push(pointEachArray)
                    statAxes.push(statEachAxes)
                  }
                  console.log("steps array: ", stepArray)


                for (let i=0;i<results.rows.length;++i){
                  stepSum = stepSum + results.rows.item(i)['dailysteps']
                  pointSum = pointSum + results.rows.item(i)['dailypoints']
                }
                
                pointsYMax = this._rangeValue(pointArray[0]) 
                stepsYMax = this._rangeValue(stepArray[0])
  
                
                this.setState({
                  stepsWalkedText: "steps walked these months",
                  pointsEarnedText: "points earned these months",
                  paddingSteps: '0%',
                  paddingPoints: '0%', 
                  stepsWalked: stepSum,
                  pointsEarned: pointSum,
                  xyData: statAxes[0],
                  present: 'This month',
                  yAxisRange: this.state.statValue==0 ? [0, pointsYMax]: [0, stepsYMax],
                  xdata: this.state.statValue==0 ? pointArray[0] : stepArray[0],
                  yLabel: this.state.statValue==0 ? 'Points' : 'Steps',
                  arrayData: stepArray,
                  arrayPointData: pointArray
            })
              });
            });  
          }
          catch(err) {
            console.log(`----> (ERROR INSIDE MONTH FUNC) <----`, err);
          }
        });
      });
               
      }

      componentDidMount() {
        this._daily(this.state.statValue)
      }
      _challenges=() => {
        this.setState({menuView: true})
    }

      changeButtonColor1= () => {
        this.setState({ opacity1: 1,opacity2: 0.5, opacity3: 0.5}); 
      }
      changeButtonColor2= () => {
        this.setState({ opacity2: 1,opacity1: 0.5, opacity3: 0.5});  
      }
      changeButtonColor3= () => {
        this.setState({ opacity3: 1,opacity2: 0.5, opacity1: 0.5});  
      }
      changeButtonColor4= () => {
        this.setState({ buttonColor4: 'rgb(254, 170, 58)', buttonColor2: '#7b899b', buttonColor1: '#7b899b', buttonColor3: '#7b899b' }); 
      }

      statValueStepButton= () => {
        this.setState({statValue: 1, stepsButton: 'bold', pointButton: 'normal', stepsButtonColor: 'rgb(253, 179, 76)', pointButtonColor: '#d6d2d295', stepsLineColor: 'rgb(253, 179, 76)', pointLineColor: '#d6d2d295', opacity1: 1,opacity2: 0.5, opacity3: 0.5})
        this._daily(1)
      }

      statValuePointButton = () => {
        this.setState({statValue: 0, stepsButton: 'normal', pointButton: 'bold', stepsButtonColor: '#d6d2d295', pointButtonColor: 'rgb(253, 179, 76)', stepsLineColor: '#d6d2d295', pointLineColor: 'rgb(253, 179, 76)', opacity1: 1,opacity2: 0.5, opacity3: 0.5})
        this._daily(0)
      }

    constructor(props) {
        super(props);
        
        this.state = {
            xdata:[{'x': 1, 'y': 1}],
            ydata:[],
            xyData: [{'x': 1, 'y': 1}],
            statData:[{}],
            stepsWalked: 10,
            pointsEarned: 10,
            position: "flex-end",
            opacity1: 1,
            opacity2: .5,
            opacity3: .5,
            stepsButton: 'bold',
            stepsButtonColor:  'rgb(253, 179, 76)',
            statValue: 1,
            pointButtonColor: '#d6d2d295',
            pointButton: 'normal',
            marginPosition: '0%',
            dailySteps: [0,0,0,0,0,0,0],
            dailyPoints: [0,0,0,0,0,0,0],
            dailyStepsY: [0,0,0,0,0,0,0,10],
            dailyPointsY: [0,0,0,0,0,0,0,10],
            barData: [0,0,0,0,0,0,0],
            stepsWalkedText: "steps walked",
            pointsEarnedText: "points earned",
            paddingSteps:'18%',
            paddingPoints: '18%',
            graphSize : this._normalize(310),
            yAxisRange : [0,100],
            present: 'Today',
            yLabel: 'Steps',
            stepsLineColor: 'rgb(253, 179, 76)',
            pointLineColor: '#d6d2d295',
            size: 1,
            page: 1,
            arrayData: [{'x': 1, 'y': 1}],
            arrayAxes: [{'x': 1, 'y': 1}],
            next: 1,
            prev: 0,
            control: 0,
            loopCond: 7,
            sampleArray: [[{"x": 12, "y": 1215}, {"x": 11, "y": 1414}, {"x": 10, "y": 8153}, {"x": 9, "y": 8914}, {"x": 8, "y": 567}, {"x": 7, "y": 124}, {"x": 6, "y": 820}], [{"x": 5, "y": 374}, {"x": 4, "y": 1078}, {"x": 3, "y": 5262}, {"x": 2, "y": 3398}, {"x": 1, "y": 10833}, {"x": 31, "y": 225}, {"x": 30, "y": 3781}], [{"x": 29, "y": 5695}, {"x": 28, "y": 35}, {"x": 27, "y": 24063}, {"x": 26, "y": 472}, {"x": 25, "y": 733}, {"x": 24, "y": 3679}, {"x": 23, "y": 6881}], [{"x": 22, "y": 85}, {"x": 21, "y": 381}, {"x": 20, "y": 844}, {"x": 19, "y": 6485}, {"x": 18, "y": 3815}, {"x": 17, "y": 183}, {"x": 16, "y": 1827}], [{"x": 15, "y": 471}, {"x": 14, "y": 4884}, {"x": 12, "y": 4573}, {"x": 11, "y": 161}, {"x": 10, "y": 3636}, {"x": 9, "y": 221}, {"x": 8, "y": 347}], [{"x": 7, "y": 8292}, {"x": 6, "y": 652}, {"x": 5, "y": 10539}, {"x": 4, "y": 837}, {"x": 3, "y": 3316}, {"x": 2, "y": 492}, {"x": 1, "y": 1504}], [{"x": 30, "y": 4679}, {"x": 29, "y": 1299}, {"x": 28, "y": 9756}, {"x": 27, "y": 1515}, {"x": 26, "y": 2912}, {"x": 25, "y": 4594}, {"x": 24, "y": 411}]]
        };
        console.log("Global user id: ", uid)
        this.dimensions = {window, screen};
        this.offsetby = 10;
        this.hsize = this.dimensions.window.width - this.offsetby;
        this.boxWidth = this.hsize - 60;
        this.boxHeight = 50;
        this.buttonWidth = this.boxWidth - wp('50%')
        var p_total = 0;
        var moveMargin = 0
        try {
          db.transaction(tx => {
            tx.executeSql('select sum(dailysteps) as dailysteps, sum(dailypoints) as dailypoints, week, year from userstatdetails group by week order by week desc', [], (tx, results) => {
            moveMargin = (results.rows.item(0)['dailysteps']/70000)*20
            if (moveMargin < 20){
              this.setState({marginPosition: moveMargin.toString()+"%"})
            }
            else{
              this.setState({marginPosition: "20%"})
            }
            });
    
        });
      }
      catch(err) {
        console.log(`----> (ERROR) <----`, err);
      }  
      
    }


    render() {
        const fill = 'rgb(254, 170, 58)'
        const data = this.state.xdata;
        const xvalue=[10,20,30,40,50,60,70]
            const axesSvg = { fontSize: 10, fill: 'white' };
            const verticalContentInset = { top: 10, bottom: 10 }
            const xAxisHeight = 30
            const tempValues = ['Mon', 'b', 'c', 'd', 'e', 'f', 'g']

            /* const ydata = [0, 200, 400, 600, 800, 1000, 1200, 1400, 1600] */
            console.log("xdata: ", this.state.xdata)
            const graphData = this.state.xdata === undefined ? [{'x': 1, 'y': 1}] : this.state.xdata
            console.log("graphdata: ", graphData)
/***************************************************************************************************************** */
        return (
            <ImageBackground
            source={back_image}
            style={{width: '100%', height: '100%'}}>
            <NavigationEvents
            onDidFocus={() => {this.componentDidMount()}}
            />
            <ScrollView>
                    <View style={{flexDirection: 'row', alignItems: 'center', paddingTop: '5%'}}>
                        <TouchableOpacity style={{paddingLeft: '20%', width: this._normalize(150), alignItems: 'center'}} onPress={() => {this.statValueStepButton()}}>
                            <Text allowFontScaling={false} style={{color: this.state.stepsButtonColor, fontWeight: this.state.stepsButton, fontSize: this._normalize(15)}}>        Steps        </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ width: this._normalize(150), alignItems: 'center'}} onPress={() => {this.statValuePointButton()}}>
                            <Text allowFontScaling={false} style={{color: this.state.pointButtonColor, fontWeight: this.state.pointButton, fontSize: this._normalize(15)}}>        Points        </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{flexDirection: 'row', paddingTop: '2%'}}>
                    <View style={{flex: 1, height: 4, backgroundColor: this.state.stepsLineColor}} />
                    <View style={{flex: 1, height: 4, backgroundColor: this.state.pointLineColor}} />
                    </View>
                    <View style={{ height: 10, width : '90%',  flexDirection: 'row', marginTop: '5%', marginLeft: wp('3%')}}>
                        <TouchableOpacity activeOpacity = { 1 }  onPress={() => {this._daily(),this.changeButtonColor1()}}>
                                <Image source={para} style={{ resizeMode: "contain",alignItems: "center", justifyContent: "center" ,width: this.buttonWidth, height: wp('6%'), opacity: this.state.opacity1}}/>
                                <Text allowFontScaling={false} style={{fontSize: this._normalize(13), position: "absolute",color: "white", marginLeft: '35%'}}>Daily</Text>
                        </TouchableOpacity>
                        <TouchableOpacity activeOpacity = { 1 } onPress={() => {this._1week(),this.changeButtonColor2()}} style={{left: -wp('3%')}} > 
                                <Image source={para} style={{ resizeMode: "contain",alignItems: "center",justifyContent: "center" ,width: this.buttonWidth, height: wp('6%'), opacity: this.state.opacity2}}/>
                                <Text allowFontScaling={false} style={{fontSize: this._normalize(13),  position: "absolute",color: "white", marginLeft: '35%'}}>Weekly</Text>
                        </TouchableOpacity>
                        <TouchableOpacity activeOpacity = { 1 } onPress={() => {this._1month(),this.changeButtonColor3()}} style={{left: -wp('6%')}}>  
                                <Image source={para} style={{ resizeMode: "contain",alignItems: "center",justifyContent: "center" ,width: this.buttonWidth, height: wp('6%'), opacity: this.state.opacity3}}/>
                                <Text allowFontScaling={false} style={{fontSize: this._normalize(13),  position: "absolute",color: "white", marginLeft: '30%'}}>Monthly</Text>   
                        </TouchableOpacity>
                    </View>
                    <View style={{height: '5%'}}/>
                    <View style={{ width: '100%', height: '38%'}} >
                    
                        <SlideBarChart
                        scrollable
                        style={{ marginTop: 64 }}
                        shouldCancelWhenOutside={false}
                        alwaysShowIndicator={false}
                        data= {graphData}
                        barSelectedColor= 'rgb(30,144,255)'
                        fillColor= 'rgb(254, 170, 58)'
                        style={{backgroundColor: "transparent"}}
                        axisWidth={32}
                        axisHeight={16}
                        width={this.state.graphSize}
                        height={250}
                        barWidth={10}
                        barSpacing={15}
                        paddingTop={50}
                        yRange={this.state.yAxisRange}
                        yAxisProps={{
                        numberOfTicks: 4,
                        markAverageLine: 'true',
                        showAverageLine: 'true',
                        axisAverageMarkerStyle: {fontSize: this._normalize(11), color: 'red'},
                        averageLineColor: 'red',
                        horizontalLineColor: 'rgb(105,105,105)',
                        verticalLineColor: 'rgb(105,105,105)',
                        axisLabel: this.state.yLabel,
                        axisLabelAlignment: 'aboveTicks',
                        axisLabelStyle: {fontSize: this._normalize(11), color: 'white'},
                        axisMarkerStyle: {fontSize: this._normalize(11), color: 'white'}
                        }}
                        xAxisProps={{
                        axisMarkerLabels: this.state.xyData.map(item => item.x.toString()),

                        specialStartMarker: this.state.present,
                        adjustForSpecialMarkers: true,

                        minimumSpacing: 2,
                        }}
                        toolTipProps={{
                        backgroundColor: '#d6d2d295',
                        toolTipTextRenderers: [
                            ({ selectedBarNumber }) => ({
                            text: graphData[selectedBarNumber].y.toString(),
                            }), 
                            () => ({ text: 'Value' }),
                        ],
                        }}
                    />
                </View>
                <View style={{flexDirection: 'row', paddingLeft: '10%', paddingRight: '5%', justifyContent: 'space-between'}}>
                    <TouchableOpacity onPress={() => this._nextPrev(0)} style={{width: this._normalize(35), height: this._normalize(18), backgroundColor: 'grey', alignItems: 'center', alignSelf: 'flex-start'}}>
                        <Text style={{fontSize: 14, color: 'white'}}>Prev</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this._nextPrev(1)} style={{width: this._normalize(35), height: this._normalize(18), backgroundColor: 'grey', alignItems: 'center', alignSelf: 'flex-end'}}>
                        <Text style={{fontSize: 14, color: 'white'}}>Next</Text>
                    </TouchableOpacity>
                </View>
                <View style= {{flexDirection:'column',flex:1, justifyContent: 'center', alignItems: 'center'}}>
                        <View style={ {flexDirection: 'row', width: this.boxWidth, height: this.boxHeight, backgroundColor: '#d6d2d295', marginBottom: '3%', marginTop: '4%',borderRadius:10, borderWidth: 1, borderColor: '#d6d2d295'}} >
                                <Text allowFontScaling={false} style={{textAlign: 'left', marginLeft: '4%', fontWeight: "bold", fontFamily: 'sans-serif', fontSize: this._normalize(25), width: 110}}>{this.state.stepsWalked}</Text>  
                                <Text allowFontScaling={false} style={{textAlign: 'right', marginTop: '3%', fontFamily: 'sans-serif', fontSize: this._normalize(13)}}>{this.state.stepsWalkedText}</Text>           
                        </View>
                        <View style={{flexDirection: 'row', width: this.boxWidth, height: this.boxHeight, backgroundColor: '#d6d2d295', borderRadius:10, borderWidth: 1, borderColor: '#d6d2d295'}} >
                            <Text allowFontScaling={false} style={{textAlign: 'left', marginLeft: '4%', fontWeight: "bold", fontFamily: 'sans-serif', fontSize: this._normalize(25), width: 110}}>{this.state.pointsEarned}</Text>  
                            <Text allowFontScaling={false} style={{textAlign: 'left', paddingTop: '3%', fontFamily: 'sans-serif', fontSize: this._normalize(13)}}>{this.state.pointsEarnedText}</Text> 
                            {this.cityLocation()}
                        </View>
                            {/* <View style={[styles.container, { justifyContent: 'this.state.position', marginLeft: 30,flexDirection: 'row'},{width: screenWidth, height: 38, marginBottom: '3%'}]}> */}
                            <View style={[styles.container, { justifyContent: 'flex-end', marginLeft: this.state.marginPosition,flexDirection: 'row'},{width: screenWidth, height: 200, marginBottom: '3%'}]}>
                                <Image source={city} style = {{ width: 500, height: 300}} />     
                            </View>
                </View>
                </ScrollView>
        </ImageBackground>
        )


    }
 
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        paddingTop: 64,
        paddingBottom: 32
      },
    button: {
      backgroundColor: "#DDDDDD"
    },
    line: {
      borderBottomWidth: 1,
      borderColor: '#f48a92'
    },
    parallelogram: {
      width: 150,
      height: 100
    },
    responsiveBox: {
      width: wp('77%'),
      height: hp('6%'),
      flexDirection: 'column',
      justifyContent: 'space-around',
      backgroundColor: '#d6d2d295', 
      borderRadius:10, 
      borderWidth: 1, 
      borderColor: '#d6d2d295' 
    }   
  });

  