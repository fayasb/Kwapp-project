import React, { useEffect, useState } from 'react'
import { BarChart, Grid } from 'react-native-svg-charts'
import { ImageBackground, Image } from 'react-native';
import { StyleSheet, View, Button, SafeAreaView, Text, Alert,UIManager,LayoutAnimation,TouchableOpacity,PixelRatio,  ScrollView} from 'react-native';
import ImagePicker from 'react-native-image-picker';

export default class profilePage extends React.PureComponent {

    state = {
        avatarSource: null
      };
      constructor(props) {
        super(props);
    
        this.selectPhotoTapped = this.selectPhotoTapped.bind(this);
      }

      selectPhotoTapped() {
        const options = {
          quality: 1.0,
          maxWidth: 500,
          maxHeight: 500,
          storageOptions: {
            skipBackup: true,
          },
        };


      ImagePicker.showImagePicker(options, response => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        let source = {uri: response.uri};

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          avatarSource: source,
        });
      }
    });
  }


    render() {

/***************************************************************************************************************** */
        return (
            <ImageBackground
            source={require('./background.png')}
            style={{width: '100%', height: '100%'}}>

                <View style={styles.container}>
                    <TouchableOpacity onPress={this.selectPhotoTapped.bind(this)}>
                    <View
                        style={[styles.avatar, styles.avatarContainer, {marginBottom: 20}]}>
                        {this.state.avatarSource === null ? (
                        <Text>Select a Photo</Text>
                        ) : (
                        <Image style={styles.avatar} source={this.state.avatarSource} />
                        )}
                    </View>
                    </TouchableOpacity>
                </View>
        </ImageBackground>
        )


    }
 
}
const styles = StyleSheet.create ({
    container: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        paddingTop: '10%'
        
      },
      avatarContainer: {
        borderColor: '#9B9B9B',
        backgroundColor: '#F5FCFF',
        borderWidth: 1 / PixelRatio.get(),
        justifyContent: 'center',
        alignItems: 'center'
      },
      avatar: {
        borderRadius: 75,
        width: 150,
        height: 150,
      }
 })


