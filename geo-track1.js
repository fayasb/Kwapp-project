import React, { Component } from 'react';
import { Text, View, ActivityIndicator, TouchableOpacity, Alert, StyleSheet, Animated } from 'react-native'
import { Foundation } from '@expo/vector-icons'
import { white, purple } from './utils/colors'
/* import { Location, Permissions } from 'expo' */
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';
import {calculateDirection} from './utils/helpers'
import Geolocation from '@react-native-community/geolocation';


/*   componentDidMount() {
    Permissions.getAsync(Permissions.LOCATION)
      .then(({ status }) => {
        if (status === 'granted') {
          return this.setLocation()
        }

        this.setState({ status });
      })
      .catch(error => {
        console.warn('error getting permission: ', error);

        this.setState({ status: 'undetermined' });
      })
  }   */
  
  /*-----------------------------------------------------------*/
  export default class App extends Component {
    state = {
      coords: null,
      status: null,
      direction: '',
      ready: false, 
      location: null, 
      error: null,
      bounceValue: new Animated.Value(1),
    };
    
  
    findCoordinates = () => {
      console.log("location log");
      Geolocation.getCurrentPosition(
        position => {
          console.log(position.coords.latitude);  
          const location = JSON.stringify(position);
          const coords = JSON.stringify(position.coords);
          const newDirection = calculateDirection(coords.heading)
          
          if (newDirection !== direction ) {
            Animated.sequence([
              Animated.timing(bounceValue, { duration: 200, toValue: 1.04 }),
              Animated.spring(bounceValue, { toValue: 1, friction: 4 })
            ]).start()
          }
          this.setState({ location, coords, status: 'granted', direction: newDirection, bounceValue });
          console.log(JSON.stringify(position.coords));
          console.log(newDirection); 
        },
        error => Alert.alert(error.message),
        { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
      );
      
    };

  geoSuccess = (position) => {  
    console.log(position.coords.latitude);  

    this.setState({  
        ready:true,  
        where: {lat: position.coords.latitude,lng:position.coords.longitude }  
    })  
}  
geoFailure = (err) => {  
    this.setState({error: err.message});  
}  


 /*  setLocation = () => {
    Location.watchPositionAsync({
      enableHighAccuracy: true,
      timeInterval: 1,
      distanceInterval: 1
    }, ({coords}) => {
      const newDirection = calculateDirection(coords.heading)
      console.log(newDirection); 
      const { direction, bounceValue } = this.state

      this.setState({
        coords,
        status: 'granted',
        direction: newDirection
      })
    })
  } */
  render() {
    const { status, coords, direction, bounceValue } = this.state
    console.log("render start");
    console.log("status: "+ status);
    if (status === null) {
      return <ActivityIndicator style={{ margin: 30 }} />
    } else if (status === 'denied') {
      return (
        <View style={styles.center}>
          <Foundation name='alert' size={50} />
          <Text style={{ textAlign: 'center'}}>
            You denied your location. You can fix this by visiting your settings and enabling location services fro this app
          </Text>
        </View>
      )
    } else if (status === 'undetermined') {
      return (
        <View style={styles.center}>
          <Foundation name='alert' size={50} />
          <Text> Oops.. You need to enable location services</Text>
          <TouchableOpacity onPress={this.askPermission} style={styles.button}>
            <Text style={styles.buttonText}> Enable</Text>
          </TouchableOpacity>
        </View>
      )
    }
    console.log("starting return");
    return (
      <View style={styles.container}>
        <View style={styles.directionContainer}>
          <Text style={styles.header}> You're heading</Text>
            { 
              direction === 'Calculating'
              ?  <ActivityIndicator style={{ margin: 30 }} />  
              : <Animated.Text style={[styles.direction, { transform: [{ scale: bounceValue }]}]}>
                  { direction }
                </Animated.Text>
            }
        </View>
        <View style={styles.metricContainer}>
          <View style={styles.metric}>
            <Text style={[styles.header, { color: white }]}>
              Altitude
            </Text>
            <Text style={[styles.subHeader, { color: white }]}>
              { Math.round(coords.altitude * 3.2808) } Feet
            </Text>
          </View>
          <View style={styles.metric}>
            <Text style={[styles.header, { color: white }]}>
              Speed
            </Text>
            <Text style={[styles.subHeader, { color: white }]}>
              { (coords.speed * 2.2369).toFixed(1) } MPH
            </Text>
          </View>
        </View>
      </View>
        
    )
  }
}

// Styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between'
  },
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 30,
    marginRight: 30,
  },
  button: {
    padding: 10,
    backgroundColor: purple,
    alignSelf: 'center',
    borderRadius: 5,
    margin: 20,
  },
  buttonText: {
    color: white,
    fontSize: 20,
  },
  direction: {
    color: purple,
    fontSize: 120,
    textAlign: 'center',
  },
  directionContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  header: {
    fontSize: 35,
    textAlign: 'center',
  },
  metricContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    backgroundColor: purple,
  },
  metric: {
    flex: 1,
    paddingTop: 15,
    paddingBottom: 15,
    backgroundColor: 'rgba(255, 255, 255, 0.1)',
    marginTop: 20,
    marginBottom: 20,
    marginLeft: 10,
    marginRight: 10,
  },
  subHeader: {
    fontSize: 25,
    textAlign: 'center',
    marginTop: 5,
  },
})
