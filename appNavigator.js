import { createStackNavigator } from 'react-navigation';
  import Home from './App';

  const AppNavigator = createStackNavigator({
    Home: { screen: Home },
  });