/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format 
 * 
 */

import React, { Component } from 'react';
import {
  Text, View, StyleSheet, ImageBackground, Dimensions,
  AppState,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import { accelerometer, gyroscope, setUpdateIntervalForType, SensorTypes } from "react-native-sensors";

//
//
// Import custom components
import CircularProgress from './src/components/circularProgressBar/CircularProgress';
import HorizontalProgress from './src/components/horizontalProgressBar/HorizontalProgress';
import CascadingCircularProgress from './src/components/circularProgressBar/CascadingCircularProgress';
import TextWithImgae from './src/components/textWithImage/textWithImage';
import Dates from './src/helpers/Dates';

//
//
// Gather app window and screen dimensions
const window = Dimensions.get("window");
const screen = Dimensions.get("screen");

//
//
// Image handler for the app background
const bgimage = require('./src/backgrounds/app_background.png');
const classTxtBgImage = require("./src/backgrounds/class_background.png");
const mascotImg = require("./src/backgrounds/mascot.png");

//
//
// General log lead strings
const logLead = "[*]";

export default class App extends Component {
  // Refresh dimensions
  // const [dimensions, setDimensions] = useState({ window, screen });

  // const onChange = ({ window, screen }) => {
  //   setDimensions({ window, screen });
  // }

  constructor(props) {
    super(props);

    this.dimensions = { window, screen };

    // Points and leveling progress
    // State
    this.state = {

      userClass: "Beginner",
      profileLevelPcnt: "99",
      stepsWalked: 0,
      stepsGoal: "10000",
      pointProg: 0,
      today: '01012020',
      appState: AppState.currentState,

    }

    // Handler variables
    this.offsetby = 96;
    this.hsize = this.dimensions.window.width - this.offsetby;
    this.vsize = (this.dimensions.window.height / 2) - this.offsetby;
    this.strokeWidth = 6;
    this.radius = ((this.hsize - this.strokeWidth) / 2) - (this.offsetby / 2);
    this.boxWidth = this.hsize - 20;
    this.boxHeight = 28;
    this.progressCx = this.hsize / 2;
    this.progressCy = this.vsize / 2;

    // Variables for profile pic view
    this.profileViewPcnt = 20;
    this.profileWidth = this.profileViewPcnt * this.hsize / 100;
    this.profileCx = this.profileWidth / 2;
    this.profileCy = this.profileWidth / 2;
    this.profileStrokeWidth = 3;
    this.profileRadius = (this.profileWidth - this.profileStrokeWidth) / 2;

    // Fetching old values from db
    db.transaction(tx => {
      tx.executeSql("select userlevel from userdetails", [], (tx, results) => {
        // read userlevel and set userclass
        console.log("userlevel from localfb: ", results.rows.item(0)['userlevel']);
        switch (results.rows.item(0)['userlevel']) {
          case 0: {
            console.log("inside if 1");
            this.setState({ userClass: 'Beginner' });
            break;
          }
          case 1: {
            console.log("inside if 2");
            this.setState({ userClass: 'Medium' });
            break;
          }
          case 2: {
            console.log("inside if 3");
            this.setState({ userClass: 'Master' });
            break;
          }
          default: {
            console.log("Error! UNKNOWN userlevel ID found.")
            break;
          }
        }
      });
    });

    // keys for async storage
    this.stateKey = "@states@";
    this.subscriptionKey = "@subscriptions@";
    this.appRunningKey = "@appRunning@";
    this.stepsKey = "@stepswalked@";
    this.pointsKey = "@pointsearned@";
    this.timeKey = "@timestamp@";

    // subscriptions
    this.asubscription = null;

    // Array storing sensor values
    this.sensorValues = [];
    this.toProcess = [];

    // recorded sensor values in current tick
    this.counter = 0;

    // ticker interval in milli seconds
    this.refreshTimer = 4500;
    // batch size (to be dumped to csv)
    this.batchSize = 15;
    // sensor update interval. in milli seconds
    this.sensorUpdateInterval = 650;
    // processing record threshold in number of records
    this.processingThreshold = 15;

    // for processing the file
    this.stepSum = 0;
    this.prevAcceleration = 0;
    this.accelThreshold = 4.2;
    this.retVal = 0;
    this.result = 0;
    this.oldState = {};

    // for point calculation
    const pointsLimits = {
      beginner: {
        goal1: 12,
        goal2: 25,
        goal3: 50,
      },
      intermediate: {
        goal1: 18,
        goal2: 37,
        goal3: 75,
      },
      master: {
        goal1: 25,
        goal2: 50,
        goal3: 100,
      },
    };
    this.currTotalSteps = 0;

    // set goal points for current user class
    this.pointGoals = pointsLimits.master;

  }





  /////////////////////////////////////////////////////////////////////////
  //
  // start tracking all sensors
  startSensors = () => {

    // set tracker interval
    setUpdateIntervalForType(
      SensorTypes.accelerometer,
      this.sensorUpdateInterval
    );

    // start tracker
    const asubscription = accelerometer
      .pipe()
      .subscribe(
        ({ x, y, z, timestamp }) => {
          // pushing into sensor array
          this.sensorValues.push({ s: "a", x: x, y: y, z: z, time: timestamp });

          this.counter = this.counter + 1;

          if (this.counter >= this.batchSize) {
            // batch limit reached

            this.writeToProcess();
          }
        },
        error => {
          console.log("The sensor is not available!");
        }
      );

    // adding to state's subscription
    this.asubscription = asubscription;

    // let multiSetKeys = [[this.subscriptionKey, "true"], [this.timeKey, Date.now()]];

    AsyncStorage.setItem(this.subscriptionKey, "true", (err) => {
      if (err === null) {
        // Subscription and Timestamp keys set.
      } else {
        console.log(error);
      }
    });

    console.log("Sensor trackers started!");
  }





  /////////////////////////////////////////////////////////////////////////
  //
  // Dump array to processing array
  writeToProcess = async () => {

    const rowString = this.sensorValues.map(
      (d) => {

        this.toProcess.push({ x: d.x, y: d.y, z: d.z });

        return `${d.s},${d.x},${d.y},${d.z},${d.time}\n\r`;
      }).join('');

    this.sensorValues = [];
    this.counter = 0;

    await this.processFile()
      .then(() => {

        this.toProcess = [];
        this.stepSum = 0;
      })
      .catch(error => console.log(error));

  }





  /////////////////////////////////////////////////////////////////////////
  //
  // Processing file and recording steps
  processFile = async () => {

    stepArray = this.toProcess.map((data) => { // changing from toProcess to 
      this.retVal = 0;

      // follows sum squared acceleration logic : sqrt(x^2 + y^2 + z^2)
      this.result = Math.sqrt(Math.pow(data.x, 2) + Math.pow(data.y, 2) + Math.pow(data.z, 2));

      if ((this.prevAcceleration != 0) && ((this.result - this.prevAcceleration) > this.accelThreshold)) {
        this.retVal = 1;
      } else {
        this.retVal = 0;
      }

      this.prevAcceleration = this.result;

      return this.retVal;
    })

    this.stepSum = stepArray.reduce((total, curr) => total + curr, 0);

    // stepSum threw an error when it was declared locally. So pushing stepSum to the class.
    // sending steps into AsyncStorage instead of the state.
    // this.setState((prevState) => {
    //   return { steps: prevState.steps + this.stepSum };
    // });

    // reading previous stored state
    await AsyncStorage.getItem(this.stateKey, (error, results) => {
      if (results !== null) {
        let storedState = JSON.parse(results);
        this.oldState = storedState;
      } else {
        this.oldState = this.state;
      }

      // setting updated state
      AsyncStorage.setItem(this.stateKey, JSON.stringify(this.oldState, this._replacer),
        (error) => {
          if (error === null) {
            // Updated states in AsyncStorage.
          } else {
            console.log(error);
          }
        }
      ).catch((error) => { console.log(error) });

      // setting updated timestamp
      AsyncStorage.setItem(this.timeKey, Date.now().toString(),
        (error) => {
          if (error === null) {
            // Updated timestamp in AsyncStorage.
          } else {
            console.log(error);
          }
        }
      ).catch((error) => { console.log(error) });
    }).catch((error) => { console.log(error) });

  }

  /////////////////////////////////////////////////////////////////////////
  //
  // replacer function to update stepswalked 
  //        * sets the updated steps and points earned *
  _replacer = (key, value) => {

    // updating values for steps walked and points earned
    if (key === "stepsWalked") {
      // updating step count
      this.currTotalSteps = value + this.stepSum;
      return this.currTotalSteps;
    } else if (key === "pointProg") {
      // calculating new points
      if (this.currTotalSteps >= (this.state.stepsGoal)) {
        return this.pointGoals.goal3;
      } else if (this.currTotalSteps >= (0.50 * this.state.stepsGoal)) {
        return this.pointGoals.goal2;
      } else if (this.currTotalSteps >= (0.25 * this.state.stepsGoal)) {
        return this.pointGoals.goal1;
      } else {
        return value;
      }
    } else {
      return value;
    }
  }





  /////////////////////////////////////////////////////////////////////////
  //
  // component mounted and start timer 
  componentDidMount() {

    // Check if already a refreshed state exists
    // this.refreshState();

    // start a timer
    this.timerID = setInterval(() => {
      this.executeEveryTick();
    }, this.refreshTimer);

    // checking for existing Asyncstorage keys and starting all necessary sensor processing
    this._checkAsyncStorage();
  }

  /////////////////////////////////////////////////////////////////////////
  //
  // component will be unmounted; stop the timer and force stop all sensors
  componentWillUnmount() {

    // clearing timer
    clearInterval(this.timerID);

    // set subscribed flag to persistant storage
    this._setSubscriptionkey();
  }






  /////////////////////////////////////////////////////////////////////////
  //
  // check asyncstorage for app run state and perform resume or restart operations
  _checkAsyncStorage = async () => {
    try {
      const key = await AsyncStorage.getItem(this.appRunningKey, (error, result) => {

        if (result === null) {

          let keys = [this.stateKey, this.subscriptionKey, this.stepsKey, this.pointsKey];

          AsyncStorage.multiRemove(keys, (error) => {

            AsyncStorage.setItem(this.appRunningKey, "true", (error) => {
              if (error === null) {

                this._checkSubscriptionKey();
              } else {
                console.log(error);
              }
            })
          })
        } else if (result === "true") {
          let keys = [this.subscriptionKey, this.stepsKey, this.pointsKey];

          AsyncStorage.multiRemove(keys, (error) => {

            AsyncStorage.setItem(this.appRunningKey, "true", (error) => {
              if (error === null) {

                this._checkSubscriptionKey();

              } else {
                console.log(error);
              }
            })
          })
        } else if (result === "false") {

          AsyncStorage.getItem(this.timeKey, (error, results) => {
            if (results !== null) {

              if ((Date.now() - results) > 30000) {
                // Restarting all sensors...

                let keys = [this.subscriptionKey, this.stepsKey, this.pointsKey];

                AsyncStorage.multiRemove(keys, (error) => {

                  AsyncStorage.setItem(this.appRunningKey, "true", (error) => {
                    if (error === null) {

                      this._checkSubscriptionKey();

                    } else {
                      console.log(error);
                    }
                  })
                })

              } else {
                console.log("Pull previous steps from storage and process existing unprocessed sensor reads.");
              }
            } else {
              console.log(error);
            }
          })
        }
      })
    } catch (error) {
      console.log(error);
    }

    // Check if day has changed
    await this.checkChangeOfDay ();

    // refresh state from stored state if it exists
    await this.refreshState ();
  }






  /////////////////////////////////////////////////////////////////////////
  //
  // checking for subscription start flag in persistant storage
  _checkSubscriptionKey = async () => {

    try {
      const keys = await AsyncStorage.getItem(this.subscriptionKey, (error, results) => {

        if (results === null) {
          AppState.addEventListener("change", this._handleAppStateChange);

          // starting sensors
          this.startSensors();

        }
      });

    } catch (error) {
      console.log(error);
    }

  }






  /////////////////////////////////////////////////////////////////////////
  //
  // handling app state changes
  _handleAppStateChange = nextAppState => {

    this.setState({ appState: nextAppState });
  }






  /////////////////////////////////////////////////////////////////////////
  //
  // setting subscription start flag in persistant storage at the end of components lifecycle
  _setSubscriptionkey = async () => {

    try {
      const keys = await AsyncStorage.getItem(this.subscriptionKey, (error, results) => {

        AsyncStorage.setItem(this.appRunningKey, "false");
      })
    } catch (error) {
      console.log(error);
    }

  }





  /////////////////////////////////////////////////////////////////////////
  //
  // methods to run every timer tick
  executeEveryTick = async () => {

    // check for a day change
    await this.checkChangeOfDay ();

    // refreshing state with stored state data
    await this.refreshState ();

  }





  /////////////////////////////////////////////////////////////////////////
  //
  // refresh current state with data from asyncstorage if it exists
  refreshState = async () => {

    // refreshing current state from asyncstorage
    await AsyncStorage.getItem(this.stateKey, (error, results) => {
      if (results !== null) {
        // refresh state with stored state
        let newState = JSON.parse(results);
        if ((this.state.stepsWalked === newState.stepsWalked) && (this.state.pointProg === newState.pointProg)) { }
        else {
          this.setState(newState);
          
          // Update state into db
          db.transaction(tx => {
            tx.executeSql("update userstatdetails set dailysteps=?, dailypoints=? where timeid=?", [newState.stepsWalked, newState.pointProg, newState.today], (tx, results) => {
              // update sql completed
              console.log("Date changed! Update row. resultsaffected: ", results.rowsAffected);
            });
          });
        }
      } else {
        console.log(`Error! ${error}... No updated state found.`);
      }
    }).catch((error) => { console.log(error) });

  }





  /////////////////////////////////////////////////////////////////////////
  //
  // check change of day and make state updates
  checkChangeOfDay = async () => {

    // Get today's date
    let today = Dates._ddmmyyyy ();

    await AsyncStorage.getItem (this.stateKey, (error, results) => {
      if (results !== null) {
        // There exists a stored state already

        let storedState = JSON.parse (results);
        // Checking if dates match
        if (storedState.today != today) {
          // A date change has been detected

          // Process for date change
          // Update the latest state into db
          //
          //
          db.transaction(tx => {
            tx.executeSql("update userstatdetails set dailysteps=?, dailypoints=? where timeid=?", [storedState.stepsWalked, storedState.pointProg, storedState.today], (tx, results) => {
              // update sql completed
              console.log("Date changed! Update row. resultsaffected: ", results.rowsAffected);
            });
          });

          // console.log(`Update qry: update userstatdetails set 
          //   dailysteps=${storedState.stepsWalked},
          //   dailypoints=${storedState.pointProg}
          //   where timeid=${storedState.today}`);

          // Insert a fresh state entry into db
          //
          //
          db.transaction(tx => {
            tx.executeSql("insert into userstatdetails (dailysteps, dailypoints, timeid, day, week, month) values (0, 0, ?, ?, ?, ?)", [today, Dates._getDay(), Dates._getWeek(), Dates._getMonth()], (tx, results) => {
              // insert sql completed
              console.log("Date changed! Insert row. resultsaffected: ", results.rowsAffected);
            });
          });

          // console.log(`Insert qry: insert into userstatdetails (dailysteps, dailypoints, timeid, day, week, month)
          //   values (0, 0, ${today}, ${Dates._getDay()}, ${Dates._getWeek()}, ${Dates._getMonth()})`);

          // Reseting current state with today's date
          this.resetStateWithDate (today);

          // Refresh stored state with reset state
          //
          // Removing the stored state from AsyncStorage
          // This forces the processFile () to use the current state and write it fresh into AsyncStorage
          AsyncStorage.removeItem (this.stateKey, (error) => {
            if (error === null) {
              // removed stateKey successfully
            }
          }).catch((error) => { console.log(error) });
        } else {
          // Dates match
          // Nothing to do here
          // console.log ("Dates match. Nothing to do here.");
        }
      } else {
        // No stored state found
        // Nothing to do here
        // console.log ("No stored state found. Nothing to do here,");
      }
    }).catch((error) => { console.log(error) });

  }





  /////////////////////////////////////////////////////////////////////////
  //
  // Reset the state with new day
  resetStateWithDate = (day) => {
    // Only the stepsWalked, pointProg and today has to be reset
    let newState = {

      stepsWalked: 0,
      pointProg: 0,
      today: day,

    };
    // Resetting
    this.setState (newState);
  }





  /////////////////////////////////////////////////////////////////////////
  //
  // rendering element stepCounter
  render() {
    return (
      <>
        <View style={styles.containerStyle}>
          <ImageBackground source={bgimage} style={styles.imageStyle}>
            <View style={styles.headerContainerStyle}>
              <View style={styles.classContainerStyle}>
                <TextWithImgae source={classTxtBgImage}>
                  {this.state.userClass} challenger
                </TextWithImgae>
              </View>
              <View style={styles.profileContainerStyle}>
                <CircularProgress
                  boxWidth={this.profileWidth}
                  boxHeight={this.profileWidth}
                  bgColor="white"
                  width={this.profileStrokeWidth}
                  r={this.profileRadius}
                  cx={this.profileCx}
                  cy={this.profileCy}
                  progressColor="white"
                  dashArray="1, 0"
                  progressPcnt={this.state.profileLevelPcnt}
                />
              </View>
            </View>
            <View style={styles.statsContainerStyle}>
              <View style={styles.pointProgressContainer}>
                <CascadingCircularProgress
                  boxWidth={this.hsize}
                  boxHeight={this.vsize}
                  bgColor="white"
                  width={this.strokeWidth}
                  r={this.radius}
                  cx={this.progressCx}
                  cy={this.progressCy}
                  progressColor="rgb(255,152,75)"
                  dashArray="1, 0"
                  steps={this.state.stepsWalked}
                  stepsGoal={this.state.stepsGoal}
                >
                  {this.state.stepsWalked}
                </CascadingCircularProgress>
                <HorizontalProgress
                  boxWidth={this.boxWidth}
                  boxHeight={this.boxHeight}
                  edgeCurve="3"
                  bgColor="#ffffff"
                  progressColor="rgb(255,152,75)"
                  progressPcnt={this.state.pointProg}
                />
                <Text style={styles.textStyle}>{this.state.pointProg}/100 points earned today</Text>
              </View>
            </View>
            <View style={styles.footerContainerStyle}>
              <View style={styles.notificationContainerStyle}>
                <Text style={styles.notificationTextStyle}>
                  You have met your goal four days in a row! Just one more until the next achievement!
                </Text>
              </View>
              <View style={styles.mascottContainerStyles}>
                <Text style={styles.placeholderStyle}>The Mascott</Text>
              </View>
            </View>
          </ImageBackground>
        </View>
      </>
    );
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between",
  },
  imageStyle: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
  },
  textStyle: {
    color: "white",
    fontSize: 15,
    fontWeight: "bold",
  },
  headerTextStyle: {
    color: "white",
    fontSize: 15,
    fontWeight: "bold",
    fontFamily: "sans-serif-light",
  },
  headerContainerStyle: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "blue",
    flexDirection: "row",
    paddingLeft: 15,
    width: "100%",
    height: "20%",
    justifyContent: "space-around",
    alignItems: "center",
  },
  statsContainerStyle: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "grey",
    flexDirection: "column",
    padding: 20,
    width: "100%",
    height: "50%",
    justifyContent: "center",
    alignItems: "center",
  },
  footerContainerStyle: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "grey",
    flexDirection: "row",
    padding: 20,
    width: "100%",
    height: "30%",
    justifyContent: "center",
    alignItems: "center",
  },
  classContainerStyle: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "red",
    padding: 20,
    width: "70%",
    justifyContent: "center",
    alignItems: "center",
  },
  profileContainerStyle: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "red",
    // padding: 10,
    width: "30%",
    justifyContent: "center",
    alignItems: "center",
  },
  roundProgressContainer: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "grey",
    height: "70%",
    justifyContent: "center",
    alignItems: "center",
  },
  pointProgressContainer: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "grey",
    height: "30%",
    justifyContent: "center",
    alignItems: "center",
  },
  notificationContainerStyle: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "grey",
    width: "75%",
    justifyContent: "center",
    alignItems: "center",
  },
  notificationTextStyle: {
    color: "white",
    fontSize: 12,
    fontWeight: "bold",
  },
  mascottContainerStyles: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "grey",
    width: "25%",
    justifyContent: "center",
    alignItems: "center",
  },
  placeholderStyle: {
    color: "#282c34",
    fontSize: 12,
    fontWeight: "bold",
  },
});

{/* <Text style={styles.textStyle}>This is the header view</Text> 
<Text style={styles.textStyle}>Profile picture</Text>
<Text style={styles.textStyle}>This is the current stats</Text>


  placeholderStyle: {
    color: "#282c34",
    fontSize: 12,
    fontWeight: "bold",
  },
*/ }