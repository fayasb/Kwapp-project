import React from 'react';
import Svg, { Circle, Path, Rect, Text } from "react-native-svg";

// Draw parameters
const bgLayerOpacity = "0.2";
const progressLayerOpacity = "1";
const firstCascade = "1.25";
const secondCascade = "2.5";

// Progress calculation parameters
const firstProgress = "0.25";
const secondProgress = "0.5";
const thirdProgress = "1";
let metGoal = 0;

const secondaryColor = "#C56490";


function generateArc(circle, stepsWalked, totalSteps, cx, cy, radius) {

    if (circle === 1) {
        // set the limit
        tgt1 = firstProgress * totalSteps;
        
        if (stepsWalked >= tgt1) {
            percentage = 99.999;
            metGoal = metGoal + 1;
        } else {
            percentage = (stepsWalked / tgt1) * 100 ;
        }
    } else if (circle === 2) {
        // setting the limits
        tgt1 = firstProgress * totalSteps;
        tgt2 = secondProgress * totalSteps;

        if (stepsWalked > tgt1 && stepsWalked <= tgt2) {
            percentage = ((stepsWalked - tgt1) / (tgt2 - tgt1)) * 100;
        } else if (stepsWalked >= tgt2) {
            percentage = 99.999;
            metGoal = metGoal + 1;
        } else {
            percentage = 0;
        }
    } else if (circle === 3) {
        // setting the limits
        tgt1 = secondProgress * totalSteps;
        tgt2 = thirdProgress * totalSteps;

        if (stepsWalked > tgt1 && stepsWalked < tgt2) {
            percentage = ((stepsWalked - tgt1) / (tgt2 - tgt1)) * 100;
        } else if (stepsWalked >= tgt2) {
            percentage = 99.999;
            metGoal = metGoal + 1;
        } else {
            percentage = 0;
        }
    }

    // console.log(`cprogress: c:${circle}, steps:${stepsWalked}/${totalSteps}, progress:${percentage}%, cx:${cx}, cy:${cy}, radius:${radius}`);

    // if (percentage === 100) percentage = 99.999;

    const a = percentage * 2 * Math.PI / 100 // angle (in radian) depends on percentage

    const r = radius // radius of the circle
    var rx = r,
        ry = r,
        xAxisRotation = 0,
        largeArcFlag = 1,
        sweepFlag = 1,
        xadjust = r * Math.sin(a),
        yadjust = r * Math.cos(a),
        x = cx + xadjust,
        y = cy - yadjust
    if (percentage <= 50) {
        largeArcFlag = 0;
    } else {
        largeArcFlag = 1
    }

    return `A${rx} ${ry} ${xAxisRotation} ${largeArcFlag} ${sweepFlag} ${x} ${y}`;
}

const cascadingCircularProgress = (props) => (
    <Svg width={props.boxWidth} height={props.boxHeight} viewBox={`0 0 ${props.boxWidth} ${props.boxHeight}`}>
        {/* Outer progress : Full radius*/}
        <Circle
            stroke={props.bgColor}
            strokeWidth={props.width}
            strokeOpacity={bgLayerOpacity}
            r={props.r}
            cx={props.cx}
            cy={props.cy}
            fill="none"
        />
        <Path
            stroke={props.progressColor}
            strokeWidth={props.width}
            strokeDasharray={props.dashArray}
            strokeOpacity={progressLayerOpacity}
            d={`M${props.cx} ${(props.cy) - props.r} 
          ${generateArc(3, props.steps, props.stepsGoal, props.cx, props.cy, props.r)}`}
        />
        {/* First Inner progress : Radius reduced by firstCascade times width */}
        <Circle
            stroke={props.bgColor}
            strokeWidth={props.width}
            strokeOpacity={bgLayerOpacity}
            r={props.r - (firstCascade * props.width)}
            cx={props.cx}
            cy={props.cy}
            fill="none"
        />
        <Path
            stroke={props.progressColor}
            strokeWidth={props.width}
            strokeDasharray={props.dashArray}
            strokeOpacity={progressLayerOpacity}
            d={`M${props.cx} ${(props.cy) - (props.r - (firstCascade * props.width))} 
          ${generateArc(2, props.steps, props.stepsGoal, props.cx, props.cy, props.r - (firstCascade * props.width))}`}
        />
        {/* Second Inner progress : Radius reduced by secondCascade times width */}
        <Circle
            stroke={props.bgColor}
            strokeWidth={props.width}
            strokeOpacity={bgLayerOpacity}
            r={props.r - (secondCascade * props.width)}
            cx={props.cx}
            cy={props.cy}
            fill="none"
        />
        <Path
            stroke={props.progressColor}
            strokeWidth={props.width}
            strokeDasharray={props.dashArray}
            strokeOpacity={progressLayerOpacity}
            d={`M${props.cx} ${(props.cy) - (props.r - (secondCascade * props.width))} 
          ${generateArc(1, props.steps, props.stepsGoal, props.cx, props.cy, props.r - (secondCascade * props.width))}`}
        />
        <Text
            stroke="white"
            fill="white"
            fontSize="15"
            fontFamily="sans-serif"
            x={props.cx - 15}
            y={props.cy - 50}
            textAnchor="start"
        >
            ** / 3
        </Text>
        <Text
            stroke="white"
            fill="white"
            fontSize="12"
            fontFamily="sans-serif"
            x={props.cx}
            y={props.cy - 30}
            textAnchor="middle"
        >
            of daily goal completed
        </Text>
        <Text
            stroke="white"
            fill="white"
            fontSize="30"
            fontFamily="sans-serif"
            x={props.cx}
            y={props.cy + 35}
            textAnchor="middle"
        >
        {props.children}
        </Text>
        <Text
            stroke="white"
            fill="white"
            fontSize="12"
            fontFamily="sans-serif"
            x={props.cx}
            y={props.cy + 60}
            textAnchor="middle"
        >
            steps walked!
        </Text>
        {/* <Rect
            x="1"
            y="1"
            width={props.boxWidth - 2}
            height={props.boxHeight - 2}
            fill="none"
            stroke="red"
            strokeWidth="1"
        /> */}
    </Svg>
);

export default cascadingCircularProgress;