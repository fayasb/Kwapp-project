import React, { Component } from 'react';
import Svg, { Circle, Path, Rect, Text } from "react-native-svg";






/////////////////////////////////////////////////////////////////////////
//
// the CascadingCircularProgress component
export default class CascadingCircularProgress extends Component {

  /////////////////////////////////////////////////////////////////////////
  //
  // constructor function
  constructor(props) {
    super(props);

    // Draw parameters
    this.bgLayerOpacity = "0.2";
    this.progressLayerOpacity = "1";
    this.firstCascade = "1.25";
    this.secondCascade = "2.5";

    // Progress calculation parameters
    this.firstProgress = "0.25";
    this.secondProgress = "0.5";
    this.thirdProgress = "1";
    this.metGoal = 0;

    // For gradient coloring of the circles
    const secondaryColor = "#C56490";

  }

  /////////////////////////////////////////////////////////////////////////
  //
  // component functions : reset the metGoal variable whenever component reloads
  componentDidMount() {

    // Resetting the metGoal variable
    // console.log(`Old metGoal: ${this.metGoal}... Resetting...`);

    this.metGoal = 0;
  }

  componentDidUpdate() {

    // Resetting the metGoal variable
    // console.log(`Old metGoal: ${this.metGoal}... Resetting...`);

    this.metGoal = 0;
  }





  /////////////////////////////////////////////////////////////////////////
  //
  // render function
  generateArc = (circle, stepsWalked, totalSteps, cx, cy, radius) => {

    if (circle === 1) {
      // set the limit
      tgt1 = this.firstProgress * totalSteps;

      if (stepsWalked >= tgt1) {
        percentage = 99.999;
        this.metGoal = this.metGoal + 1;
      } else {
        percentage = (stepsWalked / tgt1) * 100;
      }
    } else if (circle === 2) {
      // setting the limits
      tgt1 = this.firstProgress * totalSteps;
      tgt2 = this.secondProgress * totalSteps;

      if (stepsWalked > tgt1 && stepsWalked <= tgt2) {
        percentage = ((stepsWalked - tgt1) / (tgt2 - tgt1)) * 100;
      } else if (stepsWalked >= tgt2) {
        percentage = 99.999;
        this.metGoal = this.metGoal + 1;
      } else {
        percentage = 0;
      }
    } else if (circle === 3) {
      // setting the limits
      tgt1 = this.secondProgress * totalSteps;
      tgt2 = this.thirdProgress * totalSteps;

      if (stepsWalked > tgt1 && stepsWalked < tgt2) {
        percentage = ((stepsWalked - tgt1) / (tgt2 - tgt1)) * 100;
      } else if (stepsWalked >= tgt2) {
        percentage = 99.999;
        this.metGoal = this.metGoal + 1;
      } else {
        percentage = 0;
      }
    }

    // console.log(`cprogress: c:${circle}, steps:${stepsWalked}/${totalSteps}, progress:${percentage}%, cx:${cx}, cy:${cy}, radius:${radius}`);

    // if (percentage === 100) percentage = 99.999;

    const a = percentage * 2 * Math.PI / 100; // angle (in radian) depends on percentage

    const r = radius; // radius of the circle
    var rx = r,
      ry = r,
      xAxisRotation = 0,
      largeArcFlag = 1,
      sweepFlag = 1,
      xadjust = r * Math.sin(a),
      yadjust = r * Math.cos(a),
      x = cx + xadjust,
      y = cy - yadjust
    if (percentage <= 50) {
      largeArcFlag = 0;
    } else {
      largeArcFlag = 1;
    }

    return `A${rx} ${ry} ${xAxisRotation} ${largeArcFlag} ${sweepFlag} ${x} ${y}`;
  }





  /////////////////////////////////////////////////////////////////////////
  //
  // render function
  render = () => (
    <Svg width={this.props.boxWidth} height={this.props.boxHeight} viewBox={`0 0 ${this.props.boxWidth} ${this.props.boxHeight}`}>
      {/* Outer progress : Full radius*/}
      <Circle
        stroke={this.props.bgColor}
        strokeWidth={this.props.width}
        strokeOpacity={this.bgLayerOpacity}
        r={this.props.r}
        cx={this.props.cx}
        cy={this.props.cy}
        fill="none"
      />
      <Path
        stroke={this.props.progressColor}
        strokeWidth={this.props.width}
        strokeDasharray={this.props.dashArray}
        strokeOpacity={this.progressLayerOpacity}
        d={`M${this.props.cx} ${(this.props.cy) - this.props.r} 
              ${this.generateArc(3, this.props.steps, this.props.stepsGoal, this.props.cx, this.props.cy, this.props.r)}`}
      />
      {/* First Inner progress : Radius reduced by firstCascade times width */}
      <Circle
        stroke={this.props.bgColor}
        strokeWidth={this.props.width}
        strokeOpacity={this.bgLayerOpacity}
        r={this.props.r - (this.firstCascade * this.props.width)}
        cx={this.props.cx}
        cy={this.props.cy}
        fill="none"
      />
      <Path
        stroke={this.props.progressColor}
        strokeWidth={this.props.width}
        strokeDasharray={this.props.dashArray}
        strokeOpacity={this.progressLayerOpacity}
        d={`M${this.props.cx} ${(this.props.cy) - (this.props.r - (this.firstCascade * this.props.width))} 
              ${this.generateArc(2, this.props.steps, this.props.stepsGoal, this.props.cx, this.props.cy, this.props.r - (this.firstCascade * this.props.width))}`}
      />
      {/* Second Inner progress : Radius reduced by secondCascade times width */}
      <Circle
        stroke={this.props.bgColor}
        strokeWidth={this.props.width}
        strokeOpacity={this.bgLayerOpacity}
        r={this.props.r - (this.secondCascade * this.props.width)}
        cx={this.props.cx}
        cy={this.props.cy}
        fill="none"
      />
      <Path
        stroke={this.props.progressColor}
        strokeWidth={this.props.width}
        strokeDasharray={this.props.dashArray}
        strokeOpacity={this.progressLayerOpacity}
        d={`M${this.props.cx} ${(this.props.cy) - (this.props.r - (this.secondCascade * this.props.width))} 
              ${this.generateArc(1, this.props.steps, this.props.stepsGoal, this.props.cx, this.props.cy, this.props.r - (this.secondCascade * this.props.width))}`}
      />
      <Text
        stroke="white"
        fill="white"
        fontSize="15"
        fontFamily="sans-serif"
        x={this.props.cx - 15}
        y={this.props.cy - 50}
        textAnchor="start"
      >
        {(this.metGoal >= 3) ? 3 : this.metGoal} / 3
      </Text>
      <Text
        stroke="white"
        fill="white"
        fontSize="12"
        fontFamily="sans-serif"
        x={this.props.cx}
        y={this.props.cy - 30}
        textAnchor="middle"
      >
        of daily goal completed
      </Text>
      <Text
        stroke="white"
        fill="white"
        fontSize="30"
        fontFamily="sans-serif"
        x={this.props.cx}
        y={this.props.cy + 35}
        textAnchor="middle"
      >
        {this.props.children}
      </Text>
      <Text
        stroke="white"
        fill="white"
        fontSize="12"
        fontFamily="sans-serif"
        x={this.props.cx}
        y={this.props.cy + 60}
        textAnchor="middle"
      >
        steps walked!
            </Text>
      {/* <Rect
                x="1"
                y="1"
                width={this.props.boxWidth - 2}
                height={this.props.boxHeight - 2}
                fill="none"
                stroke="red"
                strokeWidth="1"
            /> */}
    </Svg>
  )
};