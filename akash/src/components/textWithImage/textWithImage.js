import React from 'react';
import {StyleSheet, View, Image, Text} from 'react-native';

const textWithImage = (props) => (
    <View style={styles.headerStyle}>
        <Image source={props.source} style={styles.txtBgImgStyle} />
        <Text style={styles.textStyle}>
            {props.children}
        </Text>
    </View>
);

const styles = StyleSheet.create({
  headerStyle: {
    // borderColor: "grey",
    // borderWidth: 1,
    // flex: 1,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
  textStyle: {
    fontSize: 22,
    position: "absolute",
    // top: 0, left: 0, right: 0, bottom: 0,
    color: "white",
    fontStyle: "italic",
    fontWeight: "bold",
  },
  txtBgImgStyle: {
    // borderColor: "red",
    // borderWidth: 1,
    resizeMode: "stretch",
    alignItems: "center",
    justifyContent: "center",
  },
});

export default textWithImage;