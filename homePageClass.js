/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format 
 * 
 */

import React, { Component } from 'react';
import {
  Text, View, StyleSheet, ImageBackground, Dimensions,
  AppState,
  NativeModules, Image, PixelRatio
} from 'react-native';
import { NavigationEvents } from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';
import DeviceInfo from 'react-native-device-info';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

// for googlefit
import GoogleFit, { Scopes } from 'react-native-google-fit';

//
//
// Import custom components
import CircularProgress from './src/components/circularProgressBar/CircularProgress';
import HorizontalProgress from './src/components/horizontalProgressBar/HorizontalProgress';
import HorizontalProgressDynamic from './src/components/horizontalProgressBar/HorizontalProgressDynamic';
import CascadingCircularProgress from './src/components/circularProgressBar/CascadingCircularProgress';
import TextWithImage from './src/components/textWithImage/textWithImage';
import Dates from './src/helpers/Dates';

//
//
// Gather app window and screen dimensions
const window = Dimensions.get("window");
const screen = Dimensions.get("screen");

//
//
// Image handler for the app background
const bgimage = require('./src/backgrounds/app_background.png');
const classTxtBgImage = require("./src/backgrounds/class_background.png");
const mascotImg = require("./src/backgrounds/mascot.png");

//
//
// General log lead strings
const logLead = "[*]";





//
//
// Main app component
export default class App extends Component {
  // Refresh dimensions
  // const [dimensions, setDimensions] = useState({ window, screen });

  // const onChange = ({ window, screen }) => {
  //   setDimensions({ window, screen });
  // }





  /////////////////////////////////////////////////////////////////////////
  //
  // Constructor
  constructor(props) {
    super(props);

    this.dimensions = { window, screen };

    // Points and leveling progress
    // State
    this.state = {

      userClass: "Beginner",
      profileLevelPcnt: "1",
      stepsWalked: 0,
      stepsGoal: "10000",
      pointProg: 0,
      today: Dates._ddmmyyyy(),
      appState: AppState.currentState,
      avatarSource: { "uri": "asset:/defaultImage.png" },
      level: 1

    }

    // Handler variables
    this.offsetby = 66;
    this.hsize = this.dimensions.window.width - this.offsetby;
    this.vsize = (this.dimensions.window.height / 2) - this.offsetby;
    this.strokeWidth = 6;
    this.radius = ((this.hsize - this.strokeWidth) / 2) - (this.offsetby / 2);
    this.boxWidth = this.hsize - 20;
    this.boxHeight = 28;
    this.progressCx = this.hsize / 2;
    this.progressCy = this.vsize / 2;

    // Variables for profile pic view
    this.profileViewPcnt = 20;
    this.profileWidth = this.profileViewPcnt * this.hsize / 100;
    this.profileCx = this.profileWidth / 2;
    this.profileCy = this.profileWidth / 2;
    this.profileStrokeWidth = 3;
    this.profileRadius = (this.profileWidth - this.profileStrokeWidth) / 2;
    this.scale = this.dimensions.window.width / 320;

    // Fetching old values from db
    db.transaction(tx => {
      tx.executeSql("select userlevel from userdetails", [],
        (tx, results) => {
          // read userlevel and set userclass
          console.log("userlevel from localdb: ", results.rows.item(0)['userlevel']);
          switch (results.rows.item(0)['userlevel']) {
            case 0: {
              console.log("inside if 1");
              this.setState({ userClass: 'Beginner', stepsGoal: 5000 });
              break;
            }
            case 1: {
              console.log("inside if 2");
              this.setState({ userClass: 'Medium', stepsGoal: 7500 });
              break;
            }
            case 2: {
              console.log("inside if 3");
              this.setState({ userClass: 'Master', stepsGoal: 10000 });
              break;
            }
            default: {
              console.log("Error! UNKNOWN userlevel ID found.")
              break;
            }
          }
        });
    });

    // keys for async storage
    this.stateKey = "@states@";
    this.subscriptionKey = "@subscriptions@";
    this.appRunningKey = "@appRunning@";
    this.stepsKey = "@stepswalked@";
    this.pointsKey = "@pointsearned@";
    this.timeKey = "@timestamp@";
    this.historyKey = "@historytimekey@";

    // Array storing sensor values
    this.sensorValues = [];
    this.toProcess = [];

    // recorded sensor values in current tick
    this.counter = 0;

    // ticker interval in milli seconds
    this.refreshTimer = 1 * 6 * 1000; // setting for 4 minutes // 5000;
    // batch size (to be dumped to csv)
    this.batchSize = 15;
    // sensor update interval. in milli seconds
    this.sensorUpdateInterval = 650;
    // processing record threshold in number of records
    this.processingThreshold = 15;

    // for processing the file
    this.stepSum = 0;
    this.prevAcceleration = 0;
    this.accelThreshold = 4.2;
    this.retVal = 0;
    this.result = 0;
    this.oldState = {};

    // for Google Fit package
    this.providerSourceTag = "com.google.android.gms:estimated_steps";

    // for point calculation
    const pointsLimits = {
      beginner: {
        goal1: 12,
        goal2: 25,
        goal3: 50,
      },
      intermediate: {
        goal1: 18,
        goal2: 37,
        goal3: 75,
      },
      master: {
        goal1: 25,
        goal2: 50,
        goal3: 100,
      },
    };
    this.currTotalSteps = 0;

    // set goal points for current user class
    this.pointGoals = pointsLimits.master;

  }





  /////////////////////////////////////////////////////////////////////////
  //
  // pushing exception to server
  _exception(appfunction = 'unknown', errorH = 'unknown', errorM = 'unknown') {
    let devicemodel = DeviceInfo.getModel();
    let sysversion = DeviceInfo.getSystemVersion()
    let devicebrand = DeviceInfo.getBrand()
    let sysos = DeviceInfo.getSystemName()
    console.log("Device Model: ", devicemodel)
    console.log("System Version: ", sysversion)
    console.log("Brand: ", devicebrand)
    console.log("OS: ", sysos)
    console.log("Function: ", appfunction)
    fetch(hostip + 'exceptions', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        userId: uid,
        brand: devicebrand,
        device: devicemodel,
        deviceOs: sysos,
        softwareVersion: sysversion,
        method: appfunction,
        errorHeader: errorH,
        errorMessage: errorM
      })
    })
      .then((response) => response.json())
      .then((responseData) => {
        console.log("Response", responseData)
        if (responseData.response == 'Success') {
          console.log("Error updated in server")
        }
      })
      .catch(error => {
        console.log('Error: ' + error.message);
      })
  }





  /////////////////////////////////////////////////////////////////////////
  //
  // Normalizing font sizes to screen resposive text sizes
  _normalize(size) {
    const newSize = size * this.scale
    return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2

  }





  /////////////////////////////////////////////////////////////////////////
  //
  // Pulling user level, leveling progress and profile image
  _userDetails() {
    console.log("inside _userdetails function")
    // user image from local database 
    db.transaction(tx => {
      tx.executeSql('select profileimage, levelprcnt, level from userdetails', [], (tx, results) => {
        console.log("User level : ", results.rows.item(0)['level'])
        this.setState({
          avatarSource: { "uri": results.rows.item(0)['profileimage'] },
          level: results.rows.item(0)['level'],
          profileLevelPcnt: results.rows.item(0)['levelprcnt'],
        });
      });
    });
  }





  /////////////////////////////////////////////////////////////////////////
  //
  // Update state with steps from Native activity tracker
  updateStates = async () => {

    // Setting loglead
    var loglead = '[updateStates] ';

    // Getting steps from Google Fit

    // Gather dates for today
    var currDateString = [this.state.today.substr(this.state.today.length - 4), this.state.today.substr(2, 2), this.state.today.substr(0, 2)].join("-")
    var currDate = new Date(currDateString)
    console.log(`${loglead}Gathered dates: ${currDateString}`)
    console.log(`${loglead}StartDate: ${currDate.toISOString()}`)
    var startDate = currDate.toISOString();
    currDate.setDate(currDate.getDate() + 1)
    console.log(`${loglead}EndDate: ${currDate.toISOString()}`)
    var endDate = currDate.toISOString();

    // Problem here ********************************************************
    // Gathered dates: 2020-09-15
    // StartDate: 2020-09-15T00:00:00.000Z
    // EndDate: 2020-09-14T23:44:39.253Z
    // *********************************************************************
    // ************************** Fixed ************************************
    // Fixed by adding one day to startDate
    // Still don't know why we had a problem with timezones in new Date()
    // *********************************************************************


    // Retrieve sample steps from googlefit
    const options = {
      startDate: startDate, // start timestamp in ISO // doint it this way marks startdate to 00:00:00 in 24hrs for that day // "2020-08-26T12:09:20.493Z"
      endDate: endDate, // now in ISO
      bucketUnit: "DAY", // optional - default "DAY". Valid values: "NANOSECOND" | "MICROSECOND" | "MILLISECOND" | "SECOND" | "MINUTE" | "HOUR" | "DAY"
      bucketInterval: 1, // optional - default 1.
    }

    await GoogleFit.getDailyStepCountSamples(options)
      .then((res) => {

        res.forEach((provider) => {

          if (provider.source.includes(this.providerSourceTag)) {
            provider.steps.forEach((entry) => {

              if (entry.date === currDateString) {
                console.log(`${loglead}Date: ${entry.date} Steps: ${entry.value}`)
                this.stepSum = entry.value
              }
            })
          }
        })
      })
      .catch((err) => {
        // pushing error to exception handler
        this._exception("homePageClass.updateStates", "GoogleFit.getDailyStepCountSamples error", err);
        console.warn(err);
      })

    console.log(`${loglead}Steps from Google Fit fetched.`);

    // reading previous stored state
    await AsyncStorage.getItem(this.stateKey, (error, results) => {
      if (results !== null) {
        let storedState = JSON.parse(results);
        this.oldState = storedState;
      } else {
        this.oldState = this.state;
      }

      // checking values
      // console.log (`oldState stepsWalked: ${this.oldState.stepsWalked}`);
      // console.log (`oldState pointProg: ${this.oldState.pointProg}`);

      // setting updated state
      AsyncStorage.setItem(this.stateKey, JSON.stringify(this.oldState, this._replacer),
        (error) => {
          if (error === null) {
            console.log(`${loglead}Updated states in AsyncStorage.`);
          } else {
            console.log(error);
          }
        }
      ).catch((error) => {
        // pushing error to exception handler
        this._exception("homePageClass.updateStates", "AsyncStorage.setItem(this.stateKey...) error", error);
        console.log(error)
      });

      // setting updated timestamp
      AsyncStorage.setItem(this.timeKey, this.state.today,
        (error) => {
          if (error === null) {
            console.log(`${loglead}Updated timestamp in AsyncStorage.`);
          } else {
            console.log(error);
          }
        }
      ).catch((error) => { console.log(error) });
    }).catch((error) => {
      // pushing error to exception handler
      this._exception("homePageClass.updateStates", "AsyncStorage.getItem(this.stateKey...) error", error);
      console.log(error)
    });

  }

  /////////////////////////////////////////////////////////////////////////
  //
  // replacer function to update stepswalked 
  //        * sets the updated steps and points earned *
  _replacer = (key, value) => {

    // updating values for steps walked and points earned
    if (key === "stepsWalked") {
      // updating step count
      this.currTotalSteps = this.stepSum;
      return this.currTotalSteps;
    } else if (key === "pointProg") {
      // calculating new points
      if (this.currTotalSteps >= (this.state.stepsGoal)) {
        return this.pointGoals.goal3;
      } else if (this.currTotalSteps >= (0.50 * this.state.stepsGoal)) {
        return this.pointGoals.goal2;
      } else if (this.currTotalSteps >= (0.25 * this.state.stepsGoal)) {
        return this.pointGoals.goal1;
      } else {
        return value;
      }
    } else {
      return value;
    }
  }





  /////////////////////////////////////////////////////////////////////////
  //
  // component mounted and start timer 
  componentDidMount() {

    // Setting loglead
    loglead = '[componentDidMount] '

    // Check for google fit access

    // Toasting for trying to login to google fit
    NativeModules.NativeProcess.toast("Trying to access GoogleFit data...", NativeModules.NativeProcess.SHORT);

    // For googlefit
    const options = {
      scopes: [
        Scopes.FITNESS_ACTIVITY_READ
      ],
    }

    GoogleFit.authorize(options)
      .then((authresult) => {
        if (authresult.success) {
          console.log(`${loglead}GoogleFit auth sucess!`);

          // Start Recording subscribtion
          const options = {
            scopes: [
              Scopes.FITNESS_ACTIVITY_READ_WRITE,
            ],
          }

          // subscribe
          GoogleFit.startRecording((callback) => {
            // This starts the google fit recording if in case its not yet
            // **** Don't know how to stop it though :P
          });

          // Toasting for trying to login to google fit
          NativeModules.NativeProcess.toast("Log in success. Tracking!", NativeModules.NativeProcess.SHORT);

          console.log(`${loglead}Starting appStartProcess!`);

          //
          //
          //
          // Start the app start services and updates
          this.appStartProcess();

        } else {
          console.log(`${loglead}GoogleFit auth denied! ${authresult.message}`);

          // pushing error to exception handler
          this._exception("homePageClass.appStartProcess", "GoogleFit Login denied", authresult.message);

          // Toasting for trying to login to google fit
          NativeModules.NativeProcess.toast("Authentication was declined. Log in Failed.", NativeModules.NativeProcess.SHORT);
        }
      })
      .catch((error) => {
        // pushing error to exception handler
        this._exception("homePageClass.appStartProcess", "GoogleFit.authorize() error", error);

        console.log(`GoogleFit auth error! ${error}`)
      })

    // refresh the level progress and levels
    this._userDetails()

  }

  /////////////////////////////////////////////////////////////////////////
  //
  // component will be unmounted; stop the timer and force stop all sensors
  componentWillUnmount() {

    // clearing timer
    clearInterval(this.timerID);
    console.log("Timer stopped.");

    // set subscribed flag to persistant storage
    // this._setSubscriptionkey();
  }





  /////////////////////////////////////////////////////////////////////////
  //
  // app start procedure
  appStartProcess = async () => {
    console.log("Processing for app start");

    // Setting loglead value
    var loglead = '[appStartProcess] '

    // Setting variables for history refresh
    var diffDays = 0;
    var today = this.state.today;

    // Refreshing historical step data
    // Checking last refreshed date
    await AsyncStorage.getItem(this.timeKey, (error, results) => {
      if (results === null) {
        // No time key found. Set history to be fetched for default prior days
        AsyncStorage.setItem(this.historyKey, diffDays.toString(), (error) => {
          if (error === null) {
            console.log(`${loglead}Set history refresh to be ${diffDays} days.`);
          } else {
            console.log(error);
          }
        })
        // Also set initial entry into db
        this.iuStepData(today, 0, { requestedBy: "AppStartProcess" });

      } else {
        // Time key found. 
        console.log(`${loglead}Found timekey. time:${results}`);

        // Checking for days missed
        var lastRefreshDate = new Date([results.substr(results.length - 4), results.substr(2, 2), results.substr(0, 2)].join("-"));
        console.log(`${loglead}Last refreshed: ${lastRefreshDate.toISOString()}`);
        var currDate = new Date([today.substr(today.length - 4), today.substr(2, 2), today.substr(0, 2)].join("-"));
        console.log(`${loglead}Current date: ${currDate}`);

        var diffTime = currDate - lastRefreshDate;
        diffDays = Math.ceil(diffTime / (1000 * 3600 * 24));

        console.log(`${loglead}Days missed: ${diffDays}`)
        console.log(`${loglead}Set history refresh to be ${diffDays} days.`);

        // if (diffDays > 1) {
        //   // Updating number of days for which history is to be fetched
        //   AsyncStorage.setItem (this.historyKey, diffDays.toString(), (error) => {
        //     if (error === null) {
        //       console.log (`${loglead}Set history refresh to be ${diffDays} days.`);
        //     } else {
        //       console.log (error);
        //     }
        //   });
        // } else {
        //   // Update will be handled by day change
        //   console.log (`${loglead}Day diff is 1. Nothing to do here.`)
        //   diffDays = 1;
        // }
      }
    })

    // Refresh historical data
    this.refreshOldSteps(today, diffDays);

    // Shifted to a new function call at the end of refreshOldSteps
  }





  /////////////////////////////////////////////////////////////////////////
  //
  // methods to run every timer tick
  executeEveryTick = async () => {

    // check for a day change
    await this.checkChangeOfDay();

    // refreshing state with stored state data
    // await this.refreshState();

    // fetch data from Native tracker and update stored state
    // await this.updateStates ();


  }





  /////////////////////////////////////////////////////////////////////////
  //
  // refresh current state with data from asyncstorage if it exists
  refreshState = async () => {

    // refreshing current state from asyncstorage
    await AsyncStorage.getItem(this.stateKey, (error, results) => {
      if (results !== null) {
        // refresh state with stored state
        let newState = JSON.parse(results);
        if ((this.state.stepsWalked === newState.stepsWalked) && (this.state.pointProg === newState.pointProg)) { }
        else {
          this.setState(newState);

          // Update state into db
          db.transaction(tx => {
            tx.executeSql("update userstatdetails set dailysteps=?, dailypoints=?, updated_by=?, updated_on=CURRENT_TIMESTAMP where timeid=?", [newState.stepsWalked, newState.pointProg, 'StateRefresh', newState.today], (tx, results) => {
              // update sql completed
              console.log("Date changed! Update row. resultsaffected: ", results.rowsAffected);
            });
          });

          // reinitialize avatar url and level value
          db.transaction(tx => {
            tx.executeSql('select profileimage, levelprcnt, level from userdetails', [], (tx, results) => {
              console.log("User level : ", results.rows.item(0)['level'])
              this.setState({
                avatarSource: { "uri": results.rows.item(0)['profileimage'] },
                level: results.rows.item(0)['level'],
                profileLevelPcnt: results.rows.item(0)['levelprcnt'],
              });
            });
          });
        }
      } else {
        console.log(`Error! ${error}... No updated state found.`);
      }
    }).catch((error) => {
      // pushing error to exception handler
      this._exception("homePageClass.refreshState", "AsyncStorage.getItem(this.stateKey...) error", error);
      console.log(error)
    });

    // Testing pulling new steps here
    this.updateStates();

  }





  /////////////////////////////////////////////////////////////////////////
  //
  // check change of day and make state updates
  checkChangeOfDay = async () => {

    // setting loglead
    loglead = "[changeOfDay] ";

    // Get today's date
    let today = Dates._ddmmyyyy();

    await AsyncStorage.getItem(this.stateKey, (error, results) => {
      if (results !== null) {
        // There exists a stored state already

        let storedState = JSON.parse(results);
        // Checking if dates match
        if (storedState.today != today) {
          // A date change has been detected

          // Fetch the steps for previous day ie storedState.today
          // Split storedState.today and convert to 'yyyy-mm-dd' format
          var oldDate = [storedState.today.substr(storedState.today.length - 4), storedState.today.substr(2, 2), storedState.today.substr(0, 2)].join("-");

          // Retrieve sample steps from googlefit
          var options = {
            startDate: new Date(oldDate).toISOString(), // start timestamp in ISO // doint it this way marks startdate to 00:00:00 in 24hrs for that day // "2020-08-26T12:09:20.493Z"
            endDate: new Date().toISOString(), // now in ISO
            bucketUnit: "DAY", // optional - default "DAY". Valid values: "NANOSECOND" | "MICROSECOND" | "MILLISECOND" | "SECOND" | "MINUTE" | "HOUR" | "DAY"
            bucketInterval: 1, // optional - default 1.
          };

          // Startdate and endDate to be refined.
          // Could lead to doublecounted steps

          GoogleFit.getDailyStepCountSamples(options)
            .then((res) => {

              console.log(loglead, 'Expanding google fit results...')

              res.forEach((provider) => {
                console.log(`Provider: ${provider.source}`)

                if (provider.source.includes(this.providerSourceTag)) {
                  // console.log ("Matched!")
                  // console.log ("Entries...")
                  provider.steps.forEach((entry) => {

                    if (entry.date === oldDate) {
                      console.log(`${loglead}Date: ${entry.date}`)
                      console.log(`${loglead}Steps: ${entry.value}`)
                      console.log(`${loglead}Updating steps for previous day from google fit...`)
                      storedState.stepsWalked = entry.value
                    }
                  })
                }
              })
            })
            .catch((err) => {
              // pushing error to exception handler
              this._exception("homePageClass.checkChangeOfDay", "GoogleFit.getDailyStepCountSamples error", err);
              console.warn(err)
            })

          // Process for date change
          // Update the latest state into db
          //
          //
          // db.transaction(tx => {
          //   tx.executeSql("update userstatdetails set dailysteps=?, dailypoints=?, updated_by=?, updated_on=CURRENT_TIMESTAMP where timeid=?", [storedState.stepsWalked, storedState.pointProg, 'ChangeOfDay', storedState.today], (tx, results) => {
          //     // update sql completed
          //     console.log("Date changed! Update row. resultsaffected: ", results.rowsAffected);
          //     // checking for valid userlevel and pushing exception if invalid
          //     if (results.rowsAffected != 1) {
          //       // sending exception
          //       this._exception ("homePageClass.checkChangeOfDay", "Date change update error", "Multiple update rows affected");
          //     }
          //   });
          // });
          var rslts;
          rslts = this.iuStepData(storedState.today, storedState.stepsWalked, { requestedBy: "ChangeOfDay" });
          if (!rslts) {
            // sending exception
            this._exception("homePageClass.checkChangeOfDay", "Date change update error", "Error while updating old steps");
          }

          // console.log(`Update qry: update userstatdetails set 
          //   dailysteps=${storedState.stepsWalked},
          //   dailypoints=${storedState.pointProg}
          //   where timeid=${storedState.today}`);

          // Insert a fresh state entry into db
          //
          //
          // var year = new Date().getFullYear();
          // db.transaction(tx => {
          //   tx.executeSql("insert into userstatdetails (dailysteps, dailypoints, timeid, day, week, month, year, updated_by, updated_on, created_by, created_on) values (0, 0, ?, ?, ?, ?, ?, ?, CURRENT_TIMESTAMP, ?, CURRENT_TIMESTAMP)", [today, Dates._getDay(), Dates._getWeek(), Dates._getMonth(), year, 'ChangeOfDay', 'ChangeOfDay'], (tx, results) => {
          //     // insert sql completed
          //     console.log(loglead, "Date changed! Insert row. resultsaffected: ", results.rowsAffected);
          //     // checking for valid userlevel and pushing exception if invalid
          //     if (results.rowsAffected == 0) {
          //       // sending exception
          //       this._exception ("homePageClass.checkChangeOfDay", "Date change insert error", "No new rows inserted");
          //     } else if (results.rowsAffected != 1) {
          //       // sending exception
          //       this._exception ("homePageClass.checkChangeOfDay", "Date change insert error", "Multiple rows inserted");
          //     }
          //   });
          // });
          rslts = this.iuStepData(today, 0, { requestedBy: "ChangeOfDay" });
          if (!rslts) {
            // sending exception
            this._exception("homePageClass.checkChangeOfDay", "Date change update error", "Error while inserting new date entry");
          }

          // console.log(`Insert qry: insert into userstatdetails (dailysteps, dailypoints, timeid, day, week, month)
          //   values (0, 0, ${today}, ${Dates._getDay()}, ${Dates._getWeek()}, ${Dates._getMonth()})`);

          // Reseting current state with today's date
          this.resetStateWithDate(today);

          // Refresh stored state with reset state
          //
          // Removing the stored state from AsyncStorage
          // This forces the processFile () to use the current state and write it fresh into AsyncStorage
          AsyncStorage.removeItem(this.stateKey, (error) => {
            if (error === null) {
              // removed stateKey successfully
            }
          }).catch((error) => {
            // pushing error to exception handler
            this._exception("homePageClass.checkChangeOfDay", "AsyncStorage.removeItem (this.stateKey...) error", err);
            console.log(error)
          });

          // All daychange process completed
          // Sending a Am-Alive status to the server for debugging
          this._exception("App alive status", "App working fine", `Application alive status for ${today}`);
        } else {
          // Dates match
          // Nothing to do here
          // console.log ("Dates match. Nothing to do here.");
        }
      } else {
        // No stored state found
        // Nothing to do here
        // console.log ("No stored state found. Nothing to do here,");
      }
    }).catch((error) => {
      // pushing error to exception handler
      this._exception("homePageClass.checkChangeOfDay", "AsyncStorage.getItem (this.stateKey...) error", err);
      console.log(error)
    });


    // testing refreshing state here
    this.refreshState();
  }





  /////////////////////////////////////////////////////////////////////////
  //
  // Reset the state with new day
  resetStateWithDate = (day) => {
    // Only the stepsWalked, pointProg and today has to be reset
    let newState = {

      stepsWalked: 0,
      pointProg: 0,
      today: day,

    };
    // Reset the stepsum variable that helps collect steps
    this.stepSum = 0;
    // Resetting
    this.setState(newState);
  }





  /////////////////////////////////////////////////////////////////////////
  //
  // Refresh database with step data for prior days marked by the appstartprocess
  refreshOldSteps = async (startday, days) => {
    // setting loglead
    loglead = '[refreshOldSteps] ';

    console.log(`${loglead}reached!`)

    // validating for refresh
    if (days > 1) {
      // data to be refreshed
      console.log(`${loglead}Refresh needed.`);

      // Toasting for trying to login to google fit
      NativeModules.NativeProcess.toast("Refreshing old step data...", NativeModules.NativeProcess.SHORT);

      var startDate = new Date([startday.substr(startday.length - 4), startday.substr(2, 2), startday.substr(0, 2)].join("-"));
      // setting the end date
      var ending = startDate.toISOString();
      console.log(`${loglead}Ending: ${ending}`);
      // calculating start date
      startDate.setDate(startDate.getDate() - days);
      var starting = startDate.toISOString();
      console.log(`${loglead}Starting: ${starting}`);

      // Variable to check if the inner function calls can be serialized
      var checkVar = false;

      // fetching from google fit data
      const options = {
        startDate: starting, // start timestamp in ISO
        endDate: ending, // end timestamp in ISO
        bucketUnit: "DAY", // optional - default "DAY". Valid values: "NANOSECOND" | "MICROSECOND" | "MILLISECOND" | "SECOND" | "MINUTE" | "HOUR" | "DAY"
        bucketInterval: 1, // optional - default 1.
      }

      await GoogleFit.getDailyStepCountSamples(options)
        .then((res) => {
          console.log(`${loglead}Daily steps >>> `, res)

          console.log(`${loglead}Expanding the results...`)

          res.forEach((provider) => {
            console.log(`${loglead}Provider: ${provider.source}`)

            if (provider.source.includes(this.providerSourceTag)) {
              console.log(`${loglead}Entries...`)
              provider.steps.forEach((entry) => {
                console.log(`${loglead}Date: ${entry.date} Steps: ${entry.value}`)

                // Passing this date and steps value for iu in db
                checkVar = this.iuStepData(Dates._ddmmyyyy({ format: 'yyyy-mm-dd', value: entry.date }), entry.value);
              })
            }
          })
        })
        .catch((err) => { console.warn(err) })

      console.log(`${loglead}Steps from Google Fit fetched. ${checkVar}`);
    } else {
      // no refresh needed
    }

    // setting loglead
    loglead = '[refreshOldSteps] ';

    /* // trying to validate database data
    console.log (`${loglead}Reached view data...`)
    db.transaction(tx => {
      tx.executeSql("SELECT * FROM userstatdetails", [], (tx, results) => {
        console.log (`${loglead}Row count: ${results.rows.length}`);
        for (let i=0;i<results.rows.length;i++) {
          console.log (`${loglead}Data row: ${JSON.stringify(results.rows.item(i))}`)
        }
      })
    }) */

    console.log(`${loglead}refreshOldSteps completed!`)

    // Continueing app start process


    // Refresh from stored state
    // await this.refreshState ();

    // // fetch data from Native tracker and update stored state
    // await this.updateStates ();

    // Check for a day change
    await this.checkChangeOfDay();

    console.log("appStartProcess is done!");

    // start a timer
    console.log("Starting timer...");
    this.timerID = setInterval(() => {
      this.executeEveryTick();
    }, this.refreshTimer);
  }





  /////////////////////////////////////////////////////////////////////////
  //
  // Helper function to write steps into database
  // Also has a requestedBy field to write the requestor for this update along with the row
  iuStepData = (timeid, steps, opts = { requestedBy: "RefreshOldStepData" }) => {
    // Setting loglead
    loglead = '[iuStepData] '
    // insert or update flag
    // var iuflag = 0;
    var existingSteps = 0;
    // Check if timeid exists in stat table
    // console.log (`${loglead}Running a select query for timeid...`)
    db.transaction(tx => {
      tx.executeSql('SELECT * FROM userstatdetails WHERE timeid=?', [timeid], (tx, results) => {
        console.log(`${loglead}Fetched ${results.rows.length} rows.`)
        // validate returned result count
        if (results.rows.length > 0) {
          // record exists for current timeid
          // flag for update
          // iuflag = 1;
          existingSteps = results.rows.item(0)['dailysteps'];
          // console.log (`${loglead}Row for ${timeid} exists. Flagging for update.. iuflag:${iuflag}`)
          // record to be updated
          // if (existingSteps != steps) { // removing this to test if it fixes duplicate issue
          // steps have changed from db. Updating data in db
          var points = this.calcPointsEarned(steps);

          db.transaction(tx => {
            tx.executeSql('UPDATE userstatdetails SET dailysteps=?, dailypoints=?, updated_by=?, updated_on=CURRENT_TIMESTAMP WHERE timeid=?', [steps, points, opts.requestedBy, timeid], (tx, results) => {
              // validate execute
              if (results.rowsAffected == 1) {
                // record updated. nothing to do here.
                return true;
              } else if (results.rowsAffected == 0) {
                // no rows updated. update query failed            
                return false;
              } else {
                // multiple rows updated
                // raise exception
                return false;
              }
            })
          })
          // } // else nothing to do here. the steps match with that in db
        } else {
          // console.log (`${loglead}Row for ${timeid} missing. iuflag:${iuflag}. Row inserting...`)
          var points = this.calcPointsEarned(steps);
          var timeString = [timeid.substr(timeid.length - 4), timeid.substr(2, 2), timeid.substr(0, 2)].join("-")
          // record set for insert
          db.transaction(tx => {
            tx.executeSql('INSERT INTO userstatdetails (dailysteps, dailypoints, timeid, day, week, month, year, updated_by, updated_on, created_by, created_on) VALUES (?,?,?,?,?,?,?,?,CURRENT_TIMESTAMP,?,CURRENT_TIMESTAMP)', [steps, points, timeid, Dates._getDay({ format: 'yyyy-mm-dd', value: timeString }), Dates._getWeek(0, { format: 'yyyy-mm-dd', value: timeString }), Dates._getMonth({ format: 'yyyy-mm-dd', value: timeString }), Dates._getYear({ format: 'yyyy-mm-dd', value: timeString }), opts.requestedBy, opts.requestedBy], (tx, results) => {
              // validate execute
              if (results.rowsAffected > 0) {
                // records inserted without issues
                return true;
              } else {
                // error inserting records
                return false;
              }
            })
          })
        }
      });
    });
  }





  /////////////////////////////////////////////////////////////////////////
  //
  // Helper function to calculate points for steps provided
  calcPointsEarned = (steps) => {
    // calculating new points
    if (steps >= (this.state.stepsGoal)) {
      return this.pointGoals.goal3;
    } else if (steps >= (0.50 * this.state.stepsGoal)) {
      return this.pointGoals.goal2;
    } else if (steps >= (0.25 * this.state.stepsGoal)) {
      return this.pointGoals.goal1;
    } else {
      return 0;
    }
  }





  /////////////////////////////////////////////////////////////////////////
  //
  // rendering element stepCounter
  render() {
    return (
      <>
        <View style={styles.containerStyle}>
          <NavigationEvents
            onDidFocus={() => { this._userDetails() }}
          />
          <ImageBackground source={bgimage} style={styles.imageStyle}>
            <View style={styles.headerContainerStyle}>
              <View style={styles.classContainerStyle}>
                <TextWithImage source={classTxtBgImage}>
                  {this.state.userClass} challenger
                </TextWithImage>
              </View>
              <View style={styles.profileContainerStyle}>
                <Image source={this.state.avatarSource} style={{ alignItems: "center", justifyContent: "center", width: this._normalize(50), height: this._normalize(50), borderRadius: 60, borderWidth: 1, borderColor: 'white', position: 'absolute' }} />
                <Text allowFontScaling={false} style={{ fontSize: this._normalize(10), fontWeight: 'bold', color: 'rgb(254, 170, 58)', position: 'absolute', bottom: -this._normalize(14) }}>Level {this.state.level}</Text>
                <CircularProgress
                  boxWidth={this.profileWidth}
                  boxHeight={this.profileWidth}
                  bgColor="white"
                  width={this.profileStrokeWidth}
                  r={this.profileRadius}
                  cx={this.profileCx}
                  cy={this.profileCy}
                  progressColor="white"
                  dashArray="1, 0"
                  progressPcnt={this.state.profileLevelPcnt}
                />
              </View>
            </View>
            <View style={styles.statsContainerStyle}>
              <View style={styles.pointProgressContainer}>
                <CascadingCircularProgress
                  boxWidth={this.hsize}
                  boxHeight={this.vsize}
                  bgColor="white"
                  width={this.strokeWidth}
                  r={this.radius}
                  cx={this.progressCx}
                  cy={this.progressCy}
                  progressColor="rgb(255,152,75)"
                  dashArray="1, 0"
                  steps={this.state.stepsWalked}
                  stepsGoal={this.state.stepsGoal}
                >
                  {this.state.stepsWalked}
                </CascadingCircularProgress>
                <HorizontalProgressDynamic
                  boxWidth={this.boxWidth}
                  boxHeight={this.boxHeight}
                  leftEdge="3"
                  rightEdge="3"
                  bgColor="#ffffff"
                  progressColor="rgb(255,152,75)"
                  progressPcnt={this.state.pointProg}
                />
                <Text allowFontScaling={false} style={styles.textStyle}>{this.state.pointProg}/100 points earned today</Text>
              </View>
            </View>
            <View style={styles.footerContainerStyle}>

            </View>
          </ImageBackground>
        </View>
      </>
    );
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between",
  },
  imageStyle: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
  },
  textStyle: {
    color: "white",
    fontSize: 15,
    fontWeight: "bold",
  },
  headerTextStyle: {
    color: "white",
    fontSize: 15,
    fontWeight: "bold",
    fontFamily: "sans-serif-light",
  },
  headerContainerStyle: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "blue",
    flexDirection: "row",
    paddingLeft: 15,
    width: "100%",
    height: "20%",
    justifyContent: "space-around",
    alignItems: "center",
  },
  statsContainerStyle: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "grey",
    flexDirection: "column",
    padding: 20,
    width: "100%",
    height: "50%",
    justifyContent: "center",
    alignItems: "center",
  },
  footerContainerStyle: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "grey",
    flexDirection: "row",
    padding: 20,
    width: "100%",
    height: "30%",
    justifyContent: "center",
    alignItems: "center",
  },
  classContainerStyle: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "red",
    padding: 20,
    width: "70%",
    justifyContent: "center",
    alignItems: "center",
  },
  profileContainerStyle: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "red",
    // padding: 10,
    width: "30%",
    justifyContent: "center",
    alignItems: "center",
  },
  roundProgressContainer: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "grey",
    height: "70%",
    justifyContent: "center",
    alignItems: "center",
  },
  pointProgressContainer: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "grey",
    height: "30%",
    justifyContent: "center",
    alignItems: "center",
  },
  notificationContainerStyle: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "grey",
    width: "75%",
    justifyContent: "center",
    alignItems: "center",
  },
  notificationTextStyle: {
    color: "white",
    fontSize: 12,
    fontWeight: "bold",
  },
  mascottContainerStyles: {
    // borderWidth: 1,
    // borderStyle: "solid",
    // borderColor: "grey",
    width: "25%",
    justifyContent: "center",
    alignItems: "center",
  },
  placeholderStyle: {
    color: "#282c34",
    fontSize: 12,
    fontWeight: "bold",
  },
});

{/* <Text style={styles.textStyle}>This is the header view</Text> 
<Text style={styles.textStyle}>Profile picture</Text>
<Text style={styles.textStyle}>This is the current stats</Text>


  placeholderStyle: {
    color: "#282c34",
    fontSize: 12,
    fontWeight: "bold",
  },
*/ }