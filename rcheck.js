import React from 'react'

import { ImageBackground, Image, Button, TouchableOpacity } from 'react-native';
import { StyleSheet, View, Text, TextInput} from 'react-native';
import profile from './profile.png'

import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
 

export default class rcheck extends React.PureComponent {

 

    render() {
        var radio_props = [
            {label: 'Beginner (5000 daily step goal)', value: 0 },
            {label: 'Intermediate (7500 daily step goal)', value: 1 },
            {label: 'Master (10000 daily step goal)', value: 2 }
          ];



/***************************************************************************************************************** */
        return (
            <ImageBackground
            source={require('./background.png')}
            style={{width: '100%', height: '100%'}}>
                <View style={{flexDirection:'column', flex:1}}>
                    <View style={{alignItems: 'center', marginTop:'10%'}}>
                        <Image source={profile} style = {{width:100,height:100}}/>
                    </View>
                    <View style={{alignItems: 'center', marginTop:'5%'}}>
                        <Text style={{fontSize:30, color:'rgb(254, 170, 58)'}}> Welcome to KWapp!</Text>
                    </View>
                    <View style={{alignItems: 'center', marginTop:'5%'}}>
                        <Text style={{fontSize:18, color:'white'}}> Start earning points</Text>
                        <Text style={{fontSize:18, color:'white'}}> immediately!</Text>
                    </View>
                    <View style={{alignItems: 'center', marginTop:'5%'}}>
                        <Text style={{fontSize:18, color:'rgb(254, 170, 58)'}}> Create a nickname(optional)</Text>
                        <TextInput style={{ height: 40,width:290, borderColor: 'gray',backgroundColor:'white', borderWidth: 1, borderRadius: 10 }} />
                        </View>
                    <View style={{marginLeft:'2%', marginTop:'5%'}}>
                        <Text style={{fontSize:20, color:'rgb(254, 170, 58)'}}> Please choose a fitness level</Text>
                    </View>
                    <View style={{marginLeft:'2%', marginTop:'1%'}}>
                        <RadioForm
                        radio_props={radio_props}
                        initial={0}
                        buttonColor={'white'}
                        buttonStyle={{buttonInnerColor:'red',buttonOuterColor:'red'}}
                        labelColor={'rgb(254, 170, 58)'}
                        onPress={(value) => {this.setState({value:value})}}
                        />
                    </View>
                    <View style={[{alignItems: 'center',  marginTop:'5%'}]}>
                        <TouchableOpacity style={{height:40,alignItems:'center', width: 150, backgroundColor: 'rgb(254, 170, 58)',borderRadius: 10}}>
                            <Text style={{color: 'white',fontSize: 25}}> Done </Text>
                        </TouchableOpacity>

                    </View>
                    
                </View>


        </ImageBackground>
        )


    }
 
}
const styles = StyleSheet.create ({
    item: {
       flexDirection: 'column',
       width: '100%',
       height: 200,
       marginLeft: '5%',
       marginRight: '10%',
       marginTop: '10%',
       marginBottom: '3%',
       alignItems: 'center',
       position: 'relative'
       
       
    }
 })


