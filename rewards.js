import React, { useEffect, useState } from 'react'
import { BarChart, Grid } from 'react-native-svg-charts'
import { ImageBackground, Image } from 'react-native';
import { StyleSheet, View, Button, SafeAreaView, Text, Alert,UIManager,LayoutAnimation,TouchableOpacity, ScrollView} from 'react-native';
import city from "./cityscape.png";
import back_image from './src/backgrounds/app_background.png'

export default class rewards extends React.PureComponent {
    componentDidMount() {
        fetch('http://192.168.0.103:5000/getRewards',{
          method: 'GET'
        })
        .then(res => res.json())
        .then((data) => {
          this.setState({ brands: data.rewards})

         console.log("rewards api called")
        })
        .catch(console.log)
      }
      constructor(props) {
        super(props);
        this.state = {
            brands: [{"id":"1","brand": "Plant a tree","uri":"http://52.29.223.120:5252/reward1.png"},
                {"id":"2","brand": "Ugrade your athletic gear","uri":"http://52.29.223.120:5252/reward2.png"},
                {"id":"3","brand": "Explore food delivery services","uri":"http://52.29.223.120:5252/reward3.png"}]
        };
    }
  /*   state = {
        brands: [{"id":"1","brand": "netflix","uri":"http://52.29.223.120:5252/netflix.png"},
                {"id":"2","brand": "amazon","uri":"http://52.29.223.120:5252/ps.png"},
                {"id":"3","brand": "sony playstation","uri":"http://52.29.223.120:5252/amazon.png"},
                {"id":"4","brand": "netflix","uri":"http://52.29.223.120:5252/spotify.png"},
                {"id":"5","brand": "amazon","uri":"http://52.29.223.120:5252/zalando.png"},
                {"id":"6","brand": "sony playstation","uri":"http://52.29.223.120:5252/netflix.png"}],
        cachedHeight: '100%',
        containerHeight: null
     } */
   
  

    render() {

/***************************************************************************************************************** */
        return (
            <ImageBackground
            source={back_image}
            style={{width: '100%', height: '100%'}}>

                <ScrollView>
                    {
                this.state.brands.map((item, index) => (
                    <View key = {item.id} style = {styles.item} marginBottom= '5%'>
                        <Image source={ {uri: item.uri} } style={styles.item} width='100%' resizeMode='contain' />
                        <Button title="Click to Redeem" style={styles.item} color='rgb(254, 170, 58)'  />
                    </View>
                ))
                }
                
                </ScrollView>

        </ImageBackground>
        )


    }
 
}
const styles = StyleSheet.create ({
    item: {
       flexDirection: 'column',
       width: '100%',
       height: 200,
       marginLeft: '5%',
       marginRight: '10%',
       marginTop: '10%',
       marginBottom: '3%',
       alignItems: 'center',
       position: 'relative'
       
       
    }
 })


