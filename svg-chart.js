import React from 'react'
import { BarChart, Grid } from 'react-native-svg-charts'
import { ImageBackground, Image } from 'react-native';
import { StyleSheet, View, Button, SafeAreaView, Text, Alert,} from 'react-native';

export default class AreaChartExample extends React.PureComponent {
 
    render() {
        const fill = 'rgb(254, 170, 58)'
        const data   = [ 50, 10, 40, 95, 4, 24,85  ]
        return (
            <ImageBackground
            source={require('./background.png')}
            style={{width: '100%', height: '100%'}}>
                <View style={{ width: 400, height: 10, justifyContent: 'center', flexDirection: 'row', alignItems: 'center', marginTop: 70}}>
                    <Button
                    title="1 Week"
                    color="#7b899b"
                    onPress={() => Alert.alert('Button with adjusted color pressed')}
                    />
                    <Button
                    title="1 Month"
                    color="#7b899b"
                    onPress={() => Alert.alert('Simple Button2 pressed')}
                    />
                    <Button
                    title="6 Month"
                    color="#7b899b"
                    onPress={() => Alert.alert('Simple Button3 pressed')}
                    
                    />
                    <Button
                    title="1 Year"
                    color="#7b899b"
                    onPress={() => Alert.alert('Simple Button4 pressed')}
                    />
                </View>

            <BarChart
                style={{ height: 300 }}
                data={ data }
                svg={{ fill }}
                contentInset={{ top: 30, bottom: 30, left: 30, right: 30,  }}
                spacingInner={0.9}
                spacingOuter={0.5}
            >
                <Grid/>
            </BarChart>
            <View style= {{flexDirection:'column',flex:1}}>
                    <View style={ {marginLeft: '15%', width: 250, height: 30, backgroundColor: '#d6d2d2', marginBottom: '3%', borderRadius:10, borderWidth: 1, borderColor: '#d6d2d2'}} >
                        <Text style={{textAlign: 'left', marginLeft: '4%'}}>
                            Steps walked
                        </Text>
                    </View>
                    <View style={{marginLeft: '15%', width: 250, height: 30, backgroundColor: '#d6d2d2', marginBottom: '3%', borderRadius:10, borderWidth: 1, borderColor: '#d6d2d2'}} >
                        <Text style={{textAlign: 'left', marginLeft: '4%'}}>
                            Points earned
                        </Text>
                    </View>
                    <View style={ {width: 250, height: 38, marginBottom: '3%'}} >
                        <Image source = {require('./cityscape.png')} style = {{ width: 500, height: 200}}/>
                    </View>
            </View>

        </ImageBackground>
        )


    }
 
}
const styles = StyleSheet.create({

    button: {
      backgroundColor: "#DDDDDD"
    }
  });