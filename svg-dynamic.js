import React, { useEffect, useState } from 'react'
import { BarChart, Grid, XAxis, YAxis  } from 'react-native-svg-charts'
import { ImageBackground, Image } from 'react-native';
import { StyleSheet, View, Button, SafeAreaView, Text, Alert,UIManager,LayoutAnimation,TouchableOpacity} from 'react-native';
import city from "./cityscape.png";
import * as scale from 'd3-scale';

UIManager.setLayoutAnimationEnabledExperimental &&
  UIManager.setLayoutAnimationEnabledExperimental(true);

  const contentInset = {top: 10}
export default class AreaChartExample extends React.PureComponent {
 
      componentDidMount() {
        fetch('http://192.168.0.103:5000/statDaily',{
          method: 'GET'
        })
        .then(res => res.json())
        .then((data) => {
          this.setState({ data1: data.day1.pointsEarned, data2: data.day2.pointsEarned,
            data3: data.day3.pointsEarned, data4: data.day4.pointsEarned, data5: data.day5.pointsEarned, 
            data6: data.day6.pointsEarned, data7: data.day7.pointsEarned,stepsWalked: data.dailyStepsWalked,
             pointsEarned: data.dailyPointsEarned})

         console.log("daily api called")

        })
        .catch(console.log)

        
      }


      cityLocation() {
          console.log("city function called")
        if (this.state.stepsWalked > 5000 && this.state.stepsWalked <= 10000) {
            this.state.position = 'center'
        } else if (this.state.stepsWalked > 10000)
        {
            this.state.position = 'flex-start'
        }
        else {
            this.state.position = 'flex-end'
        }
    }      

      _daily() {
        fetch('http://192.168.0.103:5000/statDaily',{
          method: 'GET'
        })
        .then(res => res.json())
        .then((data) => {
          this.setState({ data1: data.day1.pointsEarned, data2: data.day2.pointsEarned,
            data3: data.day3.pointsEarned, data4: data.day4.pointsEarned, data5: data.day5.pointsEarned, 
            data6: data.day6.pointsEarned, data7: data.day7.pointsEarned,stepsWalked: data.dailyStepsWalked,
             pointsEarned: data.dailyPointsEarned})
         console.log("daily function called")
        })
        .catch(console.log)
      }

      _1month()
      {
        fetch('http://192.168.0.103:5000/statMonth',{
            method: 'GET'
          })
          .then(res => res.json())
          .then((data) => {
            this.setState({ data1: data.month1.pointsEarned, data2: data.month2.pointsEarned,
              data3: data.month3.pointsEarned, data4: data.month4.pointsEarned, data5: data.month5.pointsEarned, 
              data6: data.month6.pointsEarned, data7: data.month7.pointsEarned,stepsWalked: data.dailyStepsWalked,
               pointsEarned: data.dailyPointsEarned})
  
           console.log("month function called")
          })
          .catch(console.log)
      }

      _1week()
      {
        fetch('http://192.168.0.103:5000/statWeek',{
            method: 'GET'
          })
          .then(res => res.json())
          .then((data) => {
            this.setState({ data1: data.week1.pointsEarned, data2: data.week2.pointsEarned,
              data3: data.week3.pointsEarned, data4: data.week4.pointsEarned, data5: data.week5.pointsEarned, 
              data6: data.week6.pointsEarned, data7: data.week7.pointsEarned,stepsWalked: data.dailyStepsWalked,
               pointsEarned: data.dailyPointsEarned})
  
           console.log("week function called")
          })
          .catch(console.log)
      }

      _1year()
      {
        fetch('http://192.168.0.103:5000/statYear',{
            method: 'GET'
          })
          .then(res => res.json())
          .then((data) => {
            this.setState({ data1: data.year1.pointsEarned, data2: data.year2.pointsEarned,
              data3: data.year3.pointsEarned, data4: data.year4.pointsEarned, data5: data.year5.pointsEarned, 
              data6: data.year6.pointsEarned, data7: data.year7.pointsEarned,stepsWalked: data.dailyStepsWalked,
               pointsEarned: data.dailyPointsEarned})
  
           console.log("week function called")
          })
          .catch(console.log)
      }



      changeButtonColor1= () => {
        this.setState({ buttonColor1: 'rgb(254, 170, 58)',buttonColor2: '#7b899b', buttonColor3: '#7b899b', buttonColor4: '#7b899b'  }); 
      }
      changeButtonColor2= () => {
        this.setState({ buttonColor2: 'rgb(254, 170, 58)', buttonColor1: '#7b899b', buttonColor3: '#7b899b', buttonColor4: '#7b899b' }); 
      }
      changeButtonColor3= () => {
        this.setState({ buttonColor3: 'rgb(254, 170, 58)', buttonColor2: '#7b899b', buttonColor1: '#7b899b', buttonColor4: '#7b899b' }); 
      }
      changeButtonColor4= () => {
        this.setState({ buttonColor4: 'rgb(254, 170, 58)', buttonColor2: '#7b899b', buttonColor1: '#7b899b', buttonColor3: '#7b899b' }); 
      }
    constructor(props) {
        super(props);
        this.state = {
            data1: 10,
            data2: 10,
            data3: 10,
            data4: 10,
            data5: 10,
            data6: 10,
            data7: 10,
            stepsWalked: 10,
            pointsEarned: 10,
            position: "flex-end",
            buttonColor1: 'rgb(254, 170, 58)',
            buttonColor2: '#7b899b',
            buttonColor3: '#7b899b',
            buttonColor4: '#7b899b',
            
        };
    }


    render() {
        const fill = 'rgb(254, 170, 58)'
        const data = [this.state.data1,this.state.data2,this.state.data3,this.state.data4,
            this.state.data5,this.state.data6,this.state.data7];
        const xvalue=[10,20,30,40,50,60,70]
            const axesSvg = { fontSize: 10, fill: 'red' };
            const verticalContentInset = { top: 10, bottom: 10 }
            const xAxisHeight = 30






/***************************************************************************************************************** */
        return (
            <ImageBackground
            source={require('./background.png')}
            style={{width: '100%', height: '100%'}}>
                <View style={{ width: 400, height: 10, justifyContent: 'center', flexDirection: 'row', alignItems: 'center', marginTop: 70}}>
                    <Button 
                    left= {-50}
                    title="Daily"
                    color={this.state.buttonColor1}
                    onPress={() => {this._daily(),this.changeButtonColor1()}} 
                    />
                    <Button
                    title="1 Week"
                    color={this.state.buttonColor2}
                    onPress={() => {this._1week(),this.changeButtonColor2()}}
                    />
                    <Button
                    title="1 Month"
                    color={this.state.buttonColor3}
                    onPress={() => {this._1month(),this.changeButtonColor3()}}
                    
                    />
                    <Button
                    title="1 Year"
                    color={this.state.buttonColor4}
                    onPress={() => {this._1year(),this.changeButtonColor4()}}
                    />
                </View>
                <YAxis
                    data={data}
                    contentInset={{ top: 30, bottom: 30, left: 30, right: 30,  }}
                    svg={{
                        fill: 'grey',
                        fontSize: 10,
                    }}
                    numberOfTicks={10}
                    formatLabel={(value,index) => `${data[index]} pts`}
                />
                <BarChart
                    style={{ height: 300 }}
                    data={ data }
                    svg={{ fill }}
                    gridMin={0}
                    contentInset={{ top: 30, bottom: 10, left: 30, right: 30}}
                    spacingInner={0.9}
                    spacingOuter={0.5}
                >
                    <Grid/>
                </BarChart>
                <View style={{width: '100%', height: 25, opacity: .6,backgroundColor: '#8ab0bb', textAlignVertical: "center"}}>
                  <XAxis
                      data={data}
                      style={{paddingTop: 6}}
                      formatLabel={(value,index) => `${data[index]}`}
                      contentInset={{left: 56, right: 60}}
                      svg={{ fontSize: 15, fontWeight: "bold", fill: 'rgb(255,255,255)' }}
                  />
                </View>
            <View style= {{flexDirection:'column',flex:1}}>
                    <View style={ {marginLeft: '15%', width: 320, height: 50, backgroundColor: '#d6d2d295', marginBottom: '3%', marginTop: '4%',borderRadius:10, borderWidth: 1, borderColor: '#d6d2d295'}} >
                        <Text style={{textAlign: 'left', marginLeft: '4%'}}>
                            <Text style={{fontWeight: "bold", fontSize: 25}}>{this.state.stepsWalked}</Text>                      steps taken this week         
                        </Text>
                    </View>
                    <View style={{marginLeft: '15%', width: 320, height: 50, backgroundColor: '#d6d2d295', marginBottom: '3%', borderRadius:10, borderWidth: 1, borderColor: '#d6d2d295'}} >
                        <Text style={{textAlign: 'left', marginLeft: '4%'}}>
                            <Text style={{fontWeight: "bold", fontSize: 25}}>{this.state.pointsEarned}</Text>                           points earned this week  
                        </Text>
                        {this.cityLocation(),console.log('position',  this.state.position)}
                    </View>
                    <View style={[styles.container, { justifyContent: this.state.position, flexDirection: 'row'},{width: 500, height: 38, marginBottom: '3%'}]}>
                        
                        
                        <Image source={city} style = {{ width: 500, height: 200}} />
                        
                    </View>
            </View>

        </ImageBackground>
        )


    }
 
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        paddingTop: 64,
        paddingBottom: 32
      },
    button: {
      backgroundColor: "#DDDDDD"
    },
    line: {
      borderBottomWidth: 1,
      borderColor: '#f48a92'
    },
    parallelogram: {
      width: 150,
      height: 100
    }   
  });

  