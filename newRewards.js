import React, { useEffect, useState } from 'react'
import { BarChart, Grid } from 'react-native-svg-charts'
import { ImageBackground, Image, Dimensions, PixelRatio } from 'react-native';
import { StyleSheet, View, Button, SafeAreaView, Text, Alert,UIManager,LayoutAnimation,TouchableOpacity, ScrollView} from 'react-native';
import rewards_icon from './images/rewards_icon.png'
import reward1 from './images/reward1.png'

const {
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
  } = Dimensions.get('window');

  // based on phone's scale
  const scale = SCREEN_WIDTH / 320;

export default class newrewards extends React.PureComponent {
    _normalize(size) {
        const newSize = size * scale 
        return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2
        
      }
    componentDidMount() {
        fetch(hostip+'getRewards',{
          method: 'GET'
        })
        .then(res => res.json())
        .then((data) => {
          this.setState({ brands: data.rewards})

         console.log("rewards api called")
        })
        .catch(console.log)
      }
      constructor(props) {
        super(props);
        this.state = {
            brands: [{"id":"1","brand": "Plant a tree","uri":"asset:/reward1.png"},
                {"id":"2","brand": "Ugrade your athletic gear","uri":"asset:/reward2.png"},
                {"id":"3","brand": "Explore food delivery services","uri":"asset:/reward3.png"}]
        };
    }

    render() {

/***************************************************************************************************************** */
        return (
            <ImageBackground
            source={require('./background.png')}
            style={{width: '100%', height: '100%'}}>

                <ScrollView>
                <View style={{flex: 1,alignItems: "center",justifyContent: "center", flexDirection: 'column', paddingTop: 50,width: '100%',height: 130, paddingBottom: 70}}>
                      <View style={{flex: 1,flexDirection: "row",alignItems: "center",justifyContent: "center"}}>
                        <Image source={rewards_icon} style={{ resizeMode: "contain",alignItems: "center",justifyContent: "center" }}/>
                        <Text allowFontScaling={false} style={{fontSize: this._normalize(20),  position: "absolute",color: "white"}}>Rewards</Text>
                      </View>
                </View>
                {
                this.state.brands.map((item, index) => (
                    <View key = {item.id} style = {styles.container}>
                        <Text allowFontScaling={false} style={{ marginLeft: 10, color: 'white', fontSize: this._normalize(14)}}>{item.brand}</Text>
                        <Image source={ {uri: item.uri} } style={styles.imageStyle}  />
                    </View>
                ))
                }
                </ScrollView>

        </ImageBackground>
        )


    }
 
}
const styles = StyleSheet.create ({
    container: {
       flexDirection: 'column',
       paddingBottom: 15
    },
    item: {
        marginLeft: 10, 
        color: 'white', 
        fontSize: 18,
    },
    imageStyle: {
        width: '100%',
        height: 110,
    }
 })


